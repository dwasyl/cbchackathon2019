# Team True North, CBC Hackathon 2019, February 2019
#!/usr/bin/env python
import requests
import time
import datetime

now = datetime.datetime.now()
show_duration = time.time() + 5

stream_url = "http://cbc_r1_ykn.akacast.akamaistream.net/7/369/451661/v1/rc.akacast.akamaistream.net/cbc_r1_ykn"

url_timestamp = now.strftime("%Y-%m-%d-%H-%M")

mp3_name = "streams/" + url_timestamp + "_cbc_r1_ykn.mp3"


r = requests.get(stream_url, stream=True, timeout=5)

while time.time() < show_duration:
   with open(mp3_name, 'wb') as f:
       try:
           for block in r.iter_content(1024):
               f.write(block)
       except KeyboardInterrupt:
           pass

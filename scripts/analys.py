# Team True North, CBC Hackathon 2019, February 2019
# analys.py 'inputfile' 'dictionaryfile' (saves to data.json)
import json
import csv
import sys

def gen_dict_extract(key, val, var):
    if hasattr(var,'iteritems'):
        for k, v in var.iteritems():
            if k == key and v == val:
                yield k
            if isinstance(v, dict):
                for result in gen_dict_extract(key, v):
                    yield result
            elif isinstance(v, list):
                for d in v:
                    for result in gen_dict_extract(key, d):
                        yield result

def search_dict(j, var, val):
    """ (Lazy) Search a dict (vals all converted to lowercase) """
    words = []

    for word in j:
        if word.get(var) and word[var].lower() == val.lower():
            words.append(word)

    return words

def get_word(j, time):
    """ Search for word/item dict starting at a time and return the word"""
    word = search_dict(j, 'start_time', time)[0] # take first result
    return word['content']

def get_word_item(j, time):
    """ Search for word/item dict starting at a time and return the dict"""
    word = search_dict(j, 'start_time', time)[0] # take first result
    return word

def find_host(j):
    """ Find host of show (based on first speaker in transcript """
    host = j['results']['speaker_labels']['segments'][0]['speaker_label']
    return host

def get_host_items(host, all):
    items = []

    for spkr in all:
        if spkr['speaker'] == host:
            items.extend(spkr['items'])

    return items


j = {}
with open(sys.argv[1]) as f:
    j = json.load(f)


search_words = []
if len(sys.argv) > 2:
    with open(sys.argv[2], encoding='utf-8-sig') as f:
        reader = csv.DictReader(f)

        for row in reader:
            word = {
                'index': row['KEYWORD'],
                'words': []
            }
            if row['V1']:
                word['words'].append(row['V1'])
            if row['V2']:
                word['words'].append(row['V2'])
            if row['V3']:
                word['words'].append(row['V3'])

            search_words.append(word)
else:
    search_words = [
        {
            'index': 'Fort McPherson',
            'words': ['Fort McPherson', 'Fort-McPherson'],
        },
    ]

host = find_host(j)

for word in j['results']['items']: # reorder/flatten json as only ever one alternative
     word['content'] = word['alternatives'][0]['content']
     word['confidence'] = word['alternatives'][0]['confidence']
     word.pop('alternatives')

num_speakers = j['results']['speaker_labels']['speakers']

# Build dict of phrases by speaker
phrases = []
phrase = {}
for spkr_seg in j['results']['speaker_labels']['segments']:
   if len(spkr_seg['items']) > 0:
       items = [get_word_item(j['results']['items'], time=w['start_time']) for w in spkr_seg['items']]
       words = [word['content'] for word in items]

       if not phrase.get('speaker') == spkr_seg['speaker_label']:
           if phrase.get('speaker'):
               phrases.append(phrase)

           phrase = {}
           phrase['start_time'] = spkr_seg['start_time'] # Based on speaker_labels block segments
           phrase['end_time'] = spkr_seg['end_time']
           phrase['speaker'] = spkr_seg['speaker_label']
           phrase['items'] = []
           phrase['words'] = []
       else:
           phrase['end_time'] = spkr_seg['end_time'] # Update end time with adjacent speaker block

       phrase['words'].extend(words)
       phrase['items'].extend(items)
phrases.append(phrase)

host_items = get_host_items(host, phrases)

# Host
print('{}'.format(host))
# Output all phrases
for phrase in phrases:
    print('{}: {}: {}: {}'.format(phrase['start_time'], phrase['end_time'], phrase['speaker'], ' '.join([w for w in phrase['words']]) ))

# Actually search
results = []
for needle in search_words:
    keyword = {}
    keyword['index'] = needle['index']
    keyword['results'] = []
    keyword['start_times'] = []

    for sub in needle['words']:
        result = search_dict(host_items, 'content', sub.lower())
        if result:
            keyword['results'].extend(result)
            keyword['start_times'].extend([round(float(r['start_time'])) - 1 for r in result])

    if len(keyword['start_times']) > 0:
        results.append(keyword)

with open('data.json', 'w') as outfile:
    json.dump(results, outfile)

// CBC Caffeine Player
// version 14.6.2
// built on Wednesday, January 23rd, 2019, 4:02:30 PM
if (!CBC) var CBC = {};
CBC.APP || (CBC.APP = {});
CBC.APP.CaffeineEventDispatcher || (CBC.APP.CaffeineEventDispatcher = function(a) {
    var i = {};
    this.on = function(e, t, n) {
        if (void 0 === e) throw new Error("Cannot listen for undefined event");
        i[e] || (i[e] = []);
        var r = !0 === n ? "unshift" : "push";
        i[e][r](t)
    };
    this.off = function(e, t) {
        if (void 0 === e) throw new Error("Cannot remove a listener of an undefined event");
        i[e] = CBC.APP.Caffeine._.without(i[e], t)
    };
    this.trigger = function(e, t) {
        if (void 0 === e) throw new Error("Cannot trigger undefined event");
        if (i[e])
            for (var n = i[e].slice(0), r = 0; r < n.length; r++) try {
                n[r].call(a || CBC.APP.Caffeine, t)
            } catch (e) {
                window.console.error(e)
            }
    }
});
CBC.APP.Caffeine || (CBC.APP.Caffeine = function() {
    "use strict";
    var n, i = 0,
        t = 0,
        r = !1,
        a = {},
        s = new CBC.APP.CaffeineEventDispatcher,
        o = {},
        c = {},
        f = [],
        u = ["//ec2-18-191-218-191.us-east-2.compute.amazonaws.com/assets/js/underscore-min.js", "//ec2-18-191-218-191.us-east-2.compute.amazonaws.com/assets/js/Caffeine.modules.js", "//ec2-18-191-218-191.us-east-2.compute.amazonaws.com/assets/css/caffeine.css", "//ec2-18-191-218-191.us-east-2.compute.amazonaws.com/assets/js/cbc-videoheartbeat.js", "//ec2-18-191-218-191.us-east-2.compute.amazonaws.com/assets/settings_local.json"];

    function l(r, a, i, e) {
        var s = new XMLHttpRequest;
        s.open("get", r, !0);
        !0 === e && (s.withCredentials = !0);
        s.onreadystatechange = function(e) {
            if (4 === this.readyState) {
                s.onreadystatechange = null;
                t = r, 200 === (n = this).status || 201 === n.status || 0 === n.status && "file:///" === t.substring(0, 8) && "" !== n.responseText ? void 0 !== a && a.call(s, r) : void 0 !== i && i.call(s, r);
                var t, n
            }
        };
        s.send()
    }

    function C(e) {
        var t = e.url.split("?")[0];
        switch (t = t.split(".").pop().toLowerCase()) {
            case "json":
                ! function(r) {
                    l(r, function(e) {
                        var t = JSON.parse(this.responseText);
                        o.SETTINGS = {};
                        for (var n in t) t.hasOwnProperty(n) && a(n, t[n]);
                        g(r)
                    }, P);

                    function a(e, t) {
                        Object.defineProperty(o.SETTINGS, e, {
                            get: function() {
                                return t
                            }
                        })
                    }
                }(e.url);
                break;
            case "css":
                ! function(e) {
                    var t = document.createElement("link");
                    t.setAttribute("rel", "stylesheet");
                    t.setAttribute("href", e);
                    t.setAttribute("type", "text/css");
                    t.setAttribute("media", "screen");
                    document.getElementsByTagName("head")[0].appendChild(t);
                    g(e)
                }(e.url);
                break;
            case "js":
                ! function(e) {
                    var t = document.createElement("script");
                    t.setAttribute("type", "text/javascript");
                    t.setAttribute("src", e);
                    t.onload = function() {
                        g(e)
                    };
                    t.onerror = function() {
                        P(e)
                    };
                    void 0 !== t && document.getElementsByTagName("head")[0].appendChild(t)
                }(e.url)
        }
    }

    function d(e, t) {
        r || function() {
            CBC.APP.Caffeine._ = window._.noConflict();
            n = CBC.APP.Caffeine.Helpers.Event;
            s.on(n.CAFFEINE_INSTANCEDESTROYED, v);
            r = !0;
            CBC.APP.Caffeine._.defer(s.trigger, n.CAFFEINE_READY)
        }();
        CBC.APP.Caffeine.getModule("Core.Instance").call(t, e);
        a[t.session.config.ciid] = t
    }

    function h(e) {
        if (!e) return null;
        var t = decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(e).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
        p(e, t);
        return t
    }

    function p(e, t) {
        n = new RegExp(e + "=.*?(;|$|\\s)", "g"), document.cookie = document.cookie.replace(n, "");
        var n, r = e + "=" + t + "; max-age=1296000;path=/";
        document.cookie = r
    }

    function v(e) {
        delete a[e]
    }

    function g(e) {
        for (var t = 0; t < f.length; t++)
            if (f[t].url === e) {
                f[t].status = !0;
                for (var n = 0; n < f[t].successCallbacks.length; n++) f[t].successCallbacks[n]();
                break
            }
    }

    function P(e) {
        for (var t = 0; t < f.length; t++)
            if (f[t].url === e) {
                f[t].status = !0;
                for (var n = 0; n < f[t].errorCallbacks.length; n++) f[t].errorCallbacks[n]();
                break
            }
    }

    function b() {
        for (var e = t = 0; e < f.length; e++) f[e] && !0 === f[e].status && t++;
        t === i && s.trigger("caffeineDependenciesLoaded")
    }

    function A(e, t, n) {
        this.status = !1;
        this.url = e;
        this.successCallbacks = [];
        this.errorCallbacks = [];
        this.addSuccessCallback = function(e) {
            "function" == typeof e && this.successCallbacks.push(e)
        };
        this.addErrorCallback = function(e) {
            "function" == typeof e && this.errorCallbacks.push(e)
        };
        this.addSuccessCallback(t);
        this.addErrorCallback(n)
    }

    function k(e, t, n) {
        for (var r, a = 0; a < f.length; a++)
            if (f[a].url === e) {
                r = f[a];
                break
            }
        if (!r || !0 !== r.status)
            if (r && !1 === r.status) {
                r.successCallbacks.push(t);
                r.errorCallbacks.push(n)
            } else {
                r = new A(e, t, n);
                f.push(r);
                i++;
                C(r)
            }
    }
    return {
        getSettings: function() {
            return o.SETTINGS
        },
        get volume() {
            var e = parseFloat(h("avcaff_volume"));
            return isNaN(e) ? 1 : e
        },
        set volume(e) {
            CBC.APP.Caffeine.volume !== (e = parseFloat(e)) && p("avcaff_volume", e)
        },
        get captions() {
            return !0 === JSON.parse(h("avcaff_captions"))
        },
        set captions(e) {
            p("avcaff_captions", !0 === JSON.parse(e))
        },
        get autoDockDisabled() {
            return !0 === JSON.parse(h("avcaff_autodockdisable"))
        },
        set autoDockDisabled(e) {
            p("avcaff_autodockdisable", !0 === JSON.parse(e))
        },
        get version() {
            return "14.6.2"
        },
        on: s.on,
        off: s.off,
        trigger: s.trigger,
        initInstance: function(e) {
            var t = function() {
                var e, t = {get on() {
                        return e.on
                    },
                    get off() {
                        return e.off
                    },
                    get trigger() {
                        return e.trigger
                    }
                };
                e = new CBC.APP.CaffeineEventDispatcher(t);
                return t
            }();
            if (r) d(e, t);
            else {
                s.on("caffeineDependenciesLoaded", function() {
                    d(e, t)
                });
                ! function() {
                    for (; 0 < u.length;) k(u.pop(), b, b)
                }()
            }
            return t
        },
        getInstance: function(e) {
            return void 0 === e ? a : a[e]
        },
        pauseAll: function() {
            for (var e in a) a.hasOwnProperty(e) && a[e].pause()
        },
        destroyAll: function() {
            for (var e in a) a.hasOwnProperty(e) && a[e].destroy()
        },
        getXhr: l,
        addDependency: k,
        addModule: function(e, t, n) {
            c[e] = t;
            if (n) {
                e = e.split(".");
                for (var r = CBC.APP.Caffeine, a = 0; a < e.length; a++) {
                    r[e[a]] || (a < e.length - 1 ? r[e[a]] = {} : r[e[a]] = t);
                    r = r[e[a]]
                }
            }
        },
        getModule: function(e) {
            return c[e]
        }
    }
}());
//# sourceMappingURL=Caffeine.js.map

var Listen = function() {
    "use strict";

    function a(a, b) {
        document.addEventListener(a, b)
    }

    function b(a, b) {
        document.removeEventListener(a, b)
    }

    function c(a, b) {
        var c = document.createEvent("Event");
        c.initEvent(a, !0, !1), c.data = b, document.dispatchEvent(c)
    }
    var d = this,
        e = {
            LISTEN_HOSTNAME: "www.cbc.ca",
            LISTEN_BASE_URL: "https://www.cbc.ca/listen/",
            LISTEN_PLAYER_CIID: "listen-player"
        };
    return document.addEventListener("DOMContentLoaded", function(a) {
        new d.Player, new d.Page, new d.Router, new d.Nav, new d.Playlist, new d.Share, new d.TimeTag, new d.Plus, new d.Overlay, new d.RecentClips, new d.NowPlaying, new d.LiveRegion, new d.MoreClips, new d.Search, new d.FeatureClips, new d.GPTAds, new d.CaffeineIntegrations
    }), {
        cfg: e,
        on: a,
        off: b,
        trigger: c
    }
};
if (!CBC) var CBC = {};
CBC.APP || (CBC.APP = {}), CBC.APP.Listen || (CBC.APP.Listen = new Listen, CBC.APP.Listen._ = window._.noConflict()), Listen.prototype.CaffeineIntegrations = function() {
    function a(a) {
        a.keyCode ? 13 == a.keyCode && b() : b()
    }

    function b() {
        var a = parseInt(L("cbclistenbannercount"), 10);
        a || (a = 0), document.cookie = "cbclistenbannercount=" + (parseInt(a, 10) + 1) + "; max-age=31536000; path=/; domain=cbc.ca;";
        var b = document.getElementsByClassName("ule-banner-wrapper")[0],
            c = document.getElementsByTagName("header")[0],
            d = document.getElementsByTagName("main")[0];
        b.classList.remove("ule-banner--enabled"), c.classList.remove("ule-banner--enabled"), d.classList.remove("ule-banner--enabled")
    }

    function c() {
        document.getElementById("live-wrapper") && (ca = parseInt(document.getElementById("live-wrapper").dataset.radioIndex, 10)), I(ca), -1 !== window.location.href.indexOf("?location=true") && (U.trigger("listen.changeLocation", document.getElementById("liveradio-changelocation")), window.scrollTo(0, 300))
    }

    function d(a) {
        document.getElementById("live-wrapper") && (ca = parseInt(document.getElementById("live-wrapper").dataset.radioIndex, 10)), I(ca), -1 !== window.location.href.indexOf("?location=true") && (U.trigger("listen.changeLocation", document.getElementById("liveradio-changelocation")), window.scrollTo(0, 300)), e()
    }

    function e() {
        if (ga && document.getElementById("radio-stations-list") && -1 !== window.location.href.indexOf("?play=true")) {
            var a = document.querySelector("[data-btntype=play]");
            U.trigger("listen.play", a)
            console.log("2ABC");
        }
    }

    function f(a) {
        S = V.Helpers.Event, ga = CBC.APP.Caffeine.getInstance(U.cfg.LISTEN_PLAYER_CIID), ga.on(S.SESSION_QUEUEUPDATED, function() {
            setTimeout(m, 0)
        }), ga.on(S.PLAYER_PLAYING, n), ga.on(S.PLAYER_PAUSED, r), ga.on(S.PLAYER_STOPPED, r), ga.on(S.PLAYER_ENDED, r), ga.on(S.PLAYER_READY, function() {
            var a = document.querySelector(".listen-player-container");
            parseInt(ga.currentClip.id, 10) ? a.classList.remove("live") : a.classList.add("live"), null !== document.querySelector(".seek") && document.querySelector(".seek").addEventListener("touchend", function(a) {
                isNaN(ga.player.duration) || (ga.player.currentTime = parseInt(a.changedTouches[0].clientX / document.getElementsByTagName("body")[0].clientWidth * ga.player.duration, 10))
            })
        });
        var b = 0;
        ga.on(S.PLAYER_METADATALOADED, function() {
            0 == b++ && e()
        }), o(), W && s()
    }

    function g(a) {
        h(a.data.dataset.radioIndex)
    }

    function h(a) {
        if (ca = parseInt(a, 10), 0 === ca ? (document.getElementById("radio-1-tab").classList.add("active"), document.getElementById("radio-2-tab").classList.remove("active")) : (document.getElementById("radio-2-tab").classList.add("active"), document.getElementById("radio-1-tab").classList.remove("active")), !U._.isEmpty(_[ca])) {
            document.getElementById("icon-play").dataset.liveradioid = _[ca].cbc$callSign, document.getElementById("chapter-title").innerHTML = $[ca].episode.title, document.getElementById("chapter-time").innerHTML = l($[ca].epochStart, $[ca].epochEnd), document.getElementById("current-station-title").innerHTML = _[ca].title;
            var b = _[ca].cbc$webUrl;
            b = b.replace("radio2", "cbcmusic"), document.getElementById("current-station-title").href = b, document.getElementById("station-location").href = b + "?location=true", i()
        }
    }

    function i() {
        try {
            "playing" === ga.player.status && ga.currentClip.id == document.getElementById("icon-play").dataset.liveradioid ? document.getElementById("icon-play").classList.add("playing") : document.getElementById("icon-play").classList.remove("playing")
        } catch (a) {}
    }

    function j(a) {
        var b = document.getElementById("radio-stations-list"),
            c = document.getElementById("featured-live-radio");
        (b && "" !== b.dataset.region || c) && a === ca && (c ? h(a) : (k(a), A(a)))
    }

    function k(a) {
        U._.isEmpty(_[a]) || (document.getElementById("chapter-title").innerHTML = $[a].episode.title, document.getElementById("chapter-time").innerHTML = l($[a].epochStart, $[a].epochEnd), document.getElementById("currently-playing") && (document.getElementById("currently-playing").innerHTML = $[a].episode.title))
    }

    function l(a, b) {
        var c = new Date(parseInt(a, 10)),
            d = new Date(parseInt(b, 10));
        return z(c, !1) + " - " + z(d, !1)
    }

    function m(a) {
        p()
    }

    function n() {
        q(), r(), x(), W && v()
    }

    function o() {
        ga.currentClip && r(), p()
    }

    function p() {
        for (var a = ga.session.getQueue(), b = document.querySelectorAll(".icon-plus"), c = 0; c < b.length; c++) {
            var d = b[c];
            a.indexOf(d.getAttribute("data-id")) >= 0 ? d.classList.add("added-to-queue") : d.classList.remove("added-to-queue")
        }
    }

    function q() {
        var a = document.querySelector(".listen-player-outer");
        a.classList.contains("player-visible") || a.classList.add("player-visible"), document.getElementById("accessibilitySkipLink").classList.add("player-visible")
    }

    function r() {
        for (var a = document.querySelectorAll("[data-btntype=play]"), b = 0; b < a.length; b++) {
            var c = a[b],
                d = c.getAttribute("data-audioid") || c.getAttribute("data-liveradioid");
            "playing" === ga.player.status && ga.currentClip.id == d ? c.classList.add("playing") : c.classList.remove("playing")
            console.log("3AAA");
        }
    }

    function s() {
        function a() {
            e++;
            var a = d[e];
            a ? b(a) : (c(), u())
        }

        function b(a) {
            parseInt(a, 10) ? ga.queueEnd({
                audioId: a
            }) : ga.queueEnd({
                liveRadioId: a
            })
        }

        function c() {
            ga.off(S.SESSION_QUEUEUPDATED, a), ga.on(S.SESSION_QUEUEUPDATED, w)
        }
        var d = window.localStorage.getItem("listen.queue"),
            e = -1;
        if (!d) return void c();
        d = window.localStorage.getItem("listen.queue").split(","), ga.on(S.SESSION_QUEUEUPDATED, a), a()
    }

    function t() {
        try {
            return localStorage.setItem("listen.test", "test"), localStorage.removeItem("listen.test"), !0
        } catch (a) {
            return !1
        }
    }

    function u() {
        var a = window.localStorage.getItem("listen.activeClip"),
            b = ga.session.getQueue().indexOf(a);
        b < 0 || (ga.session.createPlayer(b, !1), q())
    }

    function v() {
        var a = T || "";
        window.localStorage.setItem("listen.activeClip", a), w()
    }

    function w() {
        var a = T || "",
            b = ga.session.getQueue(),
            c = b.indexOf(a),
            d = b;
        c > -1 && (d = b.slice(c)), window.localStorage.setItem("listen.queue", d)
    }

    function x() {
        var a = document.getElementById("CaffeinePlayerlisten-player"),
            b = document.querySelector(".listen-player-container");
        if (a) {
            var c = a.querySelector(".title").textContent,
                d = a.querySelector(".showname").textContent;
            b.setAttribute("aria-label", "My Radio Player now playing " + c + " " + d)
        }
    }

    function y(a, b) {
        var c = document.createElement("tr");
        b.appendChild(c);
        var d = document.createElement("td"),
            e = new Date(parseInt(a.epochStart, 10));
        return d.innerHTML = z(e, !1), c.appendChild(d), d = document.createElement("td"), d.innerHTML = a.episode.title, c.appendChild(d), b
    }

    function z(a, b) {
        var c = a.getHours(),
            d = a.getMinutes(),
            e = c >= 12 ? "pm" : "am";
        return c %= 12, c = c || 12, d = d < 10 ? "0" + d : d, c + ":" + d + " " + (!1 === b ? e : "")
    }

    function A(a) {
        var b = "<em>We could not connect to the program guide; please refresh the page.</em>";
        if (U._.isEmpty(Z[a])) document.getElementById("program-schedule").innerHTML = b;
        else {
            var c = document.createElement("table"),
                d = Z[a].timeslots.length - aa[a];
            if (d > 6 && (d = 6), d <= 1) document.getElementById("program-schedule").innerHTML = b;
            else {
                for (var e = aa[a] + 1; e < aa[a] + d; e++) c = y(Z[a].timeslots[e], c);
                document.getElementById("program-schedule").innerHTML = "", document.getElementById("program-schedule").appendChild(c)
            }
        }
    }

    function B(a) {
        var b, c;
        if (-1 !== window.location.href.indexOf("live") ? (b = document.getElementById("radio-stations-list").dataset.region, c = R(b), c = Q(c)) : c = K(), c === da) j(ca);
        else {
            da = c;
            var d = N(da),
                e = P(da),
                f = O(da);
            _[0] = a[d], _[1] = a[f], D(), C(0, d, "cbc_radio_one"), C(1, e, "cbc_music"), J(da)
        }
    }

    function C(a, b, c) {
        b = b.replace(/-/g, "_");
        var d = F(Date.now() - 864e5),
            e = F(Date.now() + 1728e5),
            f = "https://www.cbc.ca/programguide/schedule.do?output=json&locationKey=" + b + "&networkKey=" + c + "&startDate=" + d + "&endDate=" + e,
            g = new XMLHttpRequest;
        g.onreadystatechange = function() {
            if (4 === g.readyState && 200 === g.status) {
                Z[a] = JSON.parse(g.responseText);
                for (var b = 0; b < Z[a].timeslots.length; b++)
                    if (Z[a].timeslots[b].epochStart > Date.now()) {
                        aa[a] = b - 1, ba[a] = Z[a].timeslots.length - b, $[a] = Z[a].timeslots[aa[a]];
                        var c = Z[a].timeslots[b].epochStart - Date.now();
                        ea.push(window.setTimeout(E, c, a));
                        break
                    }
                j(a)
            }
        }, g.open("GET", f), g.send(null)
    }

    function D() {
        for (var a = 0; a < ea.length; a++) clearTimeout(ea[a]);
        ea = []
    }

    function E(a) {
        if (ba[a] < 2) da = !1, I(a);
        else {
            aa[a]++, ba[a]--;
            var b = Z[a].timeslots[aa[a] + 1].epochStart - Date.now();
            ea.push(window.setTimeout(E, b, a)), $[a] = Z[a].timeslots[aa[a]], j(a)
        }
    }

    function F(a) {
        return G(a).split("-").join("/")
    }

    function G(a) {
        var b = new Date(a),
            c = b.getFullYear(),
            d = b.getMonth() + 1;
        return d = d < 10 ? "0" + d : d, c + "-" + d + "-" + (b.getDate() < 10 ? "0" + b.getDate() : b.getDate())
    }

    function H() {
        var a = document.getElementById("liveradio-radiostation");
        if (a) {
            var b, c = a.dataset.network,
                d = L("cbclocal");
            if (d) {
                var e = d.split("radio=");
                try {
                    b = e[1].split("&")[0]
                } catch (g) {
                    var f = d.split("region=");
                    try {
                        b = f[1].split("&")[0]
                    } catch (h) {
                        b = "ottawa"
                    }
                }
            } else b = "ottawa";
            setTimeout(function() {
                U.trigger("listen.navigateToPage", M(c, b))
            }, 0)
        }
    }

    function I(a) {
        var b = document.getElementById("radio-stations-list"),
            c = document.getElementById("featured-live-radio");
        if (b && "" !== b.dataset.region || c)
            if (U._.isEmpty(X)) {
                var d = new XMLHttpRequest;
                d.onreadystatechange = function() {
                    if (4 === d.readyState && 200 === d.status) {
                        X = JSON.parse(d.responseText).entries;
                        for (var a, b = 0; b < X.length; b++) a = X[b].cbc$webUrl.split("/"), Y[a[a.length - 1]] = X[b];
                        B(Y)
                    }
                }, d.open("GET", "https://tpfeed.cbc.ca/f/ExhSPC/cbc-live-radio?sort=cbc$province"), d.send(null)
            } else B(Y)
    }

    function J(a) {
        var b = L("cbclocal");
        b ? -1 === b.search("radio=") ? b += "&radio=" + a : b = b.replace(/radio=[a-z-]+/gm, "radio=" + a) : b = "radio=" + a, document.cookie = "cbclocal=" + encodeURIComponent(b) + "; max-age=31536000; path=/; domain=cbc.ca;"
    }

    function K() {
        var a, b = L("cbclocal");
        if (b) {
            var c = b.split("radio=");
            try {
                a = c[1].split("&")[0]
            } catch (e) {
                var d = b.split("region=");
                try {
                    a = d[1].split("&")[0]
                } catch (f) {
                    a = "ottawa"
                }
            }
        } else a = "ottawa";
        return a
    }

    function L(a) {
        return a ? decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(a).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null : null
    }

    function M(a, b) {
        return "cbcmusic" === a ? "/listen/live/cbcmusic/" + O(b) : "/listen/live/radio1/" + N(b)
    }

    function N(a) {
        switch (a) {
            case "bc":
                return "vancouver";
            case "sask":
                return "saskatoon";
            case "manitoba":
                return "winnipeg";
            case "nb":
                return "fredericton";
            case "pei":
                return "charlottetown";
            case "ns":
                return "halifax";
            case "nl":
            case "stjohns":
                return "st-johns";
            case "north":
                return "iqaluit";
            case "hamilton":
                return "toronto";
            case "kuujjuaq":
                return "nord-quebec";
            case "princegeorge":
                return "prince-george";
            case "thompson":
                return "winnipeg";
            case "thunderbay":
                return "thunder-bay";
            case "quebec":
                return "quebec-city";
            case "nordquebec":
                return "nord-quebec";
            case "saintjohn":
                return "saint-john";
            case "cornerbrook":
                return "corner-brook";
            case "goosebay":
                return "goose-bay";
            case "grandfalls":
                return "grand-falls-gander";
            case "capebreton":
                return "halifax";
            default:
                return a
        }
    }

    function O(a) {
        switch (a) {
            case "bc":
            case "kamloops":
            case "kelowna":
            case "princegeorge":
            case "vancouver":
            case "victoria":
            case "whitehorse":
            case "pacific":
                return "pacific";
            case "north":
            case "calgary":
            case "edmonton":
            case "inuvik":
            case "yellowknife":
            case "mountain":
                return "mountain";
            case "sask":
            case "manitoba":
            case "regina":
            case "saskatoon":
            case "thompson":
            case "winnipeg":
            case "central":
                return "central";
            case "nb":
            case "nl":
            case "ns":
            case "pei":
            case "fredericton":
            case "moncton":
            case "saintjohn":
            case "charlottetown":
            case "halifax":
            case "sydney":
            case "cornerbrook":
            case "goosebay":
            case "grandfalls":
            case "stjohns":
            case "atlantic":
                return "atlantic";
            case "internationalpacific":
            case "intP":
                return "international-pacific";
            case "internationaleastern":
            case "intE":
                return "international-eastern";
            default:
                return "eastern"
        }
    }

    function P(a) {
        switch (a) {
            case "bc":
            case "kamloops":
            case "kelowna":
            case "princegeorge":
            case "vancouver":
            case "victoria":
            case "whitehorse":
            case "pacific":
                return "vancouver";
            case "north":
            case "calgary":
            case "edmonton":
            case "inuvik":
            case "yellowknife":
            case "mountain":
                return "edmonton";
            case "sask":
            case "manitoba":
            case "regina":
            case "saskatoon":
            case "thompson":
            case "winnipeg":
            case "central":
                return "winnipeg";
            case "nb":
            case "nl":
            case "ns":
            case "pei":
            case "fredericton":
            case "moncton":
            case "saintjohn":
            case "charlottetown":
            case "halifax":
            case "sydney":
            case "cornerbrook":
            case "goosebay":
            case "grandfalls":
            case "stjohns":
            case "atlantic":
                return "halifax";
            case "internationalpacific":
            case "intP":
            case "internationaleastern":
            case "intE":
            default:
                return "toronto"
        }
    }

    function Q(a) {
        switch (a) {
            case "kitchener-waterloo":
                return "kitchener-waterloo";
            case "nord-quebec":
                return "kuujjuaq";
            case "quebec-city":
                return "quebec";
            case "grand-falls-gander":
            case "grand_falls_gander":
                return "grandfalls";
            default:
                return a.replace(/-|\s/g, "")
        }
    }

    function R(a) {
        if (a === P(da) && da) return da;
        switch (a) {
            case "atlantic":
                return "halifax";
            case "central":
                return "winnipeg";
            case "eastern":
                return "toronto";
            case "pacific":
                return "vancouver";
            case "mountain":
                return "edmonton";
            default:
                return a
        }
    }
    var S, T, U = CBC.APP.Listen,
        V = CBC.APP.Caffeine,
        W = t(),
        X = [],
        Y = {},
        Z = [{}, {}],
        $ = [{}, {}],
        _ = [{}, {}],
        aa = [{}, {}],
        ba = [],
        ca = 0,
        da = !1,
        ea = [],
        fa = document.getElementById("banner-close-button");
    fa.addEventListener("click", a), fa.addEventListener("keyup", a);
    var ga = V.getInstance(U.cfg.LISTEN_PLAYER_CIID);
    ga || V.on("caffeineDependenciesLoaded", function() {
        window.setTimeout(f, 10)
    }), U.on("listen.pageFetched", o), U.on("listen.searchResultsUpdated", o), U.on("listen.moreClipsFetched", o), U.on("listen.recentClipsFetched", o), U.on("listen.toggleRadioLiveWidget", g), U.on("listen.pageFetched", H), U.on("listen.pageFetchedCompleted", d), H(), c()
}, Listen.prototype.FeatureClips = function() {
    function a(a) {
        n = document.querySelectorAll(".featured-item");
        var b = 0;
        for (a && "listen.moreClipsFetched" === a.type && (b = 12 * a.data), b; b < n.length; b++) try {
            var g = n[b].querySelector(".focus-trigger");
            g.addEventListener("mouseenter", c), g.addEventListener("focus", e), g.addEventListener("mouseleave", d), g.addEventListener("blur", f), l(n[b], !1)
        } catch (h) {}
    }

    function b() {
        if (b)
            for (var a = 0; a < n.length; a++) {
                var b = n[a].querySelector(".focus-trigger");
                b.removeEventListener("mouseenter", e), b.removeEventListener("click", e), b.removeEventListener("mouseleave", f)
            }
    }

    function c(a) {
        var b = a.currentTarget.parentNode.parentNode;
        b !== p && (h(b), j())
    }

    function d(a) {
        var b = a.currentTarget.parentNode.parentNode;
        b !== p || o || i(b)
    }

    function e(a) {
        var b = a.currentTarget.parentNode.parentNode;
        b !== p && h(b)
    }

    function f(a) {
        var b = a.currentTarget.parentNode.parentNode;
        b !== p || o || i(b)
    }

    function g(a) {
        var b = a.data.parentNode.parentNode;
        b !== p && h(b), j()
    }

    function h(a) {
        p && i(), p = a, p.classList.add("active"), m.on("listen.closeEverything", i)
    }

    function i() {
        p && (m.off("listen.closeEverything", i), k(), p.classList.remove("active"), q = p, p = null)
    }

    function j() {
        if (!p) throw new Error("No overlay showing - cannot pin");
        o || (o = !0, window.setTimeout(function() {
            l(p, !0), p.querySelector("button.icon-play").focus()
        }, 0))
    }

    function k() {
        if (!p) throw new Error("No overlay showing - cannot pin");
        o && (l(p, !1), o = !1)
    }

    function l(a, b) {
        if (!a.classList.contains("featured-item")) throw new Error("Cannot enable featured-item tabbing on a non-featured-item node");
        for (var c = b ? "0" : "-1", d = a.querySelectorAll("a,button"), e = 0; e < d.length; e++) d[e].classList.contains("focus-trigger") || (d[e].setAttribute("tabindex", c), b ? d[e].removeAttribute("disabled") : d[e].setAttribute("disabled", "disabled"))
    }
    var m = CBC.APP.Listen,
        n = [],
        o = !1,
        p = null,
        q = null;
    m.on("listen.featureclip", g), m.on("listen.pageFetched", a), m.on("listen.moreClipsFetched", a), m.on("listen.navigateToPage", b), a()
}, Listen.prototype.GPTAds = function() {
    function a() {
        j = window.innerWidth, k < l && j > l && b(), k = j
    }

    function b() {
        e() && (g ? d() : c())
    }

    function c() {
        try {
            googletag.cmd.push(function() {
                CBC.Ads.defineDisplay({
                    type: "bigbox",
                    responsive: !1,
                    lazy: !1,
                    companion: !0,
                    container: "bigbox"
                })
            }), g = !0
        } catch (a) {}
    }

    function d() {
        if (e()) try {
            googletag.pubads().refresh()
        } catch (a) {}
    }

    function e() {
        if (!document.getElementById("bigbox")) return !1;
        var a = document.getElementById("ad-wrapper");
        return !a || "none" !== getComputedStyle(a).getPropertyValue("display")
    }
    var f = CBC.APP.Listen,
        g = !1,
        h = CBC.APP.Listen._,
        i = h.debounce(a, 500),
        j = window.innerWidth,
        k = j,
        l = 1024;
    f.on("listen.pageFetched", b), window.addEventListener("resize", i, !1), b()
}, Listen.prototype.LiveRegion = function() {
    function a(a) {
        d = document.getElementById("radio-stations-list"), e = document.querySelector('button[data-btntype="closeStationList"]'), f = document.getElementById("liveradio-changelocation"), d.classList.contains("hide") ? (c(a.data.dataset.networkKey), d.classList.remove("hide"), e.focus(), f.setAttribute("aria-expanded", "true")) : b()
    }

    function b() {
        d.classList.add("hide"), f.focus(), f.setAttribute("aria-expanded", "false")
    }

    function c(a) {
        var b = new XMLHttpRequest,
            c = new DOMParser;
        b.onreadystatechange = function() {
            if (4 === b.readyState && 200 === b.status)
                for (var a, d, e = c.parseFromString(b.responseText, "text/xml"), f = e.getElementsByTagName("city"), g = 0; g < f.length; g++) try {
                    a = document.createElement("a"), d = document.createTextNode(f[g].getElementsByTagName("title")[0].textContent), a.appendChild(d), a.href = document.getElementById(f[g].getAttribute("key")).dataset.stationUrl + "?play=true", document.getElementById(f[g].getAttribute("key")).innerHTML = "", document.getElementById(f[g].getAttribute("key")).appendChild(a)
                } catch (h) {}
        }, b.open("GET", "https://www.cbc.ca/programguide//liveAll.do?networkKey=" + a), b.send(null)
    }
    var d, e, f, g = CBC.APP.Listen;
    g.on("listen.changeLocation", a), g.on("listen.closeStationList", b)
}, Listen.prototype.MoreClips = function() {
    function a() {
        b();
        var a = document.querySelectorAll(".moreclips"),
            d = document.querySelectorAll(".icon-loading");
        0 !== a.length && (f = a[0], g = d[0], i = f.getAttribute("data-moreclips-path"), c(), k.on("scroll", c))
    }

    function b() {
        k.off("scroll", c), h = 1, f = null, i = null, j = 0
    }

    function c() {
        d() && (k.off("scroll", c), e())
    }

    function d() {
        var a = document.documentElement.scrollHeight,
            b = document.documentElement.clientHeight;
        return a - (window.pageYOffset + b) <= l
    }

    function e() {
        var a = i + (h + 1) + "?format=json",
            b = new XMLHttpRequest;
        b.open("GET", a), b.onreadystatechange = function(a) {
            if (4 == b.readyState) try {
                var d = JSON.parse(b.responseText);
                f.insertAdjacentHTML("beforeend", d.html), h++, d.meta && d.meta.clipCount < d.meta.pageSize ? g.style.display = "none" : k.on("scroll", c), k.trigger("listen.moreClipsFetched", h - 1)
            } catch (e) {
                j < 5 && (c(), j++)
            }
        }, b.setRequestHeader("X-Requested-With", "XMLHttpRequest"), b.send()
    }
    var f, g, h, i, j, k = CBC.APP.Listen,
        l = 180;
    k.on("listen.pageFetched", a), a()
}, Listen.prototype.Nav = function() {
    function a() {
        for (var a = 0; a < r.length; a++) r.item(a).classList.remove("active"); - 1 !== window.location.href.indexOf("live") ? document.getElementById("menu-live").classList.add("active") : -1 !== window.location.href.indexOf("shows") ? document.getElementById("menu-shows").classList.add("active") : -1 !== window.location.href.indexOf("categories") ? document.getElementById("menu-categories").classList.add("active") : document.getElementById("menu-featured").classList.add("active")
    }

    function b() {
        j.trigger("listen.closePlaylist"), "" === k.className ? (k.className = "", setTimeout(c, 0)) : d()
    }

    function c() {
        k.className = "open", l.setAttribute("aria-expanded", "true"), l.setAttribute("aria-label", "Close Navigation"), l.innerHTML = "Close Navigation", n.className = "animated fadeInDownBig", l.focus()
    }

    function d() {
        k.className = "", l.setAttribute("aria-expanded", "false"), l.setAttribute("aria-label", "Open Navigation"), l.innerHTML = "Open Navigation", n.className = "hide", k.contains(document.activeElement) && l.focus()
    }

    function e(a) {
        q ? f() : (j.trigger("listen.closePlaylist"), o.parentNode.className = "has-submenu open", o.setAttribute("aria-expanded", "true"), p.setAttribute("aria-expanded", "true"), o.focus(), q = !0)
    }

    function f() {
        q && (o.parentNode.className = "has-submenu", o.setAttribute("aria-expanded", "false"), p.setAttribute("aria-expanded", "false"), q = !1, o.focus())
    }

    function g(a) {
        "menu-categories" !== a.target.id && f()
    }

    function h() {
        k.contains(document.activeElement) || d(), q && !p.contains(document.activeElement) && f()
    }

    function i() {
        d(), f()
    }
    var j = CBC.APP.Listen,
        k = document.getElementById("main-nav"),
        l = document.getElementById("menu-button"),
        m = k.querySelectorAll("ul"),
        n = m[0],
        o = document.getElementById("menu-categories"),
        p = document.getElementsByClassName("subnav-menu")[0],
        q = !1,
        r = document.getElementsByClassName("menu-link");
    j.on("listen.hamburger", b), j.on("listen.closeNavigation", d), j.on("listen.toggleSubnav", e), j.on("listen.focusChange", h), j.on("listen.closeEverything", i), j.on("listen.pageFetchedCompleted", a), j.on("click", g), a()
}, Listen.prototype.NowPlaying = function() {
    function a(a) {
        f.on("listen.overlayCloseStart", b), g = a.data.parentNode.lastChild.querySelector(".overlay-content");
        var e = a.data.dataset.onnowurl,
            h = new XMLHttpRequest;
        h.onreadystatechange = function() {
            if (4 == h.readyState) try {
                c((new window.DOMParser).parseFromString(h.responseText, "application/xml"))
            } catch (a) {
                d()
            }
        }, h.open("GET", e), h.setRequestHeader("X-Requested-With", "XMLHttpRequest"), h.send()
    }

    function b() {
        f.off("listen.overlayCloseStart", b), f.trigger("listen.overlayContentClosed")
    }

    function c(a) {
        var b = CBC.APP.Listen.Templates["NowPlaying.html"]({
            program: a.querySelector("name").textContent,
            startTime: e(a.querySelector("timeslot").getAttribute("startTime")),
            endTime: e(a.querySelector("timeslot").getAttribute("endTime"))
        });
        g.insertAdjacentHTML("afterbegin", b), f.trigger("listen.overlayContentRendered")
    }

    function d() {
        var a = CBC.APP.Listen.Templates["NowPlaying.html"]({
            program: "Program information is not available at this time",
            startTime: "00:00",
            endTime: "00:00"
        });
        g.insertAdjacentHTML("afterbegin", a), f.trigger("listen.overlayContentRendered")
    }

    function e(a) {
        var b = a.split(":");
        return (parseInt(b[0], 10) + ":" + b[1]).toLowerCase()
    }
    var f = CBC.APP.Listen,
        g = null;
    f.on("listen.nowPlaying", a)
}, Listen.prototype.Overlay = function() {
    function a(a) {
        if (o && o == a.data.parentNode) return void g();
        o && g(), n = a.data, o = a.data.parentNode, q = a.data.getAttribute("data-overlaytype"), d(), e(), m.trigger("listen." + q, a.data), m.on("click", b)
    }

    function b(a) {
        n && !k(o, a.target) && g()
    }

    function c(a) {
        n && !k(o, document.activeElement) && g()
    }

    function d() {
        var a = CBC.APP.Listen.Templates["Overlay.html"]();
        o.insertAdjacentHTML("beforeend", a), p = o.lastChild, r = p.querySelector(".overlay-content"), t = p.querySelector(".overlay-pointer")
    }

    function e() {
        n.setAttribute("aria-expanded", "true"), n.setAttribute("aria-label", "Close " + q)
    }

    function f(a) {
        n && g()
    }

    function g() {
        m.trigger("listen.overlayCloseStart")
    }

    function h() {
        m.off("click", b), n.setAttribute("aria-expanded", "false"), n.setAttribute("aria-label", "Open " + q), o.removeChild(p), n = null, o = null, p = null, q = null, r = null, s = null
    }

    function i() {
        s = r.firstChild, j()
    }

    function j() {
        if (null !== s) {
            var a, b, c, d, e = s.offsetWidth,
                f = s.offsetHeight,
                g = n.offsetWidth,
                h = n.offsetHeight,
                i = l(n),
                j = i.x,
                k = i.y,
                m = t.offsetWidth,
                o = window.innerHeight,
                p = window.innerWidth,
                q = document.querySelector(".listen-player-container").offsetHeight,
                u = document.getElementsByTagName("header")[0].offsetHeight;
            r.style.width = e + "px", r.style.height = f + "px";
            var v = k - u,
                w = k + h,
                x = o - w - q,
                y = j + g / 2,
                z = p - y;
            x > f || x > v ? (a = 0, t.classList.remove("down"), c = -m) : (a = -(h + f + 30), t.classList.add("down"), c = -h), d = g / 2 - m / 2, b = y > e / 2 && z > e / 2 ? -(e / 2 - g / 2) : y > e || y > z ? -(e - g) : 0, r.style.left = b + "px", r.style.top = a + "px", t.style.left = d + "px", t.style.top = c + "px"
        }
    }

    function k(a, b) {
        for (var c = b.parentNode; null !== c;) {
            if (c == a) return !0;
            c = c.parentNode
        }
        return !1
    }

    function l(a) {
        for (var b = 0, c = 0; a;) b += a.offsetLeft - a.scrollLeft + a.clientLeft, c += a.offsetTop - a.scrollTop + a.clientTop, a = a.offsetParent;
        return {
            x: b,
            y: c
        }
    }
    var m = CBC.APP.Listen,
        n = null,
        o = null,
        p = null,
        q = null,
        r = null,
        s = null,
        t = null,
        u = CBC.APP.Listen._,
        v = u.debounce(j, 500);
    m.on("listen.toggleOverlay", a), m.on("listen.overlayContentRendered", i), m.on("listen.closeEverything", f), m.on("listen.overlayContentClosed", h), m.on("listen.focusChange", c), window.addEventListener("resize", v, !1)
}, Listen.prototype.Page = function() {
    var a = CBC.APP.Listen,
        b = document.getElementsByClassName("page")[0];
    a.on("listen.pageFetched", function(a) {
        var c = a.data;
        b.innerHTML = c.html
    })
}, Listen.prototype.Player = function() {
    function a(a) {
        var c = a.data;
        c.classList.contains("playing") ? c.getAttribute("data-liveradioid") ? e() : d() : b(c)
    }

    function b(a) {
        var b, d, e = a.dataset.audioid,
            f = a.dataset.liveradioid,
            g = {},
            i = CBC.APP.Caffeine.getInstance(h.cfg.LISTEN_PLAYER_CIID);
        if (e ? (d = parseInt(e, 10), g = {
                audioId: d
            }) : f && (g = {
                liveRadioId: f
            }, d = f), i.player && (b = i.player.currentClip), !b || b != e && b != f) {
            var j = c(d); - 1 == j ? i.playById(g) : i.session.createPlayer(j, !0)
        } else i.play()
    }

    function c(a) {
        return CBC.APP.Caffeine.getInstance(h.cfg.LISTEN_PLAYER_CIID).session.getQueue().indexOf(a)
    }

    function d() {
        CBC.APP.Caffeine.getInstance(h.cfg.LISTEN_PLAYER_CIID).pause()
    }

    function e() {
        CBC.APP.Caffeine.getInstance(h.cfg.LISTEN_PLAYER_CIID).player.stop()
    }

    function f(a) {
        var b = a.data,
            c = b.getAttribute("data-audioid");
        CBC.APP.Caffeine.getInstance(h.cfg.LISTEN_PLAYER_CIID).queueEnd(c)
    }

    function g(a) {
        var b = document.getElementById("CaffeinePlayerlisten-player"),
            c = document.querySelector(".listen-player-container");
        b && c.focus()
    }
    var h = CBC.APP.Listen;
    CBC.APP.Caffeine.initInstance({
        autoPlay: !0,
        sourceType: "audioapi",
        containerSelector: ".listen-player",
        ciid: h.cfg.LISTEN_PLAYER_CIID,
        playlistSelector: ".modal-list",
        editablePlaylist: !0,
        continuousPlay: !0,
        playlistThumbnails: "host"
    }), h.on("listen.play", a), h.on("listen.addToPlaylist", f), h.on("listen.skiplink", g)
}, Listen.prototype.Playlist = function() {
    function a(a) {
        e.on("listen.overlayCloseStart", c), f = a.data.parentNode.lastChild.querySelector(".overlay-content");
        var b = h.querySelector(".modal-body");
        f.appendChild(h), b.style.height = h.offsetHeight - 140 + "px", e.trigger("listen.overlayContentRendered")
    }

    function b() {
        e.trigger("listen.overlayCloseStart")
    }

    function c() {
        e.off("listen.overlayCloseStart", c), g.appendChild(h), e.trigger("listen.overlayContentClosed")
    }

    function d() {
        CBC.APP.Caffeine.getInstance(e.cfg.LISTEN_PLAYER_CIID).clearQueue()
    }
    var e = CBC.APP.Listen,
        f = null,
        g = document.getElementById("playlist-container"),
        h = document.getElementById("playlistModal");
    e.on("listen.Playlist", a), e.on("listen.closePlaylist", b), e.on("listen.clearPlaylist", d)
}, Listen.prototype.Plus = function() {
    function a(a) {
        f.on("listen.plusNext", b), f.on("listen.plusQueue", c), f.on("listen.overlayCloseStart", d), g = a.data.parentNode.lastChild.querySelector(".overlay-content"), h = a.data.getAttribute("data-id");
        var i;
        i = e(h) > -1 ? CBC.APP.Listen.Templates["Plus.html"]({
            audioId: h,
            queueText: "Remove from My Radio"
        }) : CBC.APP.Listen.Templates["Plus.html"]({
            audioId: h,
            queueText: "Add to My Radio"
        }), g.insertAdjacentHTML("afterbegin", i), f.trigger("listen.overlayContentRendered")
    }

    function b(a) {
        CBC.APP.Caffeine.getInstance(f.cfg.LISTEN_PLAYER_CIID).queueNext({
            audioId: h
        }), f.trigger("listen.overlayCloseStart")
    }

    function c(a) {
        var b = e(h);
        b > -1 ? CBC.APP.Caffeine.getInstance(f.cfg.LISTEN_PLAYER_CIID).session.removeItemFromQueue(b) : CBC.APP.Caffeine.getInstance(f.cfg.LISTEN_PLAYER_CIID).queueEnd({
            audioId: h
        }), f.trigger("listen.overlayCloseStart")
    }

    function d() {
        f.off("listen.plusNext", b), f.off("listen.plusQueue", c), f.off("listen.overlayCloseStart", d), f.trigger("listen.overlayContentClosed")
    }

    function e(a) {
        return CBC.APP.Caffeine.getInstance(f.cfg.LISTEN_PLAYER_CIID).session.getQueue().indexOf(a)
    }
    var f = CBC.APP.Listen,
        g = null,
        h = null;
    f.on("listen.Plus", a)
}, Listen.prototype.RecentClips = function() {
    function a(a) {
        var c = a.data,
            d = c.getAttribute("data-show"),
            e = document.querySelector("[data-shownode=show-" + d + "]");
        e && b(c, e, d)
    }

    function b(a, b, d) {
        b.classList.contains("expanded") ? (b.classList.remove("expanded"), a.setAttribute("aria-expanded", "false"), a.innerHTML = "Show Recent Audio") : (b.classList.contains("loaded") ? b.classList.add("expanded") : (b.classList.add("loading"), c(b, d)), a.setAttribute("aria-expanded", "true"), a.innerHTML = "Hide Recent Audio")
    }

    function c(a, b) {
        var c = a.querySelector(".showlist-recentcontainer"),
            e = new XMLHttpRequest;
        e.open("GET", "/listen/shows/" + b + "/recentclips/?format=json"), e.onreadystatechange = function(b) {
            if (4 == e.readyState) try {
                var f = JSON.parse(e.responseText);
                c.innerHTML = f.html, a.classList.remove("loading"), a.classList.add("loaded"), a.classList.add("expanded"), d.trigger("listen.recentClipsFetched")
            } catch (g) {}
        }, e.setRequestHeader("X-Requested-With", "XMLHttpRequest"), e.send()
    }
    var d = CBC.APP.Listen;
    d.on("listen.recentClips", a)
}, Listen.prototype.Router = function() {
    function a(a) {
        j(window.location.pathname + "?format=json")
    }

    function b(a) {
        var b = a.target,
            e = b.nodeName.toLowerCase();
        "button" === e && c(a), ("a" === e || (b = g(b, "a"))) && d(a, b)
    }

    function c(a) {
        var b = a.target.getAttribute("data-btntype");
        a.preventDefault(), console.log("listen."+b), console.log(a.target), k.trigger("listen." + b, a.target)

        if ( b == "timetagPlay" ) {
            elm = document.getElementById("icon-play");
            if (!elm.classList.contains('playing')) { window.CF_START_TIME = parseInt(a.target.getAttribute("data-timestart")); document.getElementById("icon-play").click(); } // set CF_START_TIME

//            console.log(document.getElementsByTagName("audio")[0]);
            document.getElementsByTagName("audio")[0].currentTime = parseInt(a.target.getAttribute("data-timestart"));
        }
        console.log("1AB");
    }

    function d(a, b) {
        var c = window.location.origin + "/listen/",
            d = b.getAttribute("data-linktype");
        a.button || (d ? "toc" != d && (a.preventDefault(), k.trigger("listen." + d, b)) : b.href.substring(0, c.length) === c ? (a.preventDefault(), k.trigger("listen.closeEverything"), k.trigger("listen.navigateToPage", b.href)) : "_self" !== b.target && (b.target = "_blank"))
    }

    function e(a) {
        a.preventDefault();
        var b = a.data.parentNode,
            c = b.querySelector("input[name=query]").value,
            d = b.action + "/?query=" + encodeURI(c);
        k.trigger("listen.navigateToPage", d)
    }

    function f(a) {
        switch (a.keyCode || a.which) {
            case 9:
                k.trigger("listen.focusChange", document.activeElement);
                break;
            case 27:
                k.trigger("listen.closeEverything")
        }
    }

    function g(a, b) {
        for (b = b.toLowerCase(); a;) {
            if (1 == a.nodeType && a.nodeName.toLowerCase() == b) return a;
            a = a.parentNode
        }
        return null
    }

    function h(a) {
        function b(a) {
            k.off("listen.pageFetched", b);
            var d = c.replace("/?format=json", "");
            d = d.replace("&format=json", ""), "/listen/live" != d && "/listen/live/radio1" != d && "/listen/live/radio2" != d && "/listen/live/cbcmusic" != d && (window.history.pushState({}, "", d), document.title = "CBC Listen | " + a.data.meta.title), window.scrollTo(0, 0), k.trigger("listen.pageFetchedCompleted", a)
        }
        var c, d = a.data;
        c = -1 === d.indexOf("?") ? d + "/?format=json" : d + "&format=json", k.on("listen.pageFetched", b), j(c)
    }

    function i(a) {
        a.data.meta.tracking && ("live" != a.data.meta.tracking.subsection1 || a.data.meta.tracking.subsection2) && (CBC.APP.SC.DTM.DATA = a.data.meta.tracking, CBC.APP.SC.PageTracker.init())
    }

    function j(a) {
        document.body.classList.add("loading");
        var b = new XMLHttpRequest;
        b.open("GET", a), b.onreadystatechange = function(a) {
            if (4 == b.readyState) {
                try {
                    k.trigger("listen.pageFetched", JSON.parse(b.responseText))
                } catch (d) {
                    var c = CBC.APP.Listen.Templates["ErrorGeneric.html"]();
                    k.trigger("listen.pageFetched", {
                        html: c
                    })
                }
                document.body.classList.remove("loading")
            }
        }, b.setRequestHeader("X-Requested-With", "XMLHttpRequest"), b.send()
    }
    var k = CBC.APP.Listen;
    k.on("click", b), k.on("keyup", f), k.on("listen.search", e), k.on("listen.navigateToPage", h), k.on("listen.pageFetched", i), window.addEventListener("popstate", a)
}, Listen.prototype.Search = function() {
    function a(a) {
        if (document.activeElement === r) return void(40 === b && n(1));
        var b = a.keyCode || a.which;
        switch (b) {
            case 38:
            case 75:
                n(-1);
                break;
            case 40:
                n(1);
                break;
            case 74:
                document.activeElement !== r && n(1)
        }
    }

    function b(a) {
        A != r.value && f()
    }

    function c() {
        u ? r.value.length > 0 && i() : d()
    }

    function d() {
        u = !0;
        var a = new XMLHttpRequest;
        a.open("GET", "/listen/search/shows-filter?format=json"), a.onreadystatechange = function(b) {
            if (4 == a.readyState) try {
                p.trigger("listen.search.showListFetched", JSON.parse(a.responseText))
            } catch (c) {
                u = !1
            }
        }, a.setRequestHeader("X-Requested-With", "XMLHttpRequest"), a.send()
    }

    function e(a) {
        p.off("listen.search.showListFetched", e), v = a.data.meta, r.value.length > 0 && f()
    }

    function f() {
        s = [], t = "", g(), window.clearTimeout(x), r.value.length > 3 && (x = window.setTimeout(h, z)), i(), A = r.value
    }

    function g() {
        if (0 !== r.value.length) {
            for (var a = r.value.toLowerCase(), b = 0; b < v.length; b++) {
                var c = v[b].title.toLowerCase();
                if (a.length < 4) {
                    if (a === c) {
                        s.push(v[b]);
                        break
                    }
                } else {
                    var d = !1;
                    if (c.indexOf(a) >= 0) d = !0;
                    else
                        for (var e = v[b].hosts, f = 0; f < e.length; f++) {
                            var g = e[f].name.toLowerCase();
                            g.indexOf(a) >= 0 && (d = !0)
                        }
                    d && s.push(v[b])
                }
            }
            s.sort(function(b, c) {
                var d = b.title.toLowerCase().indexOf(a),
                    e = c.title.toLowerCase().indexOf(a);
                return -1 === e ? -1 : d < e ? -1 : d > e ? 1 : 0
            })
        }
    }

    function h() {
        var a = "/listen/search/clips-filter?query=" + r.value + " &format=json";
        y && (y.abort(), y = null), y = new XMLHttpRequest, y.open("GET", a), y.onreadystatechange = function(a) {
            if (4 == y.readyState) {
                try {
                    var b = JSON.parse(y.responseText);
                    t = b.html, i()
                } catch (c) {
                    t = ""
                }
                window.clearTimeout(x), x = null, y = null
            }
        }, y.setRequestHeader("X-Requested-With", "XMLHttpRequest"), y.send()
    }

    function i() {
        if (t || s) {
            w || j();
            for (var a = "", b = 0; b < s.length; b++) {
                var c = s[b],
                    d = c.title;
                c.hosts && c.hosts.length && (d += " with " + c.hosts.map(function(a) {
                    return a.name
                }).join(" and ")), c.titleFormatted = d, a += CBC.APP.Listen.Templates["SearchOverlayResult.html"](c)
            }
            var e = q.querySelector(".shows-results-list");
            0 === s.length ? e.parentNode.classList.add("empty") : e.parentNode.classList.remove("empty"), e.innerHTML = a;
            var f = q.querySelector(".clips-results-list");
            f.innerHTML = t || (x ? CBC.APP.Listen.Templates["SearchResultsLoading.html"]() : ""), p.trigger("listen.searchResultsUpdated")
        }
    }

    function j() {
        p.trigger("listen.toggleOverlay", q.querySelector(".search-box")), w = !0, q.querySelector(".overlay-content").innerHTML = CBC.APP.Listen.Templates["SearchOverlay.html"](), p.on("listen.overlayCloseStart", o), p.trigger("listen.overlayContentRendered")
    }

    function k() {
        y && (y.abort(), y = null), window.clearTimeout(x), x = null
    }

    function l(a, b) {
        return a.classList ? a.classList.contains(b) ? a : l(a.parentNode, b) : null
    }

    function m(a) {
        w && (q.contains(document.activeElement) || o())
    }

    function n(a) {
        var b = q.querySelectorAll(".search-result-inline, .medialist-item"),
            c = l(document.activeElement, "search-result-inline");
        if (c) {
            for (var d = 0; d < b.length; d++)
                if (b[d] === c && b[d + a]) {
                    b[d + a].querySelector("a").focus();
                    break
                }
        } else b[0].querySelector("a").focus()
    }

    function o() {
        w && (p.off("listen.overlayCloseStart", o), p.trigger("listen.overlayContentClosed"), k(), w = !1)
    }
    var p = CBC.APP.Listen,
        q = document.querySelector("#SearchContainer"),
        r = q.querySelector("input[name=query]"),
        s = [],
        t = "",
        u = !1,
        v = [],
        w = !1,
        x = null,
        y = null,
        z = 750,
        A = "";
    p.on("listen.search.showListFetched", e), p.on("listen.pageFetched", o), p.on("listen.closeEverything", k), p.on("listen.focusChange", m), r.addEventListener("focus", c), q.addEventListener("keyup", a), r.addEventListener("keyup", b)
}, Listen.prototype.Share = function() {
    function a(a) {
        o.on("listen.closeShareOverlay", c), o.on("listen.shareFacebook", b), o.on("listen.shareTwitter", b), o.on("listen.shareLink", b), o.on("listen.shareEmbed", b), o.on("listen.overlayCloseStart", f), p = a.data.parentNode.lastChild.querySelector(".overlay-content"), g = a.data.dataset.url, h = a.data.dataset.audioid, i = a.data.dataset.liveradioid, j = a.data.dataset.sharemessage, k = a.data.dataset.sharesubject, l = a.data.dataset.sharetweet, m = encodeURI(g);
        var time = ''; var elm = document.getElementsByTagName("audio"); if (elm.length > 0 && elm[0].currentTime > 1) { time = "/?start=" + (document.getElementsByTagName("audio")[0].currentTime - 1) }
        var d = CBC.APP.Listen.Templates["Share.html"]({
            shareurl: g + time,
            audioid: h,
            liveradioid: i,
            sharemessage: j,
            sharesubject: k,
            sharetweet: l,
            encodedurl: m
        });
        p.insertAdjacentHTML("afterbegin", d), o.trigger("listen.overlayContentRendered");
        var e = document.getElementById("share-icon");
        e.onkeydown = function(a) {
            return a || (a = window.event), 32 === a.keyCode && (a.preventDefault(), e.click()), !0
        }
    }

    function b(a) {
        var b = a.data;
        switch (b.className) {
            case "share-icon icon-facebook":
            case "share-icon icon-twitter":
                window.open(b.getAttribute("data-url"));
                break;
            case "share-icon icon-link":
            case "share-icon icon-embed":
                d(b.parentNode)
        }
    }

    function c(a) {
        e(a.data.parentNode)
    }

    function d(a) {
        n && e(n), n = a;
        var b = a.querySelector(".share-icon"),
            c = a.querySelector("input");
        b.classList.add("active"), c.select(), c.focus()
    }

    function e(a) {
        var b = a.querySelector(".share-icon");
        b.classList.remove("active"), b.focus(), n = null
    }

    function f() {
        o.off("listen.closeShareOverlay", c), o.off("listen.shareFacebook", b), o.off("listen.shareTwitter", b), o.off("listen.shareEmail", b), o.off("listen.shareLink", b), o.off("listen.shareEmbed", b), o.off("listen.overlayCloseStart", f), o.trigger("listen.overlayContentClosed")
    }
    var g, h, i, j, k, l, m, n, o = CBC.APP.Listen,
        p = null;
    o.on("listen.Share", a)
}, Listen.prototype.TimeTag = function() {
    function a(a) {
        o.on("listen.closeTimeTagOverlay", c), o.on("listen.timeLink", b), o.on("listen.overlayCloseStart", f), p = a.data.parentNode.lastChild.querySelector(".overlay-content"), g = a.data.dataset.url, l = a.data.dataset.title, nn = a.data.dataset.times;
        var d = CBC.APP.Listen.Templates["TimeTag.html"]({
            times: nn,
        });
        p.insertAdjacentHTML("afterbegin", d), o.trigger("listen.overlayContentRendered");
    }

    function b(a) {
        var b = a.data;
        switch (b.className) {
            case "share-icon icon-facebook":
            case "share-icon icon-twitter":
                window.open(b.getAttribute("data-url"));
                break;
            case "share-icon icon-link":
            case "share-icon icon-embed":
                d(b.parentNode)
        }
    }

    function c(a) {
        e(a.data.parentNode)
    }

    function d(a) {
        n && e(n), n = a;
        var b = a.querySelector(".share-icon"),
            c = a.querySelector("input");
        b.classList.add("active"), c.select(), c.focus()
    }

    function f() {
        o.off("listen.closeTimeTagOverlay", c), o.off("listen.timeLink", b), o.off("listen.overlayCloseStart", f), o.trigger("listen.overlayContentClosed")
    }
    var o = CBC.APP.Listen,
        p = g = l = nn = null;
    o.on("listen.TimeTag", a)
};
//# sourceMappingURL=Listen.js.map

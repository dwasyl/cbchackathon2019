this["CBC"] = this["CBC"] || {};
this["CBC"]["APP"] = this["CBC"]["APP"] || {};
this["CBC"]["APP"]["Listen"] = this["CBC"]["APP"]["Listen"] || {};
this["CBC"]["APP"]["Listen"]["Templates"] = this["CBC"]["APP"]["Listen"]["Templates"] || {};

this["CBC"]["APP"]["Listen"]["Templates"]["ClipDescription.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = CBC.APP.Listen._.escape;with (obj) {__p += '<div class="clipdescription-ui animated fadeInDown">' +((__t = ( description )) == null ? '' : __t) +'</div>';}return __p};

this["CBC"]["APP"]["Listen"]["Templates"]["ErrorGeneric.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = CBC.APP.Listen._.escape;with (obj) {__p += '<div class="error-container center">\n\t<h1>Oops...</h1>\n\t<p>We couldn\'t load this page.</p>\n</div>';}return __p};

this["CBC"]["APP"]["Listen"]["Templates"]["NowPlaying.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = CBC.APP.Listen._.escape;with (obj) {__p += '<div class="nowplaying-ui">\n    <p class="program-title">' +((__t = ( program )) == null ? '' : __t) +'</p>\n    <p>' +((__t = ( startTime )) == null ? '' : __t) +' - ' +((__t = ( endTime )) == null ? '' : __t) +'</p>\n</div>';}return __p};

this["CBC"]["APP"]["Listen"]["Templates"]["Overlay.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = CBC.APP.Listen._.escape;with (obj) {__p += '<div class="overlay-ui">\n    <div class="overlay-pointer"></div>\n    <div class="overlay-content"></div>\n</div>';}return __p};

this["CBC"]["APP"]["Listen"]["Templates"]["Plus.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = CBC.APP.Listen._.escape;with (obj) {__p += '<div class="plus-ui">\n    <button class="plus-playnext" data-btntype="plusNext" data-audioid="' +((__t = ( audioId )) == null ? '' : __t) +'">Play Next</button>\n    <button class="plus-queue" data-btntype="plusQueue" data-audioid="' +((__t = ( audioId )) == null ? '' : __t) +'">' +((__t = ( queueText )) == null ? '' : __t) +'</button>\n</div>';}return __p};

this["CBC"]["APP"]["Listen"]["Templates"]["SearchOverlay.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = CBC.APP.Listen._.escape;with (obj) {__p += '<div class="search-overlay" style="height: ' +((__t = ( window.innerHeight * 0.8 )) == null ? '' : __t) +'px">\n    <div class="results">\n        <h2 class="showlist-heading">Matching Shows</h2>\n        <ul class="shows-results-list">\n        </ul>\n    </div>\n\n    <div class="results">\n        <h2 class="showlist-heading">Matching Clips</h2>\n        <ul class="medialist clips-results-list">\n        </ul>\n    </div>\n</div>';}return __p};

this["CBC"]["APP"]["Listen"]["Templates"]["SearchOverlayResult.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = CBC.APP.Listen._.escape;with (obj) {__p += '<li class="search-result-inline">\n    <a href="/listen/shows/' +((__t = ( slugTitle )) == null ? '' : __t) +'">\n        <div class="show-image">\n            <img class="round" src="' +((__t = ( hostImage )) == null ? '' : __t) +'" alt="" />\n        </div>\n        <div class="show-title">\n            ' +((__t = ( titleFormatted )) == null ? '' : __t) +'\n        </div>\n    </a>\n</li>';}return __p};

this["CBC"]["APP"]["Listen"]["Templates"]["SearchResultsLoading.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = CBC.APP.Listen._.escape;with (obj) {__p += '<li class="medialist-item">\n    <div class="medialist-item-status">\n        <div class="icon-loading"><span></span><span></span></div>\n    </div>\n    <div class="medialist-item-meta">\n        <div class="medialist-item-title">\n            <a>Searching...</a>\n        </div>\n    </div>\n</li>\n';}return __p};

this["CBC"]["APP"]["Listen"]["Templates"]["Share.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = CBC.APP.Listen._.escape, __j = Array.prototype.join;function print() { __p += __j.call(arguments, '') }with (obj) {__p += '<div class="share-ui animated fadeInDown">\n    <div class="share-content">\n        <div class="share-option">\n            <button class="share-icon icon-facebook" data-btntype="shareFacebook" data-url="https://www.facebook.com/sharer/sharer.php?u=' +((__t = ( shareurl )) == null ? '' : __t) +'">Facebook</button>\n        </div>\n        <div class="share-option">\n            <button class="share-icon icon-twitter" data-btntype="shareTwitter" data-url="http://www.twitter.com/intent/tweet?url=' +((__t = ( encodedurl )) == null ? '' : __t) +'&text=' +((__t = ( sharetweet )) == null ? '' : __t) +'">Twitter</button>\n        </div>\n        <div class="share-option">\n            <a role="button" id="share-icon" class="share-icon icon-email" target="_blank" href="mailto:?subject=' +((__t = ( sharesubject )) == null ? '' : __t) +'&body=' +((__t = ( sharemessage )) == null ? '' : __t) +'"></a>\n        </div>\n        <div class="share-option">\n            <button class="share-icon icon-link" data-btntype="shareLink">Link</button>\n            <label>\n                Audio Link:\n                <input name="link" class="link" value="' +((__t = ( shareurl )) == null ? '' : __t) +'" />\n            </label>\n            <button class="icon-close" data-btntype="closeShareOverlay">Close</button>\n        </div>\n        '; if (audioid || liveradioid) { ;__p += '\n        <div class="share-option">\n            <button class="share-icon icon-embed" data-btntype="shareEmbed">Embed</button>\n            <label>\n                Embed Code:\n                <input\n                    name="embedcode"\n                    class="embedcode"\n                    value=\'<iframe src="http://www.cbc.ca/i/caffeine/syndicate/?' +((__t = ( (audioid) ? "audioId" : "liveRadioId" )) == null ? '' : __t) +'=' +((__t = ( audioid || liveradioid )) == null ? '' : __t) +'" width="620" height="138" frameborder="0" allowfullscreen></iframe>\'\n                />\n            </label>\n            <button class="icon-close" data-btntype="closeShareOverlay">Close</button>\n        </div>\n        '; } ;__p += '\n    </div>\n</div>';}return __p};

this["CBC"]["APP"]["Listen"]["Templates"]["TimeTag.html"] = function(obj) {
    obj || (obj = {});
    var __t, __p = '',
        __e = CBC.APP.Listen._.escape;
    function format_time(i) {
        var sec_num = parseInt(i, 10); // don't forget the second param
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (seconds < 10) {seconds = "0"+seconds;}

        if (hours == 0) {return minutes + ':' + seconds + ' minutes';}
        if (hours == 0 && minutes == 0) {return '00:' + seconds + ' seconds';}
        return hours + ':' + minutes + ':' + seconds;
    };
    with(obj) {
        times_list = JSON.parse("[" + times + "]")
        __p += '<div class="timetag-ui">\n    ';
        for (var i = 0; i < times_list.length; i++) {
            __p += '<button class="timetag-link" data-btntype="timetagPlay" data-timestart="' + ((__t = (times_list[i])) == null ? '' : __t) + '">Play at ' + format_time(times_list[i]) + '</button>\n    ';
        }
        __p += '</div>';
    }
    return __p
};

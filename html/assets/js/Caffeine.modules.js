// CBC Caffeine Player
// version 14.6.2
// built on Wednesday, January 23rd, 2019, 4:02:30 PM
CBC.APP.Caffeine.addModule("Constants.ErrorList", {
    FEED_FETCH: {
        code: 1,
        message: "The clip we tried to show you isn't available.",
        debug: "API error: response code wasn't 200.",
        critical: !0,
        track: !0
    },
    EMBED_INVALID: {
        code: 2,
        message: "The clip embed value is invalid.",
        debug: "The clip embed value is invalid.",
        critical: !1,
        track: !1
    },
    FEED_PARSE: {
        code: 3,
        message: "We ran into a problem loading this clip. Refresh the page to try again.",
        debug: "Caffeine: could not parse API feed",
        critical: !0,
        track: !0
    },
    CLIP_UNAVAILABLE: {
        code: 4,
        message: "The clip we tried to show you isn't available.",
        debug: "API feed contained 0 items.",
        critical: !0,
        track: !0
    },
    DOM_FEATURE_UNAVAILABLE: {
        code: 6,
        message: "Your browser isn't capable of using this media player.",
        debug: "Required DOM method or feature is not available in this browser",
        critical: !0,
        track: !0
    },
    INJECT_PLAYER_FAILED: {
        code: 7,
        message: "We're having trouble loading this clip right now. Please try again later.",
        debug: "Error inserting Caffeine Player into DOM. Check that the surrounding markup is valid HTML and that it is valid to insert a div inside this element",
        critical: !0,
        track: !0
    },
    SLOW_PLATFORM: {
        code: 8,
        message: "We're having trouble loading this clip right now. Please try again later.",
        debug: "ThePlatform took longer than allowed to load",
        critical: !0,
        track: !0
    },
    NO_HEARTBEAT: {
        code: 9,
        message: "VideoHeartbeat Error",
        debug: "CBC.APP.SC.VideoHeartbeat does not exist",
        critical: !1,
        track: !1
    },
    DUPLICATE_CIID: {
        code: 11,
        message: "We're having trouble loading this clip right now. Please try again later.",
        debug: "A duplicate ciid was provided to Caffeine. CIIDs must be unique.",
        critical: !0,
        track: !1
    },
    INVALID_CIID: {
        code: 12,
        message: "We're having trouble loading this clip right now. Please try again later.",
        debug: "An invalid ciid was provided to Caffeine. CIIDs must be alphanumeric.",
        critical: !0,
        track: !1
    },
    INVALID_CONFIG: {
        code: 15,
        message: "The clip we tried to show you isn't available.",
        debug: "There's an error with the configuration supplied to Caffeine.",
        critical: !0,
        track: !1
    },
    CONTAINER_SELECTOR_NULL: {
        code: 16,
        message: "Could not find the specified container to add Caffeine to.",
        debug: "Query selector for caffeine container failed.",
        critical: !0,
        track: !1
    },
    TRACKING_ERROR: {
        code: 17,
        message: "VideoHeartbeat Error",
        debug: "CBC.APP.SC.VideoHeartbeat function threw an error",
        critical: !1,
        track: !1
    },
    MEDIA_ERROR: {
        code: 19,
        message: "Technical problems prevent us from playing this media.",
        debug: "The playback element threw and error that could not be recovered from",
        critical: !0,
        track: !0
    },
    AD_EXTENSION_ERROR: {
        code: 20,
        message: "There as an error with ad playback.",
        debug: "There was a problem loading or playing ads with the IMA SDK.",
        critical: !1,
        track: !1
    },
    BLOCKED: {
        code: 21,
        message: "We're sorry, this content is not available in your location. If you believe you have received this message in error please <a href='//www.cbc.ca/connects/'>contact us</a>",
        debug: "This content is unavailable in your area.",
        critical: !0,
        track: !1
    },
    AKAMAI_ANALYTICS_ERROR: {
        code: 22,
        message: "Akamai Analytics Error",
        debug: "There was a problem loading Akamai Analytics",
        critical: !1,
        track: !1
    },
    TIMEOUT_ERROR: {
        code: 23,
        message: "A media timeout has prevented us from playing this media.",
        debug: "Playback was stalled for too long, gave up",
        critical: !0,
        track: !0
    },
    HLSJS_NETWORK_ERROR: {
        code: 24,
        message: "A network timeout has prevented us from playing this media.",
        debug: "HLS.js could not recover from a network error",
        critical: !0,
        track: !0
    },
    HLSJS_DEFAULT_ERROR: {
        code: 25,
        message: "Technical problems prevent us from playing this media.",
        debug: "HLS.js had an uncaught error",
        critical: !1,
        track: !0
    },
    NO_COMPATIBLE_SOURCES: {
        code: 26,
        message: "We cannot play this media in your browser",
        debug: "All sources were attempted, and none were able to play in the browser",
        critical: !0,
        track: !1
    },
    AD_FAILURE: {
        code: 27,
        message: "Could not play ad",
        debug: "AdsErrorManager canceled a misbehaving ad.",
        critical: !1,
        track: !0
    },
    SMIL_ERROR: {
        code: 28,
        message: "Technical problems prevent us from playing this media.",
        debug: "Could not get metadata for this media.",
        critical: !0,
        track: !0
    },
    EXCESSIVE_PLAYBACK_ISSUES: {
        code: 29,
        message: "There have been many playback issues in this session.",
        debug: "There have been many playback issues in this session.",
        critical: !1,
        track: !0
    },
    EXPIRED_CONTENT: {
        code: 30,
        message: "This content is no longer available for streaming.",
        debug: "This content is no longer available for streaming.",
        critical: !0,
        track: !1
    }
}, !0);
! function() {
    for (var e in CBC.APP.Caffeine.Constants.ErrorList) CBC.APP.Caffeine.Constants.ErrorList[e].toString = function() {
        return this.message
    }
}();
CBC.APP.Caffeine.addModule("Helpers.ErrorList", CBC.APP.Caffeine.getModule("Constants.ErrorList"), !0);
CBC.APP.Caffeine.addModule("Constants.Event", {
    CAFFEINE_READY: "caffeine.ready",
    CAFFEINE_ERROR: "caffeine.error",
    CAFFEINE_INSTANCEDESTROYED: "caffeine.instanceDestroyed",
    MODELSMANAGER_COMPLETED: "modelsmanager.completed",
    REQUESTED_MODELS_READY: "handlers.requestedmodelsready",
    MODEL_READY: "model.ready",
    PLAYER_SRCSET: "player.srcset",
    PLAYER_SRCERR: "player.srcerr",
    PLAYER_PLAYINTENT: "player.playIntent",
    PLAYER_INITIALPLAY: "player.initialPlay",
    PLAYER_STATUSCHANGE: "player.statuschange",
    PLAYER_READY: "player.ready",
    PLAYER_PLAYING: "player.playing",
    PLAYER_PAUSED: "player.paused",
    PLAYER_STOPPED: "player.stopped",
    PLAYER_TIMEUPDATED: "player.timeupdate",
    PLAYER_BUFFERUPDATED: "player.bufferupdate",
    PLAYER_METADATALOADED: "player.metadataloaded",
    PLAYER_VOLUMECHANGED: "player.volumechange",
    PLAYER_ENDED: "player.ended",
    PLAYER_COMPLETED: "player.completed",
    PLAYER_BITRATECHANGED: "player.bitratechange",
    PLAYER_SEEKSTART: "player.seekstart",
    PLAYER_SEEKEND: "player.seekend",
    PLAYER_DOCKED: "player.docked",
    PLAYER_UNDOCKED: "player.undocked",
    PLAYER_CUE_CHANGED: "player.cuechanged",
    PLAYER_RESIZED: "player.resized",
    PLAYER_PLAYBACK_ISSUE: "player.playbackissue",
    PLAYER_PLAYBACK_ISSUE_RESOLVED: "player.playbackissueresolved",
    CHAPTER_START: "chapter.start",
    CHAPTER_END: "chapter.end",
    INSTANCE_TRACKSKIPPED: "instance.trackskipped",
    HLSJS_ERRORED: "hlsjs.error",
    HANDLER_MEDIA_URL_UPDATED: "handler.media_url_updated",
    SESSION_CLIPREADY: "session.clipready",
    SESSION_QUEUEUPDATED: "session.queueUpdated",
    SESSION_FULLSCREEN_ON: "session.fullScreenOn",
    SESSION_FULLSCREEN_OFF: "session.fullScreenOff",
    SESSION_PLAYERCREATIONSTARTED: "session.playercreationstarted",
    ADS_INITIALIZED: "ads.initialized",
    ADS_LOADED: "ads.loaded",
    ADS_STARTED: "ads.started",
    ADS_FIRSTQUARTILE: "ads.firstquartile",
    ADS_MIDPOINT: "ads.midpoint",
    ADS_THIRDQUARTILE: "ads.thirdquartile",
    ADS_COMPLETE: "ads.complete",
    ADS_STOPPED: "ads.stopped",
    ADS_ERRORED: "ads.error",
    ADS_BREAK_START: "ads.breakstart",
    ADS_BREAK_COMPLETE: "ads.breakcomplete",
    ADS_PAUSED: "ads.paused",
    ADS_RESUMED: "ads.resumed",
    UI_INDIRECTUSERACTION: "ui.indirectUserAction"
}, !0);
CBC.APP.Caffeine.addModule("Helpers.Event", CBC.APP.Caffeine.getModule("Constants.Event"), !0);
CBC.APP.Caffeine.addModule("Constants.PlayerStatus", {
    READY: "ready",
    LOADING: "loading",
    PLAYING: "playing",
    ADVERTISING: "advertising",
    ADVERTISING_PAUSED: "advertising-paused",
    PAUSED: "paused"
}, !0);
CBC.APP.Caffeine.addModule("Core.Components.Ads_DAI", function(e, i) {
    "use strict";
    var a, r, o, c = this,
        t = CBC.APP.Caffeine,
        s = t._,
        u = t.Constants.Event,
        n = 0,
        l = !1;
    d();

    function d() {
        i.setClickElement(e);
        f(!0)
    }

    function f(e) {
        if (i) {
            var t = google.ima.dai.api.StreamEvent.Type,
                n = e ? "addEventListener" : "removeEventListener",
                a = s.values(t);
            i[n](a, p, !1);
            c[n = e ? "on" : "off"](u.PLAYER_CUE_CHANGED, A)
        }
    }

    function A(e) {
        if ("TXXX" === e.key && 0 === e.data.indexOf("google")) {
            var t = {};
            t[e.key] = e.data;
            i.onTimedMetadata(t)
        }
    }

    function p(e) {
        if (!l) {
            var t = google.ima.dai.api.StreamEvent.Type;
            try {
                switch (e.type) {
                    case t.ERROR:
                        c.trigger(u.ADS_ERRORED);
                        break;
                    case t.STARTED:
                        ! function(e) {
                            var t = e.getAd(),
                                n = t.getAdId(),
                                a = t.getDuration(),
                                i = t.getTitle(),
                                r = t.getAdPodInfo(),
                                o = r.getAdPosition(),
                                s = r.getTotalAds();
                            c.trigger(u.ADS_LOADED, t);
                            1 === o && g();
                            var l = {
                                id: n,
                                length: a,
                                position: o,
                                name: i,
                                totalAds: s
                            };
                            c.trigger(u.ADS_STARTED, l)
                        }(e);
                        break;
                    case t.COMPLETE:
                        ! function(e) {
                            var t = e.getAd().getAdId();
                            n += e.getAd().getDuration();
                            c.trigger(u.ADS_COMPLETE, t);
                            r = null
                        }(e);
                        break;
                    case t.FIRST_QUARTILE:
                        c.trigger(u.ADS_FIRSTQUARTILE);
                        break;
                    case t.MIDPOINT:
                        c.trigger(u.ADS_MIDPOINT);
                        break;
                    case t.THIRD_QUARTILE:
                        c.trigger(u.ADS_THIRDQUARTILE);
                        break;
                    case t.AD_BREAK_STARTED:
                        g(e);
                        break;
                    case t.AD_BREAK_ENDED:
                        m(e);
                        break;
                    case t.AD_PROGRESS:
                        ! function(e) {
                            var t = e.getStreamData().adProgressData,
                                n = t.adBreakDuration;
                            if (n !== a) {
                                a = n;
                                ! function() {
                                    var e = 1e3 * Math.ceil(a);
                                    o = window.clearTimeout(o);
                                    r && (o = window.setTimeout(E, e))
                                }()
                            }
                            r = t
                        }(e);
                        break;
                    case t.CLICK:
                }
            } catch (e) {
                window.console.error(e)
            }
        }
    }

    function g(e) {
        !0;
        c.trigger(u.ADS_BREAK_START);
        c.player.volume *= .667
    }

    function m(e) {
        n = 0;
        !1;
        o = window.clearTimeout(o);
        c.trigger(u.ADS_BREAK_COMPLETE);
        c.player.volume *= 1 / .667
    }

    function E() {
        o = window.clearTimeout(o);
        m()
    }
    return {
        destroy: function() {
            f(!1);
            l = !0
        },
        getRemainingTime: function() {
            if (r) return r.adBreakDuration - n - r.currentTime;
            return -1
        },
        manageAds: d,
        pause: function() {
            document.querySelector(".playbackelement video").pause();
            c.trigger(u.ADS_PAUSED)
        },
        resume: function() {
            c.trigger(u.ADS_RESUMED);
            document.querySelector(".playbackelement video").play()
        }
    }
});
CBC.APP.Caffeine.addModule("Core.Components.Ads_IMA", function(i, e, t) {
    "use strict";
    var n, a, r, s = this,
        o = CBC.APP.Caffeine,
        l = o.Constants.Event,
        c = o.Constants.PlayerStatus,
        u = s.session.config,
        d = s.session.modelsManager.getClip(s.currentClip.id),
        f = !1,
        A = !1,
        p = .5625;
    g();

    function g() {
        if (!n) {
            s.off(l.HANDLER_MEDIA_URL_UPDATED, g);
            t = t || d.getAdUrl() || u.adUrl;
            i.classList.add("ima");
            o.getSettings().AD_URL && (t = o.getSettings().AD_URL);
            if (t) {
                (n = new google.ima.AdsLoader(e)).addEventListener(google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED, C, !1);
                n.addEventListener(google.ima.AdErrorEvent.Type.AD_ERROR, v, !1);
                (a = new google.ima.AdsRequest).adTagUrl = t;
                a.linearAdSlotWidth = 640;
                a.linearAdSlotHeight = 400;
                a.nonLinearAdSlotWidth = 640;
                a.nonLinearAdSlotHeight = 150;
                n.requestAds(a)
            }
        }
    }

    function m(e) {
        var t = e ? "on" : "off";
        s[t](l.PLAYER_RESIZED, w);
        s[t](l.PLAYER_VOLUMECHANGED, N);
        s[t](l.PLAYER_PLAYING, h);
        t = e ? "addEventListener" : "removeEventListener"
    }

    function E(e) {
        if (r) {
            var t = google.ima.AdEvent.Type,
                n = google.ima.AdErrorEvent.Type,
                a = e ? "addEventListener" : "removeEventListener";
            r[a](n.AD_ERROR, P);
            r[a](t.LOADED, y);
            r[a](t.STARTED, _);
            r[a](t.CONTENT_PAUSE_REQUESTED, S);
            r[a](t.CONTENT_RESUME_REQUESTED, R);
            r[a](t.COMPLETE, M);
            r[a](t.PAUSED, D);
            r[a](t.RESUMED, T);
            r[a](t.FIRST_QUARTILE, L);
            r[a](t.MIDPOINT, b);
            r[a](t.THIRD_QUARTILE, I)
        }
    }

    function C(e) {
        r = e.getAdsManager(s.player, {
            restoreCustomPlaybackStateOnAdBreakComplete: !1
        });
        m(!0);
        E(!0);
        s.trigger(l.ADS_INITIALIZED, r);
        s.player.status !== c.READY && h()
    }

    function v(e, t) {
        window.console.error(e.getError().toString());
        q()
    }

    function h() {
        if (!f && r && s.session.capabilities.canAutoplay) {
            var e = google.ima.ViewMode,
                t = i.style.width.match(/\d+/),
                n = i.style.height.match(/\d+/);
            try {
                r.init(t, n, e.NORMAL);
                r.start();
                f = !0
            } catch (e) {}
        }
    }

    function y(e) {
        N();
        var t = e.getAd();
        p = t.getWidth() / t.getHeight() || .5625;
        var n = t.getAdPodInfo();
        if (1 === n.getAdPosition()) {
            s.pause();
            A = !0;
            s.trigger(l.ADS_BREAK_START, n.getPodIndex());
            w()
        }
        s.trigger(l.ADS_LOADED, t)
    }

    function P(e) {
        window.console.error(e.getError().toString());
        s.trigger(l.ADS_ERRORED)
    }

    function _(e) {
        var t = e.getAd(),
            n = t.getAdId(),
            a = t.getDuration(),
            i = t.getTitle(),
            r = t.getAdPodInfo(),
            o = {
                id: n,
                length: a,
                position: r.getAdPosition(),
                name: i,
                totalAds: r.getTotalAds()
            };
        s.trigger(l.ADS_STARTED, o);
        s.player.status === c.ADVERTISING_PAUSED && CBC.APP.Caffeine._.delay(k, 10)
    }

    function S(e) {
        switch (s.player.status) {
            case c.PLAYING:
                s.pause()
        }
    }

    function R(e) {
        U()
    }

    function D(e) {
        s.trigger(l.ADS_PAUSED)
    }

    function T(e) {
        s.trigger(l.ADS_RESUMED)
    }

    function L() {
        s.trigger(l.ADS_FIRSTQUARTILE)
    }

    function b() {
        s.trigger(l.ADS_MIDPOINT)
    }

    function I() {
        s.trigger(l.ADS_THIRDQUARTILE)
    }

    function M(e) {
        s.player.status === c.ADVERTISING_PAUSED && T();
        var t = e.getAd(),
            n = t.getAdId(),
            a = t.getAdPodInfo(),
            i = a.getAdPosition(),
            r = a.getTotalAds();
        s.trigger(l.ADS_COMPLETE, n);
        i === r && U()
    }
    var w = CBC.APP.Caffeine._.throttle(function(e) {
        if (r) {
            var t, n = i.offsetWidth,
                a = Math.ceil(n * p);
            t = s.fullscreenManager.fullScreen ? google.ima.ViewMode.FULLSCREEN : google.ima.ViewMode.NORMAL;
            r.resize(n, a, t)
        }
    }, 500);

    function N() {
        r && r.setVolume(.667 * s.player.volume)
    }

    function k() {
        r.pause()
    }

    function U() {
        if (A) {
            A = !1;
            s.trigger(l.ADS_BREAK_COMPLETE)
        }
    }

    function q() {
        s.off(l.HANDLER_MEDIA_URL_UPDATED, g);
        E(!1);
        if (n) {
            n.removeEventListener(google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED, C, !1);
            n.removeEventListener(google.ima.AdErrorEvent.Type.AD_ERROR, v, !1);
            n.destroy()
        }
        r && r.destroy();
        r = a = n = null;
        m(f = !1);
        i.classList.remove("ima")
    }
    return {
        getRemainingTime: function() {
            return r ? r.getRemainingTime() : -1
        },
        manageAds: h,
        pause: k,
        resume: function() {
            r.resume()
        },
        destroy: q
    }
});
CBC.APP.Caffeine.addModule("Core.Components.Ads", function(n) {
    "use strict";
    var a, i, r, o, s, l, t, c, u, d, f, A = this,
        e = CBC.APP.Caffeine,
        p = e._,
        g = e.Constants.Event,
        m = e.Constants.PlayerStatus,
        E = e.Helpers.DateTimeFormat,
        C = A.session.config,
        v = "//imasdk.googleapis.com/js/sdkloader/ima3.js",
        h = !1,
        y = !1,
        P = !1;
    A.on(g.HANDLER_MEDIA_URL_UPDATED, L);
    A.on(g.PLAYER_RESIZED, W);
    R() ? _() : CBC.APP.Caffeine.addDependency(v, _, S);

    function _() {
        ! function() {
            var e = CBC.APP.Caffeine.Templates["Ads.html"](),
                t = C.caffeineNode;
            t.insertAdjacentHTML("afterbegin", e);
            a = t.querySelector(".advertisement");
            i = a.querySelector(".ads-video-player");
            r = a.querySelector(".ads-time-remaining");
            o = a.querySelector(".ads-count");
            s = a.querySelector(".ads-close");
            l = a.querySelector(".ads-play-button");
            i.appendChild(n);
            d = new google.ima.AdDisplayContainer(i, n);
            W({
                width: C.caffeineNode.offsetWidth,
                height: .5625 * C.caffeineNode.offsetWidth
            })
        }();
        D(!0);
        A.ima = {
            destroy: Q,
            get playing() {
                return y
            },
            pause: K,
            resume: j
        };
        u && b()
    }

    function S() {
        Q()
    }

    function R() {
        return "object" == typeof google && "object" == typeof google.ima && "string" == typeof google.ima.VERSION
    }

    function D(e) {
        var t = e ? "on" : "off";
        A[t](g.PLAYER_PLAYINTENT, O);
        A[t](g.SESSION_PLAYERCREATIONSTARTED, B);
        A[t](g.PLAYER_ENDED, H);
        A[t](g.PLAYER_STATUSCHANGE, Y, !0);
        A[t](g.ADS_BREAK_START, I, !0);
        A[t](g.ADS_BREAK_COMPLETE, q, !0);
        A[t](g.ADS_STARTED, N, !0);
        A[t](g.ADS_RESUMED, k, !0);
        A[t](g.ADS_LOADED, M, !0);
        A[t](g.ADS_ERRORED, w, !0);
        A[t](g.ADS_COMPLETE, U, !0);
        t = e ? "addEventListener" : "removeEventListener";
        s && s[t]("click", z);
        l && l[t]("click", G);
        document[t]("click", T)
    }

    function T() {
        d && !y && d.initialize();
        document.removeEventListener("click", T)
    }

    function L(e) {
        if (f) {
            f.destroy();
            y = !1
        }
        u = e;
        R() && p.defer(b)
    }

    function b() {
        if (!h) {
            if (!0 === u.extras.isDAI) f = A.initModule("Core.Components.Ads_DAI", i, u.extras.streamManager);
            else {
                var e = A.session.modelsManager.getClip(A.currentClip.id);
                f = A.initModule("Core.Components.Ads_IMA", a, d, e.getAdUrl())
            }
            h = !0
        }
    }

    function I() {
        y = !0
    }

    function M(e) {
        y = !0;
        c = window.setTimeout(V, 1500)
    }

    function w() {
        y = !1;
        F()
    }

    function N(e) {
        F();
        if (0 < e.totalAds) {
            o.innerHTML = p.template(" <%= position %> of <%= totalAds %>")(e)
        } else o.innerHTML = "";
        t = setInterval(x, 333)
    }

    function k() {}

    function U() {
        y = !1;
        clearInterval(t)
    }

    function q() {
        y = !1;
        clearInterval(t)
    }

    function O() {
        !f && u && b()
    }

    function B() {
        y = !1;
        u = null
    }

    function Y(e) {
        switch (e.to) {
            case m.READY:
                break;
            case m.PLAYING:
                y = !1;
                F()
        }
    }

    function H() {
        if (f) {
            window.clearInterval(t);
            f.destroy();
            f = null;
            h = !1;
            if (y) {
                y = !1;
                A.trigger(g.ADS_STOPPED)
            }
        }
    }

    function V() {
        c = window.clearTimeout(c);
        if (!P) {
            l.style.display = "block";
            P = !0;
            A.trigger(g.ADS_PAUSED)
        }
    }

    function F() {
        c = window.clearTimeout(c);
        if (P) {
            l.style.display = "";
            P = !1;
            A.trigger(g.ADS_RESUMED)
        }
    }

    function x() {
        var e = Math.floor(f.getRemainingTime());
        e = E.getFormattedDuration(e);
        r.innerHTML = "(" + e + ")"
    }

    function z(e) {
        H()
    }

    function G(e) {
        F();
        f.manageAds()
    }

    function K() {
        f && f.pause && f.pause()
    }

    function j() {
        P || f && f.resume && f.resume()
    }

    function W(e) {
        if (e && e.width && e.height && a) {
            a.style.width = e.width + "px";
            a.style.height = e.height + "px"
        }
    }

    function Q() {
        H();
        D(!1);
        d && d.destroy();
        a && a.parentNode === C.caffeineNode && C.caffeineNode.removeChild(a);
        A.off(g.HANDLER_MEDIA_URL_UPDATED, L);
        A.off(g.PLAYER_RESIZED, W);
        y = !1;
        delete A.ima
    }
});
CBC.APP.Caffeine.addModule("Core.Components.AutoPlayManager", function(t, n) {
    var a, i = this,
        e = !1,
        r = !1,
        o = !1,
        s = !1,
        l = CBC.APP.Caffeine,
        c = l.Constants.Event,
        u = l.Constants.PlayerStatus;
    ! function() {
        ! function() {
            (a = new Audio).canPlayType("audio/ogg") ? a.src = "data:audio/ogg;base64,T2dnUwACAAAAAAAAAADNFExGAAAAAB5NbckBHgF2b3JiaXMAAAAAAUSsAAAAAAAAgLsAAAAAAAC4AU9nZ1MAAAAAAAAAAAAAzRRMRgEAAADoAmMEDz//////////////////MgN2b3JiaXMvAAAAWGlwaC5PcmcgbGliVm9yYmlzIEkgMjAxNDAxMjIgKFR1cnBha8OkcsOkamlpbikAAAAAAQV2b3JiaXMfQkNWAQAAAQAYY1QpRplS0kqJGXOUMUaZYpJKiaWEFkJInXMUU6k515xrrLm1IIQQGlNQKQWZUo5SaRljkCkFmVIQS0kldBI6J51jEFtJwdaYa4tBthyEDZpSTCnElFKKQggZU4wpxZRSSkIHJXQOOuYcU45KKEG4nHOrtZaWY4updJJK5yRkTEJIKYWSSgelU05CSDWW1lIpHXNSUmpB6CCEEEK2IIQNgtCQVQAAAQDAQBAasgoAUAAAEIqhGIoChIasAgAyAAAEoCiO4iiOIzmSY0kWEBqyCgAAAgAQAADAcBRJkRTJsSRL0ixL00RRVX3VNlVV9nVd13Vd13UgNGQVAAABAEBIp5mlGiDCDGQYCA1ZBQAgAAAARijCEANCQ1YBAAABAABiKDmIJrTmfHOOg2Y5aCrF5nRwItXmSW4q5uacc845J5tzxjjnnHOKcmYxaCa05pxzEoNmKWgmtOacc57E5kFrqrTmnHPGOaeDcUYY55xzmrTmQWo21uaccxa0pjlqLsXmnHMi5eZJbS7V5pxzzjnnnHPOOeecc6oXp3NwTjjnnHOi9uZabkIX55xzPhmne3NCOOecc84555xzzjnnnHOC0JBVAAAQAABBGDaGcacgSJ+jgRhFiGnIpAfdo8MkaAxyCqlHo6ORUuoglFTGSSmdIDRkFQAACAAAIYQUUkghhRRSSCGFFFKIIYYYYsgpp5yCCiqppKKKMsoss8wyyyyzzDLrsLPOOuwwxBBDDK20EktNtdVYY62555xrDtJaaa211koppZRSSikIDVkFAIAAABAIGWSQQUYhhRRSiCGmnHLKKaigAkJDVgEAgAAAAgAAADzJc0RHdERHdERHdERHdETHczxHlERJlERJtEzL1ExPFVXVlV1b1mXd9m1hF3bd93Xf93Xj14VhWZZlWZZlWZZlWZZlWZZlWYLQkFUAAAgAAIAQQgghhRRSSCGlGGPMMeegk1BCIDRkFQAACAAgAAAAwFEcxXEkR3IkyZIsSZM0S7M8zdM8TfREURRN01RFV3RF3bRF2ZRN13RN2XRVWbVdWbZt2dZtX5Zt3/d93/d93/d93/d93/d1HQgNWQUASAAA6EiOpEiKpEiO4ziSJAGhIasAABkAAAEAKIqjOI7jSJIkSZakSZ7lWaJmaqZneqqoAqEhqwAAQAAAAQAAAAAAKJriKabiKaLiOaIjSqJlWqKmaq4om7Lruq7ruq7ruq7ruq7ruq7ruq7ruq7ruq7ruq7ruq7ruq7rui4QGrIKAJAAANCRHMmRHEmRFEmRHMkBQkNWAQAyAAACAHAMx5AUybEsS9M8zdM8TfRET/RMTxVd0QVCQ1YBAIAAAAIAAAAAADAkw1IsR3M0SZRUS7VUTbVUSxVVT1VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVTVN0zRNIDRkJQAABADAYo3B5SAhJSXl3hDCEJOeMSYhtV4hBJGS3jEGFYOeMqIMct5C4xCDHggNWREARAEAAMYgxxBzyDlHqZMSOeeodJQa5xyljlJnKcWYYs0oldhSrI1zjlJHraOUYiwtdpRSjanGAgAAAhwAAAIshEJDVgQAUQAAhDFIKaQUYow5p5xDjCnnmHOGMeYcc44556B0UirnnHROSsQYc445p5xzUjonlXNOSiehAACAAAcAgAALodCQFQFAnACAQZI8T/I0UZQ0TxRFU3RdUTRd1/I81fRMU1U90VRVU1Vt2VRVWZY8zzQ901RVzzRV1VRVWTZVVZZFVdVt03V123RV3ZZt2/ddWxZ2UVVt3VRd2zdV1/Zd2fZ9WdZ1Y/I8VfVM03U903Rl1XVtW3VdXfdMU5ZN15Vl03Vt25VlXXdl2fc103Rd01Vl2XRd2XZlV7ddWfZ903WF35VlX1dlWRh2XfeFW9eV5XRd3VdlVzdWWfZ9W9eF4dZ1YZk8T1U903RdzzRdV3VdX1dd19Y105Rl03Vt2VRdWXZl2fddV9Z1zzRl2XRd2zZdV5ZdWfZ9V5Z13XRdX1dlWfhVV/Z1WdeV4dZt4Tdd1/dVWfaFV5Z14dZ1Ybl1XRg+VfV9U3aF4XRl39eF31luXTiW0XV9YZVt4VhlWTl+4ViW3feVZXRdX1ht2RhWWRaGX/id5fZ943h1XRlu3efMuu8Mx++k+8rT1W1jmX3dWWZfd47hGDq/8OOpqq+brisMpywLv+3rxrP7vrKMruv7qiwLvyrbwrHrvvP8vrAso+z6wmrLwrDatjHcvm4sv3Acy2vryjHrvlG2dXxfeArD83R1XXlmXcf2dXTjRzh+ygAAgAEHAIAAE8pAoSErAoA4AQCPJImiZFmiKFmWKIqm6LqiaLqupGmmqWmeaVqaZ5qmaaqyKZquLGmaaVqeZpqap5mmaJqua5qmrIqmKcumasqyaZqy7LqybbuubNuiacqyaZqybJqmLLuyq9uu7Oq6pFmmqXmeaWqeZ5qmasqyaZquq3meanqeaKqeKKqqaqqqraqqLFueZ5qa6KmmJ4qqaqqmrZqqKsumqtqyaaq2bKqqbbuq7Pqybeu6aaqybaqmLZuqatuu7OqyLNu6L2maaWqeZ5qa55mmaZqybJqqK1uep5qeKKqq5ommaqqqLJumqsqW55mqJ4qq6omea5qqKsumatqqaZq2bKqqLZumKsuubfu+68qybqqqbJuqauumasqybMu+78qq7oqmKcumqtqyaaqyLduy78uyrPuiacqyaaqybaqqLsuybRuzbPu6aJqybaqmLZuqKtuyLfu6LNu678qub6uqrOuyLfu67vqucOu6MLyybPuqrPq6K9u6b+sy2/Z9RNOUZVM1bdtUVVl2Zdn2Zdv2fdE0bVtVVVs2TdW2ZVn2fVm2bWE0Tdk2VVXWTdW0bVmWbWG2ZeF2Zdm3ZVv2ddeVdV/XfePXZd3murLty7Kt+6qr+rbu+8Jw667wCgAAGHAAAAgwoQwUGrISAIgCAACMYYwxCI1SzjkHoVHKOecgZM5BCCGVzDkIIZSSOQehlJQy5yCUklIIoZSUWgshlJRSawUAABQ4AAAE2KApsThAoSErAYBUAACD41iW55miatqyY0meJ4qqqaq27UiW54miaaqqbVueJ4qmqaqu6+ua54miaaqq6+q6aJqmqaqu67q6Lpqiqaqq67qyrpumqqquK7uy7Oumqqqq68quLPvCqrquK8uybevCsKqu68qybNu2b9y6ruu+7/vCka3rui78wjEMRwEA4AkOAEAFNqyOcFI0FlhoyEoAIAMAgDAGIYMQQgYhhJBSSiGllBIAADDgAAAQYEIZKDRkRQAQJwAAGEMppJRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkgppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkqppJRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoplVJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSCgCQinAAkHowoQwUGrISAEgFAACMUUopxpyDEDHmGGPQSSgpYsw5xhyUklLlHIQQUmktt8o5CCGk1FJtmXNSWosx5hgz56SkFFvNOYdSUoux5ppr7qS0VmuuNedaWqs115xzzbm0FmuuOdecc8sx15xzzjnnGHPOOeecc84FAOA0OACAHtiwOsJJ0VhgoSErAYBUAAACGaUYc8456BBSjDnnHIQQIoUYc845CCFUjDnnHHQQQqgYc8w5CCGEkDnnHIQQQgghcw466CCEEEIHHYQQQgihlM5BCCGEEEooIYQQQgghhBA6CCGEEEIIIYQQQgghhFJKCCGEEEIJoZRQAABggQMAQIANqyOcFI0FFhqyEgAAAgCAHJagUs6EQY5Bjw1BylEzDUJMOdGZYk5qMxVTkDkQnXQSGWpB2V4yCwAAgCAAIMAEEBggKPhCCIgxAABBiMwQCYVVsMCgDBoc5gHAA0SERACQmKBIu7iALgNc0MVdB0IIQhCCWBxAAQk4OOGGJ97whBucoFNU6iAAAAAAAAwA4AEA4KAAIiKaq7C4wMjQ2ODo8AgAAAAAABYA+AAAOD6AiIjmKiwuMDI0Njg6PAIAAAAAAAAAAICAgAAAAAAAQAAAAICAT2dnUwAEAAAAAAAAAADNFExGAgAAAPrUdacBAQA=" : a.src = "data:audio/mpeg;base64,/+MYxAAAAANIAUAAAASEEB/jwOFM/0MM/90b/+RhST//w4NFwOjf///PZu////9lns5GFDv//l9GlUIEEIAAAgIg8Ir/JGq3/+MYxDsLIj5QMYcoAP0dv9HIjUcH//yYSg+CIbkGP//8w0bLVjUP///3Z0x5QCAv/yLjwtGKTEFNRTMuOTeqqqqqqqqqqqqq/+MYxEkNmdJkUYc4AKqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq";
            a.classList.add("autoplay-test");
            t.caffeineNode.appendChild(a);
            var e = a.play();
            void 0 !== e ? e.then(function() {
                A()
            }).catch(function(e) {}) : a.addEventListener("playing", A)
        }();
        f(!0)
    }();

    function d(e) {
        if (!o) {
            document.removeEventListener("click", d, !0);
            n.audio.load();
            n.video.load();
            n.ads.load();
            o = !0
        }
    }

    function f(e) {
        var t = e ? "on" : "off";
        i[t](c.PLAYER_READY, p);
        t = e ? "addEventListener" : "removeEventListener";
        document[t]("click", d, !0)
    }

    function A() {
        o = r = e = !0;
        ! function() {
            a.removeEventListener("playing", A);
            t.caffeineNode.removeChild(a)
        }();
        g()
    }

    function p() {
        g()
    }

    function g() {
        i.player && i.player.status === u.READY && s && (e || o ? i.player.play() : r || a.addEventListener("playing", m))
    }

    function m() {
        i.player.play();
        a.removeEventListener("playing", m)
    }
    i.autoPlayManager = {get autoPlayNextMedia() {
            return s
        },
        set autoPlayNextMedia(e) {
            s = !0 === e
        },
        get canAutoPlay() {
            return e || o
        },
        destroy: function() {
            a.removeEventListener("playing", m);
            f(!1);
            delete i.autoPlayManager
        }
    }
});
CBC.APP.Caffeine.addModule("Core.Components.ClipMetadata", function(e) {
    "use strict";
    var t = this,
        n = t.queue[e],
        a = t.session.modelsManager.getClip(n);
    return {get id() {
            return a.getId()
        },
        get idType() {
            return a.getIdType()
        },
        get queuePosition() {
            return e
        },
        get title() {
            return a.getTitle()
        },
        get description() {
            return a.getDescription()
        },
        get artist() {
            if ("music" === a.getContentArea().toLowerCase()) return a.getShowName()
        },
        get album() {
            if ("music" === a.getContentArea().toLowerCase()) return a.getDescription()
        },
        get showName() {
            return a.getShowName()
        },
        get thumbnail() {
            return a.getThumbnail()
        },
        get hostImage() {
            return a.getHostImage()
        },
        get duration() {
            return t.player.duration || a.getDuration()
        },
        get chapters() {
            return a.getChapters()
        },
        get currentChapterIndex() {
            return t.player.currentChapterIndex
        },
        get airDate() {
            return a.getAirDate()
        },
        get addedDate() {
            return a.getAddedDate()
        },
        get contentArea() {
            return a.getContentArea()
        },
        get season() {
            return a.getSeason()
        },
        get episode() {
            return a.getEpisode()
        },
        get type() {
            return a.getType()
        },
        get region() {
            return a.getRegion()
        },
        get sport() {
            return a.getSport()
        },
        get genre() {
            return a.getGenre()
        },
        get categories() {
            return a.getCategories()
        },
        get pageUrl() {
            return a.getPageUrl()
        },
        get isAudio() {
            return a.isAudio()
        },
        get isVideo() {
            return a.isVideo()
        },
        get isLive() {
            return a.isLive()
        },
        get isOnDemand() {
            return a.isOnDemand()
        },
        get isBlocked() {
            return a.isBlocked()
        },
        get embeddedVia() {
            return a.getEmbeddedVia()
        },
        toString: function() {
            return n
        }
    }
});
CBC.APP.Caffeine.addModule("Core.Components.Conductrics", function() {
    "use strict";
    var t, e = this,
        n = CBC.APP.Caffeine.Helpers.Event,
        a = !1;
    e.on(n.PLAYER_PLAYINTENT, i);

    function i() {
        e.off(n.PLAYER_PLAYINTENT, i);
        if (t = CBC.APP.Caffeine.getSettings().CONDUCTRICS_AGENT_ID) try {
            CBC.APP.AB.Testing.getDecision(t, !1, !1, r)
        } catch (e) {
            window.console.error("Error getting selection from Conductrics")
        }
    }

    function r(e) {
        try {
            a = e.data.sels[t] || !1
        } catch (e) {
            window.console.error("Error parsing selection from Conductrics")
        }
    }
    e.session.conductrics = {
        destroy: function() {
            e.off(n.PLAYER_PLAYINTENT, i);
            delete e.session.conductrics
        },
        get selection() {
            return a
        }
    };
    return e.session.conductrics
});
CBC.APP.Caffeine.addModule("Core.Components.Config", function(r) {
    "use strict";
    var n = CBC.APP.Caffeine.Helpers.ErrorList,
        a = CBC.APP.Caffeine.getSettings(),
        i = Math.round(100 * Math.random());

    function o() {
        var e = document.querySelector(r.containerSelector);
        if (null === e) throw new Error(n.CONTAINER_SELECTOR_NULL.debug);
        if (!e.insertAdjacentHTML || "undefined" == typeof JSON) {
            var t = n.DOM_FEATURE_UNAVAILABLE;
            try {
                e.innerHTML = t.message
            } catch (e) {} finally {
                throw new Error(n.DOM_FEATURE_UNAVAILABLE.debug)
            }
        }
        return e
    }

    function t() {
        return e
    }

    function s() {
        return "cp" + Math.floor(99999 * Math.random())
    }
    var e = function() {
        var e, t;
        if (r.ciid && CBC.APP.Caffeine.getInstance(r.ciid)) {
            t = n.DUPLICATE_CIID;
            o().insertAdjacentHTML("beforebegin", t.message);
            throw new Error(t.debug)
        }
        if (!/^([a-z_]|-[a-z_-])[a-z\d_-]*$/i.test(r.ciid)) {
            t = n.INVALID_CIID;
            o().insertAdjacentHTML("beforebegin", t.message);
            throw new Error(t.debug)
        }
        e = r.ciid || s();
        for (; CBC.APP.Caffeine.getInstance(e);) e = s();
        return e
    }();
    return {get container() {
            return o()
        },
        get caffeineNode() {
            return function() {
                var e = document.getElementById("Caffeine" + t());
                if (!e) throw new Error(n.INJECT_PLAYER_FAILED.debug);
                return e
            }()
        },
        get ciid() {
            return t()
        },
        get embeddedClips() {
            return function() {
                for (var e = ["audioId", "audioCategoryId", "mediaId", "inline", "clipId", "categoryId", "categoryName", "platformQuery", "liveRadioId", "webRadioId", "firstPlayId", "neuroMediaId"], t = {}, n = 0; n < e.length; n++) {
                    var a = e[n];
                    if (r[a])
                        if ("platformQuery" === a) {
                            var i = JSON.stringify(r[a]);
                            t[a] = i
                        } else t[a] = r[a]
                }
                return t
            }()
        },
        get autoPlay() {
            return "true" == r.autoPlay || !0 === r.autoPlay
        },
        get autoDock() {
            return "true" == r.autoDock || !0 === r.autoDock
        },
        get playerWidth() {
            return function() {
                if (void 0 !== r.playerWidth) return 0 < parseInt(r.playerWidth) ? parseInt(r.playerWidth) : void 0
            }()
        },
        get forceVideo() {
            return "true" == r.forceVideo || !0 === r.forceVideo
        },
        get adPolicy() {
            return !!r.adPolicy && r.adPolicy
        },
        get thumbnail() {
            return function() {
                if (r.thumbnail) {
                    r.thumbnail = decodeURIComponent(r.thumbnail);
                    return !!/[-a-zA-Z0-9@:%_\+.~#?&\/=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&\/=]*)?/gi.test(r.thumbnail) && r.thumbnail
                }
                return !1
            }()
        },
        get shareUrl() {
            return function() {
                if (r.shareUrl) {
                    r.shareUrl = decodeURIComponent(r.shareUrl);
                    return !!/(http|https):\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/=]*)/g.test(r.shareUrl) && r.shareUrl
                }
                return !1
            }()
        },
        get noCompanion() {
            return "true" == r.noCompanion || !0 === r.noCompanion
        },
        get adUrl() {
            return !!r.adUrl && r.adUrl
        },
        get playlistLimit() {
            return function() {
                var e = void 0 !== r.playlistLimit && /^\d+$/.test(r.playlistLimit) ? Number(r.playlistLimit) : 1 / 0;
                0 === e && (e = 1);
                return e
            }()
        },
        get sort() {
            return void 0 !== r.sort && "dateadded" === r.sort.toLowerCase() ? "dateAdded" : "dateAired"
        },
        get playlistThumbnails() {
            return function() {
                switch (r.playlistThumbnails) {
                    case "host":
                        return "host";
                    case "false":
                        return !1;
                    default:
                        return "default"
                }
            }()
        },
        get playlistContainer() {
            return document.querySelector(r.playlistSelector)
        },
        get continuousPlay() {
            return "true" == r.continuousPlay || !0 === r.continuousPlay
        },
        get editablePlaylist() {
            return "true" == r.editablePlaylist || !0 === r.editablePlaylist
        },
        get musicDJUid() {
            return !!r.musicDJUid && r.musicDJUid
        },
        get beta() {
            return function() {
                var e = !1;
                e = !0 === r.beta || "true" === r.beta;
                if (a.FORCE_INTERNAL_BETA) {
                    var t = CBC.APP.Caffeine.Helpers.Utils.getIsInternal();
                    a.FORCE_INTERNAL_BETA && t && (e = !0)
                }
                i <= a.FORCE_BETA_THRESHOLD && (e = !0);
                var n = decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent("avcaff_beta").replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1"));
                "false" === n ? e = !1 : "true" === n && (e = !0);
                return e
            }()
        },
        get maxHeight() {
            return function() {
                if (void 0 !== r.maxHeight) return 0 < parseInt(r.maxHeight) ? parseInt(r.maxHeight) : void 0
            }()
        },
        get env() {
            return /^[a-zA-Z0-9]{2,25}$/.test(r.env) ? r.env : void 0
        }
    }
});
CBC.APP.Caffeine.addModule("Core.Components.ErrorManager", function(r) {
    "use strict";
    var o, s = this,
        e = CBC.APP.Caffeine,
        t = e.Constants.Event,
        n = e.Constants.PlayerStatus,
        a = e.getSettings(),
        l = "//ec2-18-191-218-191.us-east-2.compute.amazonaws.com/assets/js/raygun.vanilla.min.js",
        c = !1,
        i = !1,
        u = [],
        d = r.caffeineNode.querySelector(".cbc-caffeine-error-messages"),
        f = !1,
        A = null,
        p = null;

    function g() {
        (o = Raygun.noConflict()).init(a.RAYGUN_API_KEY);
        o.setVersion("14.6.2");
        o.onBeforeSend(function(e) {
            try {
                i = e.Details.User.UUID
            } catch (e) {}
            return e
        });
        c = !0;
        for (var e = 0; e < u.length; e++) {
            C(u[e])
        }
        u = []
    }
    s.on(t.CAFFEINE_ERROR, m);

    function m(e) {
        e.track && C(e);
        e.critical && function(e) {
            if (s.session.config.continuousPlay && s.hasNextTrack()) s.playNextTrack();
            else if (s.player) {
                f = !0;
                A = s.session.queueactiveposition;
                var t = [n.READY, n.LOADING];
                if (-1 < t.indexOf(s.player.status)) {
                    s.player.destroy();
                    E(e)
                }
            } else E(e)
        }(e);
        window.console && window.console.error && window.console.error(e.debug)
    }

    function E(e) {
        i = !!e.track && i;
        var t = CBC.APP.Caffeine.Templates["ErrorManager.html"]({
            error: e,
            retry: f,
            raygunUUID: i
        });
        d.innerHTML = t;
        r.caffeineNode.setAttribute("data-status", "fatalerror");
        f && (p = d.querySelector(".error-retry")).addEventListener("click", v)
    }

    function C(e) {
        if (c) try {
            var t, n, a = "Error " + e.code + ": " + e.debug;
            try {
                throw new Error(a)
            } catch (e) {
                t = e
            }
            var i = CBC.APP.Caffeine.Helpers.Utils.getIsInternal();
            n = s.player ? {
                config: r,
                playheadTime: Math.floor(1e3 * s.player.currentTime),
                playerStatus: s.player.status,
                currentChapterIndex: s.player.currentChapterIndex,
                mediaId: s.player.currentClip,
                isInternal: i
            } : {
                config: r,
                feedResponse: e.feedResponse,
                feedStatus: e.feedStatus,
                isInternal: i
            };
            o.send(t, n)
        } catch (e) {
            if (window.console) {
                window.console.log("Raygun error:");
                window.console.log(e)
            }
        } else {
            CBC.APP.Caffeine.addDependency(l, g);
            u.push(e)
        }
    }

    function v() {
        p.removeEventListener("click", v);
        s.session.createPlayer(A, !0)
    }
    return {
        destroy: function() {
            p && p.removeEventListener("click", v);
            s.off(t.CAFFEINE_ERROR, m)
        }
    }
});
CBC.APP.Caffeine.addModule("Core.Components.EventLogger", function() {
    "use strict";
    var n = CBC.APP.Caffeine._,
        a = this,
        i = CBC.APP.Caffeine.Helpers.Event,
        r = [],
        o = {},
        t = [i.PLAYER_TIMEUPDATED, i.PLAYER_BUFFERUPDATED];
    ! function() {
        for (var e in i) i.hasOwnProperty(e) && -1 === n.indexOf(t, i[e]) && s(e)
    }();

    function s(t) {
        var e = function() {
            var e = {
                name: t,
                args: n.clone(CBC.APP.Caffeine._.toArray(arguments)),
                timestamp: Date.now()
            };
            r.push(e)
        };
        a.on(i[t], e);
        o[t] = e
    }
    a.eventLogger = {
        getEventsFromLast: function(e) {
            if (!e) return r;
            for (var t = r.length - 1; - 1 < t; t--)
                if (r[t].name === e) return r.slice(t)
        },
        destroy: function() {
            r = [];
            for (var e in o) {
                a.off(i[e], o[e]);
                delete o[e]
            }
            delete a.eventLogger
        }
    }
});
CBC.APP.Caffeine.addModule("Core.Components.FullscreenManager", function(a, i) {
    var n = this,
        r = !1,
        o = !1,
        s = CBC.APP.Caffeine.Helpers.Event;
    ! function() {
        a.caffeineNode.requestFullscreen && window.addEventListener("resize", c);
        a.caffeineNode.webkitRequestFullscreen && window.addEventListener("resize", u);
        a.caffeineNode.mozRequestFullScreen && window.addEventListener("resize", d);
        a.caffeineNode.msRequestFullscreen && window.addEventListener("resize", f);
        n.on(s.PLAYER_METADATALOADED, A);
        n.on(s.PLAYER_ENDED, p)
    }();

    function e() {
        return document.webkitFullscreenEnabled || document.mozFullScreenEnabled || document.msFullscreenEnabled
    }

    function t(e) {
        e && !r ? (n = a.caffeineNode.requestFullscreen || a.caffeineNode.webkitRequestFullscreen || a.caffeineNode.mozRequestFullScreen || a.caffeineNode.msRequestFullscreen) ? n.call(a.caffeineNode) : o && i.video.webkitEnterFullscreen() : !e && r && (t = document.exitFullscreen || document.webkitExitFullscreen || document.mozCancelFullScreen || document.msExitFullscreen) && t.call(document);
        var t, n;
        a.caffeineNode.focus()
    }

    function l(e) {
        var t = r;
        r = !!e;
        t && !e ? n.trigger(s.SESSION_FULLSCREEN_OFF) : !t && e && n.trigger(s.SESSION_FULLSCREEN_ON)
    }

    function c() {
        l(document.fullscreenElement)
    }

    function u() {
        l(document.webkitFullscreenElement)
    }

    function d() {
        l(document.mozFullScreenElement)
    }

    function f() {
        l(document.msFullscreenElement)
    }

    function A() {
        if (!e() && i.video.webkitSupportsFullscreen) {
            o = !0;
            a.caffeineNode.querySelector(".fullscreen").classList.remove("unavailable")
        }
    }

    function p() {
        if (o) {
            o = !1;
            var e = a.caffeineNode.querySelector(".fullscreen");
            e && e.classList.add("unavailable")
        }
    }
    n.fullscreenManager = {get fullScreen() {
            return r
        },
        set fullScreen(e) {
            t(e)
        },
        get canFullscreen() {
            return e()
        },
        destroy: function() {
            window.removeEventListener("resize", c);
            window.removeEventListener("resize", u);
            window.removeEventListener("resize", d);
            window.removeEventListener("resize", f);
            n.off(s.PLAYER_METADATALOADED, A);
            n.off(s.PLAYER_ENDED, p);
            delete n.fullscreenManager
        }
    }
});
CBC.APP.Caffeine.addModule("Core.Components.Heartbeat", function() {
    "use strict";
    var i, r, o, n, s = this,
        e = CBC.APP.Caffeine,
        l = e.Constants.Event,
        c = e.Constants.ErrorList,
        u = e.Helpers.DateTimeFormat,
        d = e._,
        t = !1,
        a = !1,
        f = 0,
        A = !0;
    ! function() {
        try {
            i = CBC.APP.SC.VideoHeartbeat;
            r = i.init({
                playerName: "cbc-caffeine-heartbeat",
                comScore: !0
            })
        } catch (e) {
            s.trigger(l.CAFFEINE_ERROR, c.NO_HEARTBEAT)
        }
        B = d.throttle(B, 1e3, {
            trailing: !1
        });
        p(!0)
    }();

    function p(e) {
        var t = e ? "on" : "off";
        s[t](l.PLAYER_INITIALPLAY, T);
        s[t](l.PLAYER_ENDED, Y);
        s[t](l.PLAYER_PLAYING, L);
        s[t](l.PLAYER_PAUSED, b);
        s[t](l.CHAPTER_START, I);
        s[t](l.CHAPTER_END, M);
        s[t](l.PLAYER_SEEKSTART, w);
        s[t](l.PLAYER_SEEKEND, N);
        s[t](l.ADS_BREAK_START, k);
        s[t](l.ADS_BREAK_COMPLETE, U);
        s[t](l.ADS_STARTED, q);
        s[t](l.ADS_COMPLETE, O);
        s[t](l.PLAYER_TIMEUPDATED, B)
    }

    function g() {
        var e = o,
            t = {
                streamtitle: e.getTitle(),
                contentarea: e.getContentArea(),
                source: e.getIdType(),
                type: e.getType(),
                show: e.isLive() ? "" : e.getShowName(),
                genre: e.getGenre(),
                audiovideo: e.isAudio() ? "audio" : "video",
                totalchapters: e.isLive() ? "1" : e.getChapters().length + "",
                uid: e.getId() + ""
            },
            n = {
                liveondemand: e.isLive() ? "Live" : "On-Demand",
                pdkplayername: "cbc-caffeine-heartbeat"
            },
            a = {
                streamType: e.isLive() ? "live" : "vod",
                playhead: s.player.currentTime,
                length: y(s.currentClip.id) ? C() : o.isLive() ? -1 : o.getDuration(),
                cbcContextVideo: t
            };
        switch (e.getIdType()) {
            case "webRadioId":
                a.streamType = "live";
                a.length = -1;
                t.streamtitle = e.getDescription();
                t.totalchapters = "-1";
                delete t.show;
                break;
            case "firstPlayId":
                a.length = C();
                t.streamtitle = e.getDescription();
                t.totalchapters = v().length + "";
                break;
            case "mediaId":
            case "liveRadioId":
                t.seasonnumber = e.getSeason() + "";
                t.episodenumber = e.getEpisode() + "";
                t.region = e.getRegion();
                t.airDate = u.getIso8601Date(e.getAirDate());
                t.sport = e.getSport();
                a.playerContextVideo = n;
                break;
            case "audioId":
                t.airDate = u.getIso8601Date(e.getAirDate());
                a.playerContextVideo = n;
                delete t.genre
        }
        return a
    }

    function m() {
        var e = s.session.modelsManager.getClip(s.currentClip.id),
            t = !!o;
        t && (t = o.getId() === e.getId());
        t && (t = n === s.currentClip.queuePosition);
        return !t
    }

    function E() {
        if (a || m()) {
            n = s.currentClip.queuePosition;
            o = s.session.modelsManager.getClip(s.currentClip.id);
            a = !1;
            if (!t || !y(s.currentClip.id) || !P(e = s.session.queueactiveposition, e - 1)) {
                t && D();
                H("videoLoad", A, g());
                t = o;
                var e
            }
        }
    }

    function C() {
        for (var e = 0, t = v(), n = 0; n < t.length; n++) e += t[n].getDuration();
        return 0 < e && e < 1 / 0 ? e : -1
    }

    function v() {
        var e, t, n = [o],
            a = s.queue,
            i = s.session.queueactiveposition;
        for (e = i - 1; 0 <= e; e--) {
            t = s.session.modelsManager.getClip(a[e]);
            if (!P(e, i)) break;
            n.push(t)
        }
        for (e = i + 1; e < a.length; e++) {
            t = s.session.modelsManager.getClip(a[e]);
            if (!P(e, i)) break;
            n.push(t)
        }
        return n
    }

    function h(e) {
        for (var t = 0, n = e - 1; 0 <= n && P(e, n);) {
            var a = s.queue[n];
            t += s.session.modelsManager.getClip(a).getDuration();
            n--
        }
        return t
    }

    function y() {
        for (var e = ["webRadioId", "firstPlayId"], t = 0; t < arguments.length; t++) {
            var n = arguments[t];
            if (!n) return !1;
            var a = s.session.modelsManager.getClip(n);
            if (-1 === e.indexOf(a.getIdType())) return !1
        }
        return !0
    }

    function P(e, t) {
        var n = s.queue[e],
            a = s.queue[t];
        if (!n || !a) return !1;
        if (!y(n, a)) return !1;
        var i = s.session.modelsManager.getClip(n),
            r = s.session.modelsManager.getClip(a);
        return i.getIdType() === r.getIdType() && d.isEqual(i.getEmbeddedVia(), r.getEmbeddedVia())
    }

    function _(e) {
        return y(s.currentClip.id) ? function() {
            for (var e = 1, t = s.session.queueactiveposition - 1; 0 <= t && P(t, s.session.queueactiveposition);) {
                e++;
                t--
            }
            return e
        }() : e + 1
    }

    function S() {
        return !o.isLive() && (!y(s.currentClip.id) || o.isOnDemand() && !P(e = s.session.queueactiveposition, e + 1));
        var e
    }

    function R() {
        return Math.round(s.player.currentTime)
    }

    function D() {
        H("videoUnload", !A);
        t = !1
    }

    function T() {
        E()
    }

    function L() {
        H("videoPlay", A)
    }

    function b() {
        H("videoPause", A)
    }

    function I(e) {
        var t, n = o.getChapters()[e],
            a = {
                length: function(e) {
                    var t = o.getChapters();
                    if (e < 0 || e >= t.length) return -1;
                    var n, a = t[e];
                    n = e === t.length - 1 ? o.getDuration() - a.startTime : t[e + 1].startTime - a.startTime;
                    n <= 0 && (n = -1);
                    return n
                }(e),
                position: _(e),
                startTime: (t = e, y(s.currentClip.id) ? h(s.session.queueactiveposition) : o.getChapters()[t].startTime),
                playhead: y(s.currentClip.id) ? h(s.session.queueactiveposition) : R()
            };
        switch (o.getIdType()) {
            case "liveRadioId":
                a.cbcContextChapter = {
                    show: n.name
                };
                break;
            case "firstPlayId":
            case "webRadioId":
                a.cbcContextChapter = {
                    author: o.getShowName(),
                    chaptertitle: o.getTitle()
                }
        }
        H("chapterStart", !A, a)
    }

    function M(e) {
        H("chapterComplete", A)
    }

    function w() {
        H("seekStart", A)
    }

    function N() {
        H("seekComplete", A)
    }

    function k(e) {
        m() && E();
        f++;
        var t;
        switch (e) {
            case -1:
                t = "post-roll";
                break;
            case 0:
                t = "pre-roll";
                break;
            default:
                t = "mid-roll-" + e
        }
        H("adBreakStart", A, {
            name: t,
            position: f
        })
    }

    function U() {
        H("adBreakComplete", A)
    }

    function q(e) {
        H("adStart", !A, {
            id: e.id,
            length: e.length,
            position: e.position,
            name: e.name
        })
    }

    function O(e) {
        H("adComplete", !A, {
            id: e
        })
    }

    function B() {
        H("updatePlayHead", A)
    }

    function Y() {
        if (o) {
            if (S()) {
                a = !0;
                H("videoComplete", A)
            } else if (o.isLive()) {
                a = !0;
                D()
            }
            f = 0
        }
    }

    function H(e, t, n) {
        if (!0 === t) {
            n || (n = {});
            n.playhead = R()
        }
        if ("updatePlayHead" !== e && window.debugCaffeineHeartbeat) {
            window.console.log("%c Heartbeat " + e, "color: blue");
            window.console.log(n)
        }
        if (i) try {
            var a = [r];
            n && a.push(n);
            i[e].apply(window, a)
        } catch (e) {
            s.trigger(l.CAFFEINE_ERROR, c.TRACKING_ERROR)
        }
    }
    return {
        destroy: function() {
            i && i.destroy();
            p(!1)
        }
    }
});
CBC.APP.Caffeine.addModule("Core.Components.Keyboard", function(t) {
    "use strict";
    var n = this,
        a = n.session.config,
        i = {
            SPACE_BAR: 32,
            UP_ARROW: 38,
            DOWN_ARROW: 40,
            RIGHT_ARROW: 39,
            LEFT_ARROW: 37
        },
        r = CBC.APP.Caffeine.Helpers.Event,
        o = CBC.APP.Caffeine.Constants.PlayerStatus;
    a.caffeineNode.addEventListener("keydown", e);

    function e(e) {
        if (!(e.altKey || e.ctrlKey || e.metaKey || e.shiftKey)) switch (e.keyCode) {
            case i.SPACE_BAR:
                ! function(e) {
                    if (document.activeElement === a.caffeineNode || s(a.caffeineNode, document.activeElement) && "BUTTON" !== document.activeElement.tagName.toUpperCase()) {
                        e.preventDefault();
                        ! function() {
                            if (n.player.status === o.PLAYING && t.isLive()) {
                                n.player.stop();
                                n.trigger(r.UI_INDIRECTUSERACTION, "stop")
                            } else if (n.player.status === o.PLAYING) {
                                n.player.pause();
                                n.trigger(r.UI_INDIRECTUSERACTION, "pause")
                            } else {
                                n.player.play();
                                n.trigger(r.UI_INDIRECTUSERACTION, "play")
                            }
                        }()
                    }
                }(e);
                break;
            case i.UP_ARROW:
            case i.DOWN_ARROW:
                ! function(e) {
                    if (document.activeElement === a.caffeineNode || s(a.caffeineNode, document.activeElement) && "INPUT" !== document.activeElement.tagName.toUpperCase()) {
                        var t = e.keyCode == i.UP_ARROW ? .1 : -.1;
                        e.preventDefault();
                        ! function(e) {
                            var t = Math.round(10 * (n.player.volume + e)) / 10;
                            if (0 <= t && t <= 1) {
                                n.player.volume = t;
                                n.trigger(r.UI_INDIRECTUSERACTION, "volume")
                            }
                        }(t)
                    }
                }(e);
                break;
            case i.LEFT_ARROW:
            case i.RIGHT_ARROW:
                ! function(e) {
                    if (n.player.status === o.ADVERTISING) return;
                    if (n.player.status === o.ADVERTISING_PAUSED) return;
                    if (document.activeElement === a.caffeineNode || document.activeElement.classList.contains("seek") || s(a.caffeineNode, document.activeElement) && "INPUT" !== document.activeElement.tagName.toUpperCase()) {
                        var t = e.keyCode == i.LEFT_ARROW ? -5 : 5;
                        e.preventDefault();
                        n.player.currentTime = t + n.player.currentTime;
                        n.trigger(r.UI_INDIRECTUSERACTION, e.keyCode == i.LEFT_ARROW ? "skip-back" : "skip-forward")
                    }
                }(e);
                break;
            case 27:
                n.player.ui.closeOverlay();
                n.player.ui.closeContextMenu();
                e.preventDefault();
                break;
            case 192:
                n.player.playbackStats.show();
                e.preventDefault()
        }
    }

    function s(e, t) {
        for (var n = t.parentNode; null !== n;) {
            if (n == e) return !0;
            n = n.parentNode
        }
        return !1
    }
    return {
        destroy: function() {
            document.removeEventListener("keydown", e)
        }
    }
});
CBC.APP.Caffeine.addModule("Core.Components.LocalStorage", function() {
    "use strict";
    var n, a, i, r, o = this,
        s = CBC.APP.Caffeine.Helpers.Event;
    ! function() {
        try {
            a = window.localStorage;
            i = JSON.parse(a.getItem("caffeine")) || {}
        } catch (e) {
            return
        }
        e(!0)
    }();

    function l(e) {
        if (void 0 !== a) {
            var t = i[r];
            return void 0 === t ? null : void 0 === e ? t[r] : t ? t[e] : null
        }
    }

    function e(e) {
        var t = e ? "addEventListener" : "removeEventListener";
        window[t]("beforeunload", u);
        o[t = e ? "on" : "off"](s.PLAYER_READY, c)
    }

    function c() {
        o.off(s.PLAYER_READY, c);
        n = o.session.modelsManager.getClip(o.player.currentClip);
        r = n.getId();
        var e = l("lastModified");
        if (1728e5 < Date.now() - e) ! function() {
            delete i[r];
            try {
                a.setItem("caffeine", JSON.stringify(i))
            } catch (e) {}
        }();
        else {
            if(CF_START_TIME) {
                var t = CF_START_TIME;
            } else {
                var t = l("currentTime");  // TODO Is it here?
            }
            //console.log("TODO2: " + window.CF_START_TIME);
            //console.log("TODO: " + t);
            n.isOnDemand() && 0 < t && (o.player.currentTime = t)
        }
    }

    function u() {
        t()
    }

    function t() {
        if (o.player && void 0 !== a && function() {
                var e = !0; - 1 === ["audioId", "mediaId"].indexOf(n.getIdType()) && (e = !1);
                n.isLive() && (e = !1);
                return e
            }()) {
            var e = o.player.currentTime;.9 <= e / n.getDuration() && (e = 0);
            e !== (l("currentTime") || 0) && function(e, t) {
                if (void 0 !== a) {
                    var n = l() || {};
                    null === n && (n = {});
                    if (void 0 === t) delete n[e];
                    else {
                        n[e] = t;
                        n.lastModified = Date.now()
                    }
                    Object.keys(n).length < 2 ? delete i[r] : i[r] = n;
                    try {
                        a.setItem("caffeine", JSON.stringify(i))
                    } catch (e) {}
                }
            }("currentTime", e)
        }
    }
    return {
        destroy: function() {
            e(!1);
            n && t()
        }
    }
});
CBC.APP.Caffeine.Model = function(a, e) {
    "use strict";
    var i = CBC.APP.Caffeine._,
        t = CBC.APP.Caffeine,
        n = t.Helpers.Event,
        r = t.Helpers.DateTimeFormat,
        o = e,
        s = [],
        l = this;
    this.getId = function() {
        return o.id
    };
    ! function() {
        for (var e = 0; e < o.assetDescriptors.length; e++) {
            var t = a.initModule("Helpers.AssetDescriptor", l.getId(), o.assetDescriptors[e].loader, o.assetDescriptors[e].key, o.assetDescriptors[e].mimeType, o.assetDescriptors[e].codec);
            s.push(t)
        }
    }();

    function c(e, t) {
        return void 0 !== e && void 0 !== o[e] ? o[e] : void 0 !== t ? t : ""
    }
    this.getIdType = function() {
        return o.idType
    };
    this.getTitle = function() {
        return c("title")
    };
    this.getDescription = function() {
        return c("description")
    };
    this.getCategories = function() {
        return c("categories")
    };
    this.getThumbnail = function() {
        return c("thumbnail")
    };
    this.getHostImage = function() {
        return c("hostImage")
    };
    this.isLive = function() {
        return c("isLive")
    };
    this.getDuration = function() {
        return -1 === o.duration ? 1 / 0 : c("duration", 0)
    };
    this.getFormattedDuration = function() {
        return l.isLive() ? "Live" : r.getFormattedDuration(l.getDuration())
    };
    this.getChapters = function() {
        return c("chapters")
    };
    this.getShowName = function() {
        if ("liveRadioId" !== this.getIdType()) return c("showName");
        var e = "";
        if (a.player) {
            for (var t = this.getChapters(), n = t.length - 1; 0 <= n && !(Date.now() >= t[n].startTime); n--);
            0 <= n && (e = t[n].name)
        }
        return e
    };
    this.getAirDate = function() {
        return c("airDate")
    };
    this.getFormattedAirDate = function() {
        return r.getFormattedDate(l.getAirDate())
    };
    this.getAddedDate = function() {
        return c("addedDate")
    };
    this.getFormattedAddedDate = function() {
        return r.getFormattedDate(l.getAddedDate())
    };
    this.getContentArea = function() {
        return c("contentArea")
    };
    this.getType = function() {
        return c("type")
    };
    this.getRegion = function() {
        return c("region")
    };
    this.getSport = function() {
        return c("sport")
    };
    this.getGenre = function() {
        return c("genre")
    };
    this.getSeason = function() {
        return c("season")
    };
    this.getEpisode = function() {
        return c("episode")
    };
    this.getToken = function() {
        return c("token")
    };
    this.getPageUrl = function() {
        return c(encodeURI("pageUrl"))
    };
    this.isAudio = function() {
        return c("isAudio")
    };
    this.isVideo = function() {
        return void 0 === o.isVideo ? !l.isAudio() : o.isVideo
    };
    this.isOnDemand = function() {
        return c("isOnDemand")
    };
    this.isDRM = function() {
        return c("isDRM")
    };
    this.getCaptions = function() {
        return c("captions")
    };
    this.isBlocked = function() {
        return c("isBlocked")
    };
    this.isDAI = function() {
        return c("isDAI")
    };
    this.getEmbeddedVia = function() {
        return c("embeddedVia")
    };
    this.getAssetDescriptors = function() {
        return s
    };
    this.getAdUrl = function() {
        var e = c("adUrl");
        if (e) {
            if ("" !== (e = i.template(e)({
                    ix_u: document.location.href.replace(location.hash, ""),
                    url: document.location.href.replace(location.hash, "")
                }))) {
                var t, n = "&cust_params=plat%3Dcaffeine";
                (t = document.referrer.split("/"), -1 < i.indexOf(t, "amp")) && (n += "%26env%3Damp");
                window.CAFFEINE_AD_ENV && "fb" === window.CAFFEINE_AD_ENV && (n += "%26env%3Dfb");
                return e + n
            }
        }
    };
    CBC.APP.Caffeine._.defer(function() {
        a.trigger(n.MODEL_READY, l)
    })
};
CBC.APP.Caffeine.addModule("Core.Components.ModelsManager", function() {
    "use strict";
    var t = this,
        n = t.session.config,
        a = CBC.APP.Caffeine.Helpers.Event,
        i = CBC.APP.Caffeine._,
        r = {},
        o = 0,
        s = t.initModule("Handlers.Inline"),
        l = t.initModule("Handlers.Bistro");
    t.on(a.MODEL_READY, e);
    t.on(a.REQUESTED_MODELS_READY, c);

    function e(e) {
        r[e.getId()] || (r[e.getId()] = e);
        t.trigger(a.SESSION_CLIPREADY, e.getId())
    }

    function c(e) {
        0 === --o && i.defer(function() {
            t.trigger(a.MODELSMANAGER_COMPLETED)
        })
    }
    t.session.modelsManager = {
        destroy: function() {
            delete t.session.modelsManager;
            s.destroy();
            l.destroy();
            t.off(a.MODEL_READY, e);
            t.off(a.REQUESTED_MODELS_READY, c)
        },
        requestClips: function(e, t) {
            if (e.inline) {
                var n = i.pick(e, "inline");
                s.requestClips(n);
                delete e.inline;
                o++
            }
            if (!i.isEmpty(e)) {
                l.requestClips(e, t);
                o++
            }
        },
        getClip: function(e) {
            return r[e]
        },
        getAllClips: function(e) {
            var t = function(e) {
                var t = i.sortBy(e, function(e) {
                    return e["dateAdded" == n.sort ? "getAddedDate" : "getAirDate"]()
                });
                t.reverse();
                return t
            }(r);
            e && (t = i.first(t, e));
            return t
        },
        getNumClips: function() {
            return i.size(r)
        }
    }
});
CBC.APP.Caffeine.addModule("Core.Components.MusicDJPlaybackTracking", function() {
    "use strict";
    var i = this,
        e = CBC.APP.Caffeine.Helpers.Event,
        r = "https://services.radio-canada.ca/music/dj/v1/playbacklog",
        o = "2";
    i.on(e.PLAYER_INITIALPLAY, t);

    function t() {
        var e = i.currentClip.id,
            t = i.session.modelsManager.getClip(e);
        if ("webRadioId" === t.getIdType()) {
            var n = t.getEmbeddedVia(),
                a = {};
            a.userId = i.session.config.musicDJUid;
            a.trackId = e;
            a.sourceId = parseInt(o);
            a.webRadioId = n.value;
            (function(e) {
                for (var t in e)
                    if (null === e[t] || void 0 === e[t] || "" === e[t]) return !1;
                return !0
            })(a) && function(e) {
                var t = new XMLHttpRequest;
                t.open("post", r);
                t.setRequestHeader("Content-type", "application/json");
                t.send(JSON.stringify(e))
            }(a)
        }
    }
    i.musicDJPlaybackTracking = {
        destroy: function() {
            i.off(e.PLAYER_INITIALPLAY, t);
            delete i.musicDJPlaybackTracking
        }
    }
});
CBC.APP.Caffeine.addModule("Core.Components.Playlist", function() {
    "use strict";
    var l, n = this,
        t = CBC.APP.Caffeine.Helpers.Event,
        c = n.session.config;
    ! function() {
        var e = document.createElement("ul");
        c.playlistContainer.appendChild(e);
        l = e;
        n.on(t.SESSION_QUEUEUPDATED, i);
        n.on("metadataUpdated", i)
    }();

    function a(e, t) {
        var n = c.playlistThumbnails,
            a = !1;
        "host" == n ? a = e.getHostImage() : "default" == n && (a = e.getThumbnail());
        var i = CBC.APP.Caffeine.Templates["PlaylistItem.html"]({
            dataModel: e,
            thumbnail: a,
            index: t,
            editable: c.editablePlaylist
        });
        l.insertAdjacentHTML("beforeend", i);
        var r = l.lastElementChild,
            o = r.querySelector("button");
        o.onclick = CBC.APP.Caffeine._.bind(d, o);
        if (c.editablePlaylist) {
            var s = r.querySelector(".delete");
            s.onclick = CBC.APP.Caffeine._.bind(f, s)
        }
        CBC.APP.Caffeine._.delay(CBC.APP.Caffeine._.bind(u, o), 50)
    }

    function u() {
        this.classList.add("fadeInDown")
    }

    function d() {
        n.session.createPlayer(Number(this.getAttribute("data-index")), !0)
    }

    function f(e) {
        var t = Number(this.previousElementSibling.getAttribute("data-index"));
        n.session.removeItemFromQueue(t)
    }

    function i() {
        var e = n.session.getQueue();
        l.innerHTML = "";
        for (var t = 0; t < e.length; t++) {
            a(n.session.modelsManager.getClip(e[t]), t)
        }
    }
    n.playlist = {
        setActivePlaylistItem: function(e, t) {
            var n;
            if (0 !== (n = l.querySelectorAll("button.item")).length) {
                -1 < t && n[t].classList.remove("active");
                n[e] && n[e].classList.add("active")
            }
        },
        destroy: function() {
            c.playlistContainer.removeChild(l);
            n.off(t.SESSION_QUEUEUPDATED, i);
            n.off("metadataUpdated", i);
            delete n.playlist
        }
    }
});
CBC.APP.Caffeine.addModule("Core.Components.QueueAutoUpdater", function(n) {
    "use strict";
    var a = this,
        e = CBC.APP.Caffeine.Helpers.Event,
        t = CBC.APP.Caffeine.Constants.PlayerStatus,
        i = CBC.APP.Caffeine._;
    if (void 0 === n) throw new Error("Cannot run the queue auto-updater without supplying which parameters to auto-update.");
    if (void 0 !== n.webRadioId) {
        a.on(e.PLAYER_PLAYING, r);
        a.on(e.SESSION_QUEUEUPDATED, o);
        return {
            destroy: function() {
                a.off(e.PLAYER_PLAYING, r);
                a.off(e.SESSION_QUEUEUPDATED, o)
            }
        }
    }

    function r() {
        (function() {
            if (s()) return l(a.currentClip.id);
            return !1
        })() && (e = i.pick(n, "webRadioId"), a.queueNext(e));
        var e
    }

    function o() {
        a.off(e.SESSION_QUEUEUPDATED, o);
        a.session.config.continuousPlay && a.player && a.player.status === t.READY && (s() || a.playNextTrack())
    }

    function s() {
        var e = a.queue[a.session.queueactiveposition + 1];
        return void 0 === e || !l(e)
    }

    function l(e) {
        void 0 === e && window.console.trace();
        var t = a.session.modelsManager.getClip(e);
        return -1 !== Object.keys(n).indexOf(t.getIdType())
    }
});
CBC.APP.Caffeine.addModule("Core.Components.RecSys", function() {
    var n = this,
        a = CBC.APP.Caffeine.Helpers.Event,
        e = {
            All: "bf73e4",
            News: "a67db5"
        },
        i = {
            user: {},
            content: {
                id: ""
            },
            verb: "streamed"
        },
        r = 50,
        o = 0,
        t = "https://stream-recsys.data.cbc.ca/v1/";
    n.on(a.PLAYER_READY, s);

    function s() {
        if ("mediaId" === (e = n.currentClip).idType && e.isVideo && !e.isLive) {
            var e, t;
            ! function() {
                i.user.id = "";
                i.content.id = ""
            }();
            (t = n.currentClip).duration && (o = Math.ceil(t.duration * (r / 100)));
            n.on(a.PLAYER_TIMEUPDATED, u)
        }
    }

    function l() {
        ! function() {
            i.content.id = n.currentClip.id;
            i.user = n.session.userManager.getRecSysUserId()
        }();
        if (void 0 !== i.user && i.user !== {}) {
            "News" === n.currentClip.contentArea && c(t + e.News + "/publish", JSON.stringify(i));
            c(t + e.All + "/publish", JSON.stringify(i))
        }
    }

    function c(e, t) {
        var n = new XMLHttpRequest;
        n.open("post", e);
        n.setRequestHeader("Content-type", "application/json");
        n.send(t)
    }

    function u(e) {
        if (Math.floor(e) == o) {
            n.off(a.PLAYER_TIMEUPDATED, u);
            l()
        } else Math.floor(e) > o && n.off(a.PLAYER_TIMEUPDATED, u)
    }
    n.recSys = {
        destroy: function() {
            n.off(a.PLAYER_READY, s);
            n.off(a.PLAYER_TIMEUPDATED, u)
        }
    }
});
CBC.APP.Caffeine.addModule("Core.Components.Session", function(e) {
    "use strict";
    var t, a, i = this,
        r = CBC.APP.Caffeine._,
        o = CBC.APP.Caffeine.Helpers.ErrorList,
        s = CBC.APP.Caffeine.Helpers.Event,
        l = [],
        n = !1,
        c = 0,
        u = -1,
        d = {},
        f = i.initModule("Core.Components.Config", e);
    ! function() {
        d.audio = new Audio;
        d.video = document.createElement("video");
        d.video.setAttribute("playsinline", "");
        d.ads = document.createElement("video");
        d.ads.setAttribute("playsinline", "")
    }();
    ! function() {
        var e = CBC.APP.Caffeine.Templates["Caffeine.html"](f);
        try {
            f.container.insertAdjacentHTML("afterbegin", e)
        } catch (e) {
            throw new Error(o.INJECT_PLAYER_FAILED.debug)
        }
        f.playerWidth && (f.container.style.width = f.playerWidth + "px")
    }();
    var A = i.initModule("Core.Components.ErrorManager", f);
    i.initModule("Core.Components.AutoPlayManager", f, d);
    i.initModule("Core.Components.FullscreenManager", f, d);
    i.initModule("Players.Components.MediaSessionAPI");
    i.initModule("Core.Components.EventLogger");
    var p = i.initModule("Core.Components.Heartbeat");
    i.initModule("Core.Components.MusicDJPlaybackTracking");
    i.initModule("Core.Components.RecSys");
    r.defer(function() {
        i.initModule("Core.Components.Conductrics");
        i.initModule("Core.Components.UserManager");
        i.initModule("Core.Components.Ads", d.ads);
        i.initModule("Core.Components.ModelsManager");
        if (0 < Object.keys(f.embeddedClips).length) {
            i.on(s.MODELSMANAGER_COMPLETED, g);
            i.session.modelsManager.requestClips(f.embeddedClips, f.playlistLimit);
            S(f.embeddedClips)
        } else i.on(s.MODELSMANAGER_COMPLETED, _)
    });

    function g() {
        i.off(s.MODELSMANAGER_COMPLETED, g);
        i.on(s.MODELSMANAGER_COMPLETED, _);
        var e, t = i.session.modelsManager.getAllClips();
        e = f.playlistLimit <= t.length ? f.playlistLimit : t.length;
        for (var n = 0; n < e; n++) l.push(t[n].getId());
        1 < e && f.playlistContainer && i.initModule("Core.Components.Playlist");
        i.trigger(s.SESSION_QUEUEUPDATED, this);
        m(0, f.autoPlay)
    }

    function m(e, t) {
        if (!l[e]) throw new Error("Queue index is out of bounds");
        i.player && i.player.destroy();
        i.autoPlayManager.autoPlayNextMedia = t;
        var n = C(e);
        E(e);
        if (i.session.modelsManager.getClip(n).isBlocked()) i.trigger(s.CAFFEINE_ERROR, o.BLOCKED);
        else {
            i.trigger(s.SESSION_PLAYERCREATIONSTARTED);
            i.initModule("Players.Main", e, d)
        }
    }

    function E(e) {
        a = u;
        u = e;
        i.playlist && i.playlist.setActivePlaylistItem(u, a)
    }

    function C(e) {
        return l[e]
    }

    function v() {
        return u
    }

    function h(e) {
        P(c, e);
        c++
    }

    function y(e) {
        P(c = l.length, e)
    }

    function P(e, t) {
        l.splice(e, 0, t);
        n = !0;
        !i.playlist && f.playlistContainer && i.initModule("Core.Components.Playlist");
        i.playlist && i.playlist.setActivePlaylistItem(u, a)
    }

    function _() {
        i.off(s.SESSION_CLIPREADY, h);
        i.off(s.SESSION_CLIPREADY, y);
        if (n) {
            n = !1;
            i.trigger(s.SESSION_QUEUEUPDATED, this)
        }
    }

    function S(e) {
        if (void 0 !== e.webRadioId) {
            t && t.destroy();
            t = i.initModule("Core.Components.QueueAutoUpdater", e)
        }
    }
    i.session = {get config() {
            return f
        },
        get capabilities() {
            return {
                canChangeVolume: (e = new Audio, (e.volume = .9) == e.volume),
                get canAutoplay() {
                    return i.autoPlayManager.canAutoPlay
                },
                get canFullscreen() {
                    return i.fullscreenManager.canFullscreen
                }
            };
            var e
        },
        get queueactiveposition() {
            return v()
        },
        get currentClipId() {
            return C(v())
        },
        createPlayer: m,
        getIdAtIndex: C,
        getQueue: function() {
            return l
        },
        getTrackFromQueue: function(e) {
            e = Number(e);
            var t = u + e;
            if (l[t]) return t
        },
        queueNext: function(e) {
            i.off(s.MODELSMANAGER_COMPLETED, g);
            i.off(s.SESSION_CLIPREADY, y);
            i.off(s.SESSION_CLIPREADY, h);
            i.on(s.SESSION_CLIPREADY, h);
            c = u + 1;
            i.session.modelsManager.requestClips(e);
            S(e);
            return c
        },
        queueEnd: function(e) {
            i.off(s.MODELSMANAGER_COMPLETED, g);
            i.off(s.SESSION_CLIPREADY, h);
            i.off(s.SESSION_CLIPREADY, y);
            i.on(s.SESSION_CLIPREADY, y);
            c = l.length - 1;
            i.session.modelsManager.requestClips(e);
            S(e);
            return c
        },
        removeItemFromQueue: function(e) {
            var t, n = !1;
            l[e + 1] && (t = e + 1);
            l.splice(e, 1);
            if (e < u) E(u - 1);
            else if (e === u && t) m(e);
            else if (e === u & !t) {
                i.player.destroy();
                E(u - 1);
                n = !0
            }
            r.defer(function() {
                i.trigger(s.SESSION_QUEUEUPDATED, this);
                n || i.playlist.setActivePlaylistItem(u, a)
            })
        },
        clearQueue: function() {
            l = [];
            E(-1);
            i.trigger(s.SESSION_QUEUEUPDATED, this)
        },
        destroy: function() {
            i.ima && i.ima.destroy();
            f.container.innerHTML = "";
            i.autoPlayManager.destroy();
            i.mediaSession.destroy();
            i.fullscreenManager.destroy();
            i.eventLogger.destroy();
            i.musicDJPlaybackTracking.destroy();
            i.recSys.destroy();
            i.session.userManager.destroy();
            p.destroy();
            A.destroy();
            t && t.destroy();
            i.session.conductrics.destroy();
            i.off(s.MODELSMANAGER_COMPLETED, _);
            i.session.modelsManager.destroy();
            delete i.session
        }
    }
});
CBC.APP.Caffeine.addModule("Core.Components.UserManager", function() {
    "use strict";
    var e, t = this,
        a = CBC.APP.Caffeine._,
        n = CBC.APP.Caffeine.Helpers.Event,
        i = "https://uie.data.cbc.ca/v0/current_user";
    r();
    t.on(n.PLAYER_INITIALPLAY, r);

    function r() {
        CBC.APP.Caffeine.getXhr(i, o, s, !0)
    }

    function o() {
        e = JSON.parse(this.responseText)
    }

    function s() {
        window.console.error("Could not retrieve user id from UIE - status: " + this.status)
    }
    t.session.userManager = {
        destroy: function() {
            t.off(n.PLAYER_INITIALPLAY, r);
            delete t.session.userManager
        },
        getRecSysUserId: function() {
            var n;
            a.find(e, function(e, t) {
                if ("cbc:cbc_plus_id" === t) return n = {
                    cbc_plus: e[0]
                };
                "cbc:cbc_visitor_id" === t && (n = {
                    cbc_visitor: e[0]
                })
            });
            return n
        }
    }
});
CBC.APP.Caffeine.addModule("Core.Instance", function(e) {
    "use strict";
    var n = CBC.APP.Caffeine.Constants.Event;
    this.session = {};
    Object.defineProperty(this, "fullScreen", {
        configurable: !0,
        enumerable: !0,
        get: function() {
            return this.fullscreenManager.fullScreen
        },
        set: function(e) {
            this.fullscreenManager.fullScreen = e
        }
    });
    Object.defineProperty(this, "currentClip", {
        configurable: !0,
        enumerable: !0,
        get: function() {
            return this.player ? this.player.metadata : null
        }
    });
    Object.defineProperty(this, "queue", {
        configurable: !0,
        enumerable: !0,
        get: function() {
            return this.session.getQueue()
        }
    });
    this.initModule = function(t) {
        var e = CBC.APP.Caffeine.getModule(t),
            n = CBC.APP.Caffeine._.toArray(arguments).slice(1);
        try {
            return e.apply(this, n)
        } catch (e) {
            window.console.error("Error loading module " + t);
            window.console.error(e);
            throw e
        }
    };
    this.playById = function(e) {
        var t = CBC.APP.Caffeine.Helpers.Event,
            n = this.session.queueNext(e);
        this.on(t.SESSION_QUEUEUPDATED, a);
        this.on(t.MODELSMANAGER_COMPLETED, function e() {
            this.off(t.SESSION_QUEUEUPDATED, a);
            this.off(t.MODELSMANAGER_COMPLETED, e)
        });

        function a() {
            this.queue[n] && this.session.createPlayer(n, !0)
        }
    };
    this.playInline = function(e) {
        this.playById({
            inline: e
        })
    };
    this.play = function() {
        this.player.play()
    };
    this.pause = function() {
        this.player.pause()
    };
    this.unpause = function() {
        this.player.unpause()
    };
    this.queueNext = function(e) {
        this.session.queueNext(e)
    };
    this.queueEnd = function(e) {
        this.session.queueEnd(e)
    };
    this.playNextTrack = function() {
        this.playTrack(1)
    };
    this.playPrevTrack = function() {
        this.playTrack(-1)
    };
    this.playTrack = function(e, t) {
        var n = this.session.getTrackFromQueue(e);
        this.playTrackByIndex(n, t)
    };
    this.playTrackByIndex = function(e, t) {
        if (!this.queue[e]) throw new Error("Queue index is out of bounds");
        !1 !== t && (t = !0);
        this.trigger(n.INSTANCE_TRACKSKIPPED, e - this.currentClip.queuePosition);
        this.session.createPlayer(e, t)
    };
    this.hasNextTrack = function() {
        return Boolean(this.session.getTrackFromQueue(1))
    };
    this.clearQueue = function() {
        this.session.clearQueue()
    };
    this.destroy = function() {
        var e = CBC.APP.Caffeine.Helpers.Event,
            t = this.session.config.ciid;
        this.player && this.player.destroy();
        this.playlist && this.playlist.destroy();
        this.session.destroy();
        for (var n = Object.keys(this), a = 0; a < n.length; a++) {
            delete this[n[a]]
        }
        CBC.APP.Caffeine.trigger(e.CAFFEINE_INSTANCEDESTROYED, t)
    };
    this.initModule("Core.Components.Session", e);
    return this
});
CBC.APP.Caffeine.addModule("Handlers.Bistro", function() {
    "use strict";
    var o, r = this,
        s = r.session.config,
        a = CBC.APP.Caffeine.Helpers.Event,
        l = CBC.APP.Caffeine._,
        c = CBC.APP.Caffeine.Helpers.ErrorList,
        u = {},
        d = {},
        f = {},
        A = "https://www.cbc.ca/bistro/";
    r.on(a.MODEL_READY, e);

    function p(e) {
        this.errors = {};
        this.finished = !1;
        this.params = e
    }

    function g(t) {
        0;
        var e = JSON.parse(this.responseText),
            n = {
                feedResponse: this.responseText,
                feedStatus: this.status
            };
        if (l.isEmpty(d[t])) {
            for (var a = 0; a < e.errors.length; a++) window.console.error(e.errors[a]);
            try {
                ! function(e, t) {
                    if (0 === e.length) throw new Error(c.CLIP_UNAVAILABLE);
                    var n = {};
                    try {
                        for (var a = 0; a < e.length; a++) {
                            var i = new CBC.APP.Caffeine.Model(r, e[a]);
                            n[i.getId()] = i
                        }
                        d[t] = n
                    } catch (e) {
                        d[t] = c.CLIP_UNAVAILABLE;
                        throw e
                    }
                    f = l.extend(f, n)
                }(e.items, t)
            } catch (e) {
                l.isEmpty(d[t]) && (d[t] = c.FEED_PARSE);
                i(t, n);
                return
            } finally {
                delete u[t];
                E()
            }
        } else i(t, n)
    }

    function m(e) {
        0;
        d[e] = c.FEED_FETCH;
        delete u[e];
        i(e, {
            feedStatus: this.status
        });
        E()
    }

    function i(e, t) {
        var n = l.extend(d[e], t);
        l.defer(function() {
            r.trigger(a.CAFFEINE_ERROR, n)
        })
    }

    function E() {
        0 === l.size(u) && 0 === l.size(f) && l.defer(function() {
            r.trigger(a.REQUESTED_MODELS_READY)
        })
    }

    function e(e) {
        if ("inline" !== e.getIdType())
            if (f[e.getId()]) {
                f[e.getId()] && delete f[e.getId()];
                E()
            } else window.console.warn("Caffeine Bistro Handler - A clip I don't know anything about just told me it loaded. I'm confused.")
    }
    return {
        requestClips: function(e, t) {
            if (e) {
                void 0 === t && (t = 10);
                t === 1 / 0 && (t = 10);
                0 === t && (t = 1);
                var n = {},
                    a = A + "order?";
                for (var i in e)
                    if (e.hasOwnProperty(i)) {
                        a += i + "=" + e[i] + "&";
                        if ("webRadioId" === i) {
                            var r = !!s.musicDJUid && s.musicDJUid;
                            r && (a += "musicDJUid=" + r + "&")
                        }
                    }
                a = a.slice(0, -1);
                a += "&limit=" + t;
                s.sort && (a += "&sort=" + s.sort);
                s.env && (a += "&env=" + s.env);
                n[a] = new p(e);
                if (0 === Object.keys(n).length) throw new Error("Can not determine any feeds to load with these parameters");
                void 0 === o && (o = Object.keys(n));
                l.extendOwn(u, n);
                l.each(u, function(e, t) {
                    ! function(e) {
                        e.length;
                        l.each(e, function(e) {
                            d[e] = {};
                            CBC.APP.Caffeine.getXhr(e, g, m)
                        })
                    }([t])
                })
            }
        },
        destroy: function() {
            r.off(a.MODEL_READY, e)
        }
    }
});
CBC.APP.Caffeine.addModule("Handlers.Components.DAILoader", function(n) {
    var l = this,
        e = CBC.APP.Caffeine,
        c = e._,
        a = e.getModule("Helpers.Asset"),
        i = e.Constants.Event,
        t = e.Constants.PlayerStatus,
        r = "//imasdk.googleapis.com/js/sdkloader/ima3_dai.js",
        o = !1;
    "object" == typeof window.google && "object" == typeof window.google.ima && "object" == typeof window.google.ima.dai ? s() : CBC.APP.Caffeine.addDependency(r, s, f);

    function s() {
        l.player && l.player.status !== t.READY ? u() : l.on(i.PLAYER_PLAYINTENT, u)
    }

    function u() {
        if (l.currentClip.id === n.id) {
            l.off(i.PLAYER_PLAYINTENT, u);
            var e = n.key;
            CBC.APP.Caffeine.getXhr(e + "&manifest=m3u", d, f)
        }
    }

    function d(e) {
        var t = (new window.DOMParser).parseFromString(this.responseText, "application/xml"),
            n = window.google.ima.dai.api,
            a = l.session.config.caffeineNode.querySelector(".playbackelement video"),
            i = c.values(n.StreamEvent.Type),
            r = new n.StreamManager(a);
        r.addEventListener(i, A, !1);
        var o = t.querySelector("param[name=assetKey]").getAttribute("value"),
            s = new n.LiveStreamRequest;
        s.assetKey = o;
        s.apiKey = "0A753C2A9BE994A75562DD9EAE366396307A3E2F489D692A561E2D3665AB1069";
        r.requestStream(s)
    }

    function f(e) {
        e && window.console.error(e);
        l.trigger(i.PLAYER_SRCERR)
    }

    function A(e) {
        if (!o) {
            var t = window.google.ima.dai.api.StreamEvent.Type;
            switch (e.type) {
                case t.LOADED:
                    ! function(e) {
                        o = !0;
                        var t = new a(e.getStreamData().url, 1 / 0, n.mimeType, n.codec, {
                            isDAI: !0,
                            streamManager: e.target
                        });
                        l.trigger(i.HANDLER_MEDIA_URL_UPDATED, t)
                    }(e);
                    break;
                case t.ERROR:
                    ! function(e) {
                        window.console.error("DAILoader stream error");
                        try {
                            window.console.error(e.getStreamData().errorMessage)
                        } catch (e) {}
                        l.trigger(i.PLAYER_SRCERR)
                    }(e)
            }
        }
    }
    return {
        destroy: function() {
            l.off(i.PLAYER_PLAYINTENT, u)
        }
    }
});
CBC.APP.Caffeine.addModule("Handlers.Components.MediaNetLoader", function(n) {
    var a, i = this,
        e = CBC.APP.Caffeine,
        r = e._,
        o = e.Constants.Event,
        t = e.Constants.ErrorList,
        s = e.Constants.PlayerStatus,
        l = e.getModule("Helpers.Asset"),
        c = 0;
    i.player && i.player.status !== s.READY ? u() : i.on(o.PLAYER_PLAYINTENT, u);

    function u() {
        i.off(o.PLAYER_PLAYINTENT, u);
        if (i.currentClip.id === n.id) {
            if ("object" == typeof a) {
                var e = a.expiry;
                if (Date.now() / 1e3 <= e) {
                    r.defer(A);
                    return
                }
            }
            c++;
            var t = "https://api.radio-canada.ca/validationMedia/v1/Validation.html?appCode=" + n.key.appCode + "&idMedia=" + n.key.wamId + "&deviceType=ipad&connectionType=wifi&multibitrate=false&output=json";
            CBC.APP.Caffeine.getXhr(t, d, f)
        }
    }

    function d(e) {
        var t;
        try {
            t = JSON.parse(this.responseText)
        } catch (e) {
            f();
            return
        }
        if (t.url && !t.errorCode) {
            var n;
            try {
                n = parseInt(t.url.match(/exp=([0-9]*)/)[1])
            } catch (e) {}
            n || (n = Date.now() / 1e3 + 120);
            a = new l(t.url, n, "application/vnd.apple.mpegurl", "mp4a.40.2");
            c = 0;
            A()
        } else f()
    }

    function f(e) {
        c < 5 ? r.delay(u, 500) : i.trigger(o.CAFFEINE_ERROR, t.SMIL_ERROR)
    }

    function A() {
        i.trigger(o.HANDLER_MEDIA_URL_UPDATED, a)
    }
    return {
        destroy: function() {
            i.off(o.PLAYER_PLAYINTENT, u)
        }
    }
});
CBC.APP.Caffeine.addModule("Handlers.Components.PlatformLoader", function(n) {
    var r, o, a = this,
        e = CBC.APP.Caffeine,
        i = e._,
        s = e.Constants.Event,
        t = e.Constants.ErrorList,
        l = e.Constants.PlayerStatus,
        c = e.getModule("Helpers.Asset");
    a.player && a.player.status !== l.READY ? u() : a.on(s.PLAYER_PLAYINTENT, u);

    function u() {
        a.off(s.PLAYER_PLAYINTENT, u);
        if (a.currentClip.id === n.id) {
            if ("object" == typeof o) {
                var e = o.expiry;
                if (Date.now() / 1e3 <= e) {
                    i.defer(A);
                    return
                }
            }
            var t = n.key;
            CBC.APP.Caffeine.getXhr(t, d, f)
        }
    }

    function d(e) {
        var t = (r = (new window.DOMParser).parseFromString(this.responseText, "application/xml")).querySelector("video") || r.querySelector("ref"),
            n = t.getAttribute("src"),
            a = t.getAttribute("type"),
            i = n.match(/~exp=([0-9]+)/);
        i = null === i ? 1 / 0 : 1 < i.length ? i[1] : 1 / 0;
        o = new c(n, i, a, "");
        A()
    }

    function f(e) {
        a.trigger(s.CAFFEINE_ERROR, t.SMIL_ERROR)
    }

    function A() {
        o.id = n.id;
        a.trigger(s.HANDLER_MEDIA_URL_UPDATED, o)
    }
    return {
        destroy: function() {
            a.off(s.PLAYER_PLAYINTENT, u)
        }
    }
});
CBC.APP.Caffeine.addModule("Handlers.Components.SimpleURLLoader", function(e) {
    var t = CBC.APP.Caffeine,
        n = t.getModule("Helpers.Asset"),
        a = t.Constants.Event,
        i = new n(e.key, 1 / 0, e.mimeType, e.codec);
    this.trigger(a.HANDLER_MEDIA_URL_UPDATED, i)
});
CBC.APP.Caffeine.addModule("Handlers.Inline", function() {
    "use strict";
    var i = this,
        n = CBC.APP.Caffeine.Helpers.Event,
        r = [];
    i.on(n.MODEL_READY, e);

    function e(e) {
        if ("inline" === e.getIdType()) {
            var t = r.indexOf(e); - 1 < t && r.splice(t, 1);
            0 === r.length && i.trigger(n.REQUESTED_MODELS_READY)
        }
    }
    return {
        requestClips: function(e) {
            var t, n, a = function(e) {
                var t = e.inline,
                    n = t.src,
                    a = t.mimeType;
                "string" == typeof n && (n = [n]);
                if (!(n instanceof Array)) throw new Error("Inline clip has invalid source");
                for (var i = 0; i < n.length; i++) {
                    var r = "";
                    "string" == typeof a ? r = a : a instanceof Array && (r = a[i]);
                    t.assetDescriptors = [];
                    var o = n[i];
                    "string" == typeof o && t.assetDescriptors.push({
                        loader: "SimpleURLLoader",
                        key: o,
                        mimeType: r
                    })
                }
                t.id = function(e) {
                    for (var t = [], n = 0; n < e.length; n++) t.push(e[n].key);
                    return t.toString()
                }(t.assetDescriptors);
                t.idType = "inline";
                return t
            }(e);
            t = a, n = new CBC.APP.Caffeine.Model(i, t), r.push(n)
        },
        destroy: function() {
            i.off(n.MODEL_READY, e)
        }
    }
});
CBC.APP.Caffeine.addModule("Helpers.Asset", function(e, t, n, a, i) {
    if (!e) throw new Error("Asset requires a url");
    return {get url() {
            return e
        },
        get expiry() {
            return t ? parseInt(t) : 1 / 0
        },
        get mimeType() {
            return n || {
                ".flv": "video/x-flv",
                ".mp4": "video/mp4",
                ".m3u8": "application/x-mpegURL",
                ".ts": "video/MP2T",
                ".3gp": "video/3gpp",
                ".mov": "video/quicktime",
                ".avi": "video/x-msvideo",
                ".wmv": "video/x-ms-wmv",
                ".mp3": "audio/mp3"
            }[/(?:\.([^.]+))?$/.exec(e)[0]]
        },
        get codec() {
            return a || ""
        },
        get extras() {
            return i || {}
        }
    }
});
CBC.APP.Caffeine.addModule("Helpers.AssetDescriptor", function(e, t, n, a, i) {
    if (!t || !n) throw new Error("AssetDescriptor requires a loader and a key");
    var r, o = this,
        s = {get id() {
                return e
            },
            get key() {
                return n
            },
            get mimeType() {
                return a
            },
            get codec() {
                return i
            },
            load: function() {
                r && r.destroy && r.destroy();
                r = o.initModule("Handlers.Components." + t, s)
            },
            unload: function() {
                r && r.destroy && r.destroy()
            },
            toString: function() {
                return t
            }
        };
    return s
});
CBC.APP.Caffeine.addModule("Helpers.DateTimeFormat", {
    getFormattedDuration: function(e) {
        var t = 0 <= e ? "" : "-";
        e = Math.floor(Math.abs(parseFloat(e)));
        var n = Math.floor(e / 3600),
            a = Math.floor(e / 60 % 60),
            i = Math.round(e % 60);
        n && (t += (n < 10 ? "0" + n : n) + ":");
        t += (a < 10 ? "0" + a : a) + ":";
        return t += i < 10 ? "0" + i : i
    },
    getFormattedDate: function(e) {
        var t = new Date(e);
        return ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][t.getMonth()] + " " + t.getDate() + ", " + t.getFullYear()
    },
    getIso8601Date: function(e) {
        var t = new Date(e),
            n = t.getFullYear(),
            a = t.getMonth() + 1;
        return n + "-" + (a = a < 10 ? "0" + a : a) + "-" + (t.getDate() < 10 ? "0" + t.getDate() : t.getDate())
    },
    getProgramGuideDate: function(e) {
        return this.getIso8601Date(e).split("-").join("/")
    },
    Round: function(e, t) {
        var n = Math.pow(10, t),
            a = e * n;
        return Math.round(a) / n
    }
}, !0);
CBC.APP.Caffeine.addModule("Helpers.Utils", {
    getIsInternal: function() {
        var e = CBC.APP.Caffeine.getSettings(),
            t = !1;
        if (e.INTERNAL_IP_MATCH && e.USER_IP) {
            var n = new RegExp(e.INTERNAL_IP_MATCH);
            t = null !== e.USER_IP.match(n)
        }
        return t
    },
    getIsInIframe: function() {
        return window.self !== window.top
    }
}, !0);
CBC.APP.Caffeine.addModule("Players.Barista.Components.AdsErrorManager", function() {
    "use strict";
    var t, e, n, a, i, r, o, s = this,
        l = CBC.APP.Caffeine.Helpers.Event,
        c = CBC.APP.Caffeine.Helpers.ErrorList,
        u = CBC.APP.Caffeine.Constants.PlayerStatus,
        d = CBC.APP.Caffeine.getSettings();
    ! function() {
        A(!0);
        e = window.setTimeout(f, "number" == typeof d.ADS_TIMEOUT ? d.ADS_TIMEOUT : 2e4)
    }();

    function f() {
        p();
        s.trigger(l.CAFFEINE_ERROR, c.AD_FAILURE)
    }

    function A(e) {
        var t = e ? "on" : "off";
        s[t](l.PLAYER_STATUSCHANGE, g);
        s[t](l.ADS_INITIALIZED, m);
        s[t](l.ADS_LOADED, E);
        s[t](l.ADS_STARTED, C);
        s[t](l.ADS_FIRSTQUARTILE, v);
        s[t](l.ADS_MIDPOINT, h);
        s[t](l.ADS_THIRDQUARTILE, y);
        s[t](l.ADS_COMPLETE, P);
        s[t](l.ADS_STOPPED, p);
        s[t](l.ADS_ERRORED, p)
    }

    function p() {
        window.clearTimeout(e);
        window.clearTimeout(n);
        window.clearTimeout(a);
        window.clearTimeout(i);
        window.clearTimeout(r);
        window.clearTimeout(o)
    }

    function g(e) {
        switch (s.player.status) {
            case u.ADVERTISING:
                p();
                ! function() {
                    if (t) {
                        !0 !== n && (n = window.setTimeout(f, 5e3));
                        !0 !== a && (a = window.setTimeout(f, 5e3 + .25 * t.adDuration * 1e3));
                        !0 !== i && (i = window.setTimeout(f, 5e3 + .5 * t.adDuration * 1e3));
                        !0 !== r && (r = window.setTimeout(f, 5e3 + .75 * t.adDuration * 1e3));
                        o = window.setTimeout(f, 5e3 + 1e3 * t.adDuration)
                    }
                }();
                break;
            case u.ADVERTISING_PAUSED:
                p()
        }
    }

    function m() {
        window.clearTimeout(e)
    }

    function E(e) {
        t = e
    }

    function C() {
        window.clearTimeout(n);
        n = !0
    }

    function v() {
        window.clearTimeout(a);
        a = !0
    }

    function h() {
        window.clearTimeout(i);
        i = !0
    }

    function y() {
        window.clearTimeout(r);
        r = !0
    }

    function P() {
        p();
        n = a = i = r = !1
    }
    s.player.adsErrorManager = {
        destroy: function() {
            p();
            A(!1);
            delete s.player.adsErrorManager
        }
    }
});
CBC.APP.Caffeine.addModule("Players.Barista.PlaybackController", function(t, n) {
    "use strict";
    var a, i, r, e, o, s, l, c = this,
        u = CBC.APP.Caffeine._,
        d = CBC.APP.Caffeine.Helpers.Event,
        f = CBC.APP.Caffeine.Constants.PlayerStatus,
        A = c.session.config,
        p = 0,
        g = !1,
        m = !1,
        E = 0,
        C = [];
    ! function() {
        r = n.isAudio() ? t.audio : t.video;
        e = n.isLive() ? "live" : "on-demand", A.caffeineNode.setAttribute("data-type", e);
        var e;
        ! function() {
            var e = A.caffeineNode.querySelector(".playbackelement");
            e && e.appendChild(r);
            r.preload = "metadata";
            r.volume = CBC.APP.Caffeine.volume
        }();
        _(!0);
        s = c.initModule("Players.Components.ChapterManager");
        l = c.initModule("Players.Components.MediaSourceManager", r, n);
        !0 === n.isVideo() && (o = c.initModule("Players.Components.Captions", r, n));
        v(f.READY)
    }();
    return {
        play: x,
        pause: function() {
            switch (i) {
                case f.PAUSED:
                case f.ADVERTISING_PAUSED:
                    return;
                case f.ADVERTISING:
                    if (c.ima.playing) {
                        c.ima.pause();
                        v(f.ADVERTISING_PAUSED)
                    } else r.pause();
                    break;
                case f.PLAYING:
                case f.LOADING:
                    r.pause()
            }
        },
        unpause: function() {
            x()
        },
        stop: function() {
            if (i === f.READY) return;
            P()
        },
        get status() {
            return i
        },
        get currentTime() {
            return z()
        },
        set currentTime(e) {
            return function(e) {
                if (e === z()) return;
                var t = r.duration ? r.duration : n.getDuration();
                e = Math.max(Math.min(t, e), 0);
                r.currentTime = e;
                i !== f.PLAYING && c.trigger(d.PLAYER_TIMEUPDATED, e)
            }(e)
        },
        get duration() {
            return r.duration
        },
        get muted() {
            return r.muted
        },
        set muted(e) {
            t = e, r.muted = t;
            var t
        },
        get volume() {
            return r.volume
        },
        set volume(e) {
            ! function(e) {
                r.volume = e;
                M()
            }(e)
        },
        get currentChapterIndex() {
            return s.index
        },
        captions: o,
        destroy: function() {
            P();
            s && s.destroy();
            c.player.AkamaiAnalytics && c.player.AkamaiAnalytics.destroy();
            l && l.destroy();
            o && o.destroy();
            _(!1);
            for (var e = 0; e < C.length; e++) C[e].removeEventListener("cuechange", F);
            delete c.player
        }
    };

    function v(e) {
        if (!u.contains(f, e)) throw new Error("Cannot set player to invalid status of " + e);
        if (e !== i) {
            var t, n = i;
            i = e;
            A.caffeineNode.setAttribute("data-status", i);
            switch (i) {
                case f.PLAYING:
                    t = d.PLAYER_PLAYING;
                    break;
                case f.PAUSED:
                    t = d.PLAYER_PAUSED;
                    break;
                case f.READY:
                    t = n ? d.PLAYER_STOPPED : d.PLAYER_READY
            }
            t && u.defer(c.trigger, t);
            u.defer(c.trigger, d.PLAYER_STATUSCHANGE, {
                from: n,
                to: e
            })
        }
    }

    function h() {
        window.AKAMAI_MEDIA_ANALYTICS_CONFIG_FILE_PATH || c.initModule("Players.Components.AkamaiAnalytics")
    }

    function y() {
        c.trigger(d.PLAYER_METADATALOADED);
        if ((!c.ima || !c.ima.playing) && g && !r.playing) {
            var e = r.play();
            void 0 !== e && e.then(function() {}).catch(function() {
                i === f.LOADING && v(f.PAUSED)
            })
        }
    }

    function P() {
        r.pause();
        0 < r.currentTime && (r.currentTime = 0);
        g = m = !1;
        v(f.READY);
        c.trigger(d.PLAYER_ENDED)
    }

    function _(e) {
        var t = e ? "addEventListener" : "removeEventListener";
        r[t]("loadedmetadata", y);
        r[t]("loadeddata", T);
        r[t]("playing", L);
        r[t]("timeupdate", b);
        r[t]("pause", I);
        r[t]("volumechange", M);
        r[t]("ended", w);
        r[t]("seeking", Y);
        r[t]("seeked", H);
        r.textTracks[t]("addtrack", V);
        c[t = e ? "on" : "off"](d.PLAYER_READY, h);
        c[t](d.PLAYER_SRCSET, D);
        c[t](d.PLAYER_PLAYBACK_ISSUE, S);
        c[t](d.PLAYER_PLAYBACK_ISSUE_RESOLVED, R);
        c[t](d.ADS_INITIALIZED, N);
        c[t](d.ADS_BREAK_START, k);
        c[t](d.ADS_BREAK_COMPLETE, O);
        c[t](d.ADS_PAUSED, U);
        c[t](d.ADS_RESUMED, q);
        c[t](d.ADS_STOPPED, P);
        c[t](d.ADS_ERRORED, B);
        e ? a = window.setInterval(function() {
            var e = r.buffered;
            if (e.length) {
                if (e.end(e.length - 1) === p) return;
                p = e.end(e.length - 1);
                c.trigger(d.PLAYER_BUFFERUPDATED, e.end(e.length - 1));
                e.end(e.length - 1) >= r.duration && window.clearInterval(a)
            }
        }, 500) : window.clearInterval(a)
    }

    function S() {
        v(f.LOADING)
    }

    function R() {
        v(f.PLAYING)
    }

    function D() {
        E = r.currentTime
    }

    function T() {
        !m && 0 < E && (r.currentTime = E)
    }

    function L() {
        if (!m) {
            m = !0;
            r.volume = CBC.APP.Caffeine.volume;
            u.defer(c.trigger, d.PLAYER_INITIALPLAY)
        }
        switch (i) {
            case f.PLAYING:
                return;
            case f.ADVERTISING:
            case f.ADVERTISING_PAUSED:
                c.ima.playing || v(f.PLAYING);
                break;
            default:
                v(f.PLAYING)
        }
    }

    function b() {
        e !== r.currentTime && c.trigger(d.PLAYER_TIMEUPDATED, r.currentTime);
        e = r.currentTime
    }

    function I() {
        switch (i) {
            case f.PLAYING:
                v(f.PAUSED);
                break;
            default:
                return
        }
    }

    function M() {
        if (CBC.APP.Caffeine.volume !== c.player.volume) {
            CBC.APP.Caffeine.volume = c.player.volume;
            c.trigger(d.PLAYER_VOLUMECHANGED, r.volume)
        }
    }

    function w() {
        c.trigger(d.PLAYER_COMPLETED);
        P();
        A.continuousPlay && c.hasNextTrack() && c.playNextTrack()
    }

    function N() {}

    function k() {
        v(f.ADVERTISING)
    }

    function U() {
        v(f.ADVERTISING_PAUSED)
    }

    function q() {
        v(f.ADVERTISING)
    }

    function O() {
        x()
    }

    function B() {
        v(f.PAUSED);
        x()
    }

    function Y() {
        m && c.trigger(d.PLAYER_SEEKSTART)
    }

    function H() {
        if (m) {
            c.trigger(d.PLAYER_TIMEUPDATED, r.currentTime);
            c.trigger(d.PLAYER_SEEKEND)
        }
    }

    function V(e) {
        var t = e.track;
        if ("metadata" == t.kind) {
            t.mode = "hidden";
            t.addEventListener("cuechange", F);
            C.push(t)
        }
    }

    function F(e) {
        var t = e.target;
        for (var n in t.activeCues) c.initModule("Players.Components.CueParser", t.activeCues[n])
    }

    function x() {
        if (!c.session.capabilities.canAutoplay) throw new Error("Cannot start playback before a user action");
        switch (i) {
            case f.READY:
                ! function() {
                    g = !0;
                    v(f.LOADING);
                    c.trigger(d.PLAYER_PLAYINTENT);
                    r.play()
                }();
                break;
            case f.PLAYING:
                return;
            case f.PAUSED:
                r.play();
                break;
            case f.ADVERTISING:
                if (c.ima.playing);
                else {
                    v(f.PLAYING);
                    r.play()
                }
                break;
            case f.ADVERTISING_PAUSED:
                if (c.ima.playing) c.ima.resume();
                else {
                    v(f.PLAYING);
                    r.play()
                }
        }
    }

    function z() {
        return r.currentTime
    }
});
CBC.APP.Caffeine.addModule("Players.Components.AkamaiAnalytics", function() {
    "use strict";
    var r, t, n = this,
        a = CBC.APP.Caffeine.Helpers.Event,
        e = CBC.APP.Caffeine.Helpers.ErrorList,
        i = "https://ma80-r.analytics.edgekey.net/config/beacon-18697.xml?enableGenericAPI=1",
        o = "https://ma80-r.analytics.edgekey.net/config/beacon-18698.xml?enableGenericAPI=1",
        s = "https://79423.analytics.edgekey.net/js/csma.js",
        l = n.player,
        c = n.session.modelsManager.getClip(l.currentClip);
    ! function() {
        c.isLive() && c.isAudio() ? window.AKAMAI_MEDIA_ANALYTICS_CONFIG_FILE_PATH = i : window.AKAMAI_MEDIA_ANALYTICS_CONFIG_FILE_PATH = o;
        ! function() {
            for (var e = document.getElementsByTagName("script"), t = 0; t < e.length; t++)
                if (e[t].getAttribute("src") === s) return !0;
            return !1
        }() || "function" != typeof AkaHTML5MediaAnalytics ? CBC.APP.Caffeine.addDependency(s, u, d) : u()
    }();

    function u() {
        (r = new AkaHTML5MediaAnalytics({
            streamHeadPosition: f,
            streamLength: A,
            streamURL: p
        })).setData("viewerId", Math.floor(9999999 * Math.random()));
        r.handleSessionInit();
        g(!0);
        ! function() {
            var e, t, n, a, i;
            if (c.isLive() && c.isAudio()) {
                e = c.getCategories();
                t = e[0].name || "";
                n = e[1].name || ""
            } else {
                t = c.getContentArea();
                n = c.getContentArea()
            }
            i = c.isLive() ? "live" : "onDemand";
            a = c.isAudio() ? "audio" : "video";
            r.setData("title", c.getTitle());
            r.setData("eventName", c.getTitle());
            r.setData("category", t);
            r.setData("subCategory", n);
            r.setData("show", c.getShowName());
            r.setData("pageUrl", c.getPageUrl());
            r.setData("pageReferrer", document.referrer);
            r.setData("contentLength", c.getDuration());
            r.setData("contentType", a);
            r.setData("device", "");
            r.setData("deliveryType", i);
            r.setData("playerId", a + "-" + i)
        }()
    }

    function d() {
        n.trigger(a.CAFFEINE_ERROR, e.AKAMAI_ANALYTICS_ERROR)
    }

    function f() {
        return Math.round(l.currentTime)
    }

    function A() {
        return c.getDuration()
    }

    function p() {
        return t
    }

    function g(e) {
        var t = e ? "on" : "off";
        n[t](a.PLAYER_SRCSET, m);
        n[t](a.PLAYER_PLAYING, E);
        n[t](a.PLAYER_PAUSED, C);
        n[t](a.PLAYER_STOPPED, v);
        n[t](a.PLAYER_ENDED, h);
        n[t](a.CAFFEINE_ERROR, y);
        n[t](a.PLAYER_BITRATECHANGED, P);
        n[t](a.PLAYER_STATUSCHANGE, _);
        n[t](a.ADS_LOADED, S);
        n[t](a.ADS_STARTED, R);
        n[t](a.ADS_FIRSTQUARTILE, D);
        n[t](a.ADS_MIDPOINT, T);
        n[t](a.ADS_THIRDQUARTILE, L);
        n[t](a.ADS_COMPLETE, b);
        n[t](a.ADS_STOPPED, I);
        n[t](a.ADS_ERRORED, M)
    }

    function m(e) {
        t = e
    }

    function E() {
        r.handlePlaying()
    }

    function C() {
        r.handlePause()
    }

    function v() {
        r.handlePlayEnd("User.Stop.Button")
    }

    function h() {
        r.handlePlayEnd("Play.End.Detected")
    }

    function y(e) {
        e.track && r.handleError("Error " + e.code + " " + e.debug)
    }

    function P(e) {
        r.handleBitRateSwitch(e)
    }

    function _(e) {
        e.to === CBC.APP.Caffeine.Constants.PlayerStatus.LOADING && r.handleBufferStart();
        e.from === CBC.APP.Caffeine.Constants.PlayerStatus.LOADING && r.handleBufferEnd()
    }

    function S(e) {
        r.handleAdLoaded(e)
    }

    function R() {
        r.handleAdStarted()
    }

    function D() {
        r.handleAdFirstQuartile()
    }

    function T() {
        r.handleAdMidPoint()
    }

    function L() {
        r.handleAdThirdQuartile()
    }

    function b() {
        r.handleAdCompleted()
    }

    function I() {
        r.handleAdStopped()
    }

    function M() {
        r.handleAdError()
    }
    n.player.AkamaiAnalytics = {
        destroy: function() {
            g(!1);
            delete n.player.AkamaiAnalytics;
            delete window.AKAMAI_MEDIA_ANALYTICS_CONFIG_FILE_PATH
        }
    }
});
CBC.APP.Caffeine.addModule("Players.Components.Captions", function(e, t) {
    "use strict";
    var n, a, i, r = this,
        o = CBC.APP.Caffeine.Helpers.Event,
        s = e,
        l = t.getCaptions(),
        c = !1,
        u = CBC.APP.Caffeine.captions,
        d = !1;
    ! function() {
        0 < s.textTracks.length && !0 === t.isLive() && r.on(o.PLAYER_PLAYING, f);
        s.textTracks.addEventListener("addtrack", f);
        (i = r.session.config.container.querySelector("button.captions")).removeAttribute("disabled");
        i.addEventListener("click", E);
        C()
    }();
    l && ("object" == typeof(n = t.getCaptions()) ? A() : r.on(o.HANDLER_MEDIA_URL_UPDATED, A));

    function f() {
        i.classList.remove("unavailable");
        c = !0;
        C()
    }

    function A() {
        if ("object" == typeof(n = t.getCaptions()) && n.src) {
            r.off(o.HANDLER_MEDIA_URL_UPDATED, A);
            s.textTracks.removeEventListener("addtrack", f);
            (a = document.createElement("track")).src = n.src;
            a.srclang = n.lang;
            a.kind = "captions";
            a.default = !0;
            a.label = "English";
            s.setAttribute("crossorigin", "anonymous");
            s.addEventListener("loadedmetadata", p);
            s.addEventListener("playing", m)
        }
    }

    function p() {
        s.removeEventListener("loadedmetadata", p);
        a.addEventListener("load", g);
        s.appendChild(a)
    }

    function g() {
        a.removeEventListener("load", g);
        c = !0;
        C()
    }

    function m() {
        v(CBC.APP.Caffeine.captions)
    }

    function E() {
        v(!u)
    }

    function C() {
        i.classList.add(u ? "on" : "off");
        i.classList.remove(u ? "off" : "on");
        if (c) {
            d || function() {
                if (0 < s.textTracks.length)
                    for (var e = 0; e < s.textTracks.length; e++)
                        if ("captions" === s.textTracks[e].kind || "subtitles" === s.textTracks[e].kind) {
                            d = s.textTracks[e];
                            return
                        }
            }();
            d && (d.mode = u ? "showing" : "hidden")
        }
    }

    function v(e) {
        u = !0 === e;
        CBC.APP.Caffeine.captions = u;
        C()
    }
    return {get hasCaptions() {
            return !!n
        },
        get captions() {
            return u
        },
        set captions(e) {
            v(e)
        },
        destroy: function() {
            c = !1;
            s.contains(a) && s.removeChild(a);
            s.textTracks.removeEventListener("addtrack", f);
            s.removeEventListener("loadedmetadata", p);
            s.removeEventListener("playing", m);
            if (i) {
                i.setAttribute("disabled", "");
                i.removeEventListener("click", E)
            }
            a && a.removeEventListener("load", g)
        }
    }
});
CBC.APP.Caffeine.addModule("Players.Components.ChapterManager", function() {
    "use strict";
    var t, n, a = this,
        i = CBC.APP.Caffeine._,
        r = CBC.APP.Caffeine.Helpers.Event,
        o = CBC.APP.Caffeine.Constants.PlayerStatus,
        s = -1,
        l = [];
    a.on(r.PLAYER_READY, function e() {
        a.off(r.PLAYER_READY, e);
        t = a.session.modelsManager.getClip(a.player.currentClip);
        c();
        u(!0)
    });

    function c() {
        var e = -1; - 1 < s && (e = l[s].startTime);
        l = a.session.modelsManager.getClip(a.player.currentClip).getChapters();
        s = i.findLastIndex(l, {
            startTime: e
        })
    }

    function u(e) {
        var t = e ? "on" : "off";
        a[t](r.PLAYER_COMPLETED, f, !0);
        a[t](r.PLAYER_STATUSCHANGE, d)
    }

    function d(e) {
        switch (e.to) {
            case o.PLAYING:
                n = window.clearInterval(n);
                a.on(r.PLAYER_TIMEUPDATED, A);
                break;
            default:
                if ("number" == typeof n) return;
                a.off(r.PLAYER_TIMEUPDATED, A);
                n = window.setInterval(A, 1e3)
        }
    }

    function f() {
        a.trigger(r.CHAPTER_END, l.length - 1);
        s = -1
    }

    function A() {
        if (a.player.status === o.PLAYING) {
            var e = p();
            if (e != s) {
                0 <= s && i.defer(a.trigger, r.CHAPTER_END, s);
                i.defer(a.trigger, r.CHAPTER_START, e)
            }
            s = e
        }
    }

    function p() {
        return function(e) {
            if (0 === l.length) return -1;
            if (e < l[0].startTime) return -1;
            for (var t = l.length - 1; 0 <= t; t--)
                if (e >= l[t].startTime) return t;
            return -1
        }(t && t.isLive() ? Date.now() : Math.floor(a.player.currentTime))
    }
    return {get index() {
            return p()
        },
        get currentChapter() {
            return l[p()]
        },
        destroy: function() {
            window.clearInterval(n);
            u(!1);
            a.off(r.PLAYER_TIMEUPDATED, A);
            delete a.player.chapterManager
        }
    }
});
CBC.APP.Caffeine.addModule("Players.Components.CueParser", function(e) {
    if ("object" == typeof e) {
        var t, n = CBC.APP.Caffeine.Constants.Event,
            a = window.DataCue || window.VTTCue;
        void 0 !== a && e instanceof a ? t = e.data instanceof ArrayBuffer ? new Uint8Array(e.data) : e.value : e instanceof ArrayBuffer && (t = (t = e.data) instanceof ArrayBuffer ? new Uint8Array(t) : null);
        t instanceof Uint8Array && (t = function(e) {
            var t, n = "",
                a = "";
            if (73 !== e[0] || 68 !== e[1] || 51 !== e[2]) return null;
            t = 10;
            for (; t < 14 && t < e.length && !(e[t] < 32);) {
                n += String.fromCharCode(e[t]);
                t++
            }
            t = 22;
            for (; t < e.length;) {
                32 <= e[t] && (a += String.fromCharCode(e[t]));
                t++
            }
            return {
                key: n,
                data: a
            }
        }(t));
        t.value && !t.data && (t.data = t.value);
        if (t) this.trigger(n.PLAYER_CUE_CHANGED, t);
        else {
            window.console.warn("CueParser could not parse this received cue:");
            window.console.warn(e)
        }
    }
});
CBC.APP.Caffeine.addModule("Players.Components.DockManager", function(t) {
    "use strict";
    var a, n, i, r, o, s, l, c, u = this,
        d = CBC.APP.Caffeine.Helpers.Event,
        e = CBC.APP.Caffeine.Constants.PlayerStatus,
        f = u.session.config,
        A = !1,
        p = !1,
        g = CBC.APP.Caffeine.autoDockDisabled;
    ! function() {
        a = f.container.querySelector(".cbc-caffeine-container");
        ! function() {
            n = a.querySelector(".ui .extras");
            var e = CBC.APP.Caffeine.Templates["UI_DockManager.html"]();
            n.querySelector("button.fullscreen").insertAdjacentHTML("beforebegin", e);
            i = n.querySelector("button.dock")
        }();
        ! function() {
            var e = CBC.APP.Caffeine.Templates["DockManager.html"]({
                thumbnail: f.thumbnail || t.getThumbnail() || "//www.cbc.ca/player/images/default_video_image.jpg"
            });
            f.container.insertAdjacentHTML("afterbegin", e);
            r = f.container.querySelector(".cbc-caffeine-dock-placeholder");
            s = r.querySelector("button.stop");
            l = r.querySelector("button.play");
            c = r.querySelector("button.pause");
            o = r.querySelector("button.undock")
        }();
        h(!0);
        u.on(d.PLAYER_RESIZED, y)
    }();

    function m() {
        window.removeEventListener("scroll", L);
        if (A) {
            E();
            i.focus()
        } else {
            C();
            o.focus()
        }
    }

    function E() {
        window.removeEventListener("scroll", L);
        if (p) {
            g = !0;
            CBC.APP.Caffeine.autoDockDisabled = !0
        }
        v()
    }

    function C() {
        if (!A) {
            a.classList.add("docked");
            r.style.display = "flex";
            A = !0;
            i.setAttribute("aria-label", "Undock player");
            i.querySelector("span").innerHTML = "Undock player";
            u.off(d.PLAYER_RESIZED, y);
            u.trigger(d.PLAYER_DOCKED)
        }
    }

    function v() {
        if (A) {
            p = !1;
            a.classList.remove("docked");
            r.style.display = "none";
            A = !1;
            i.setAttribute("aria-label", "Dock player to corner of screen");
            i.querySelector("span").innerHTML = "Dock player to corner of screen";
            u.on(d.PLAYER_RESIZED, y);
            u.trigger(d.PLAYER_UNDOCKED)
        }
    }

    function h(e) {
        var t = e ? "addEventListener" : "removeEventListener";
        i[t]("click", m);
        o[t]("click", E);
        s[t]("click", T);
        l[t]("click", R);
        c[t]("click", D);
        f.autoDock && !g && window[t]("scroll", L);
        u[t = e ? "on" : "off"](d.PLAYER_STATUSCHANGE, S);
        u[t](d.SESSION_FULLSCREEN_ON, P);
        u[t](d.SESSION_FULLSCREEN_OFF, _)
    }

    function y(e) {
        r.style.width = e.width + "px";
        r.style.height = e.height + "px"
    }

    function P() {
        v();
        i.disabled = !0
    }

    function _() {
        i.disabled = !1
    }

    function S(e) {
        r.setAttribute("data-status", e.to)
    }

    function R() {
        u.player.play()
    }

    function D() {
        u.player.pause()
    }

    function T() {
        u.player.stop()
    }

    function L() {
        if (u.player.status === e.PLAYING)
            if (A && b()) v();
            else if (!A && !b()) {
            p = !0;
            C()
        }
    }

    function b() {
        var e, t = (e = A ? r.getBoundingClientRect() : a.getBoundingClientRect()).top + e.height / 3 <= 0,
            n = e.bottom >= document.documentElement.clientHeight + e.height / 3;
        return !(t || n)
    }
    return {
        destroy: function() {
            h(!1);
            u.off(d.PLAYER_RESIZED, y);
            f.container.removeChild(r);
            n.removeChild(i);
            v()
        }
    }
});
CBC.APP.Caffeine.addModule("Players.Components.HlsjsManager", function(e, t) {
    var n, a, i = this,
        r = CBC.APP.Caffeine,
        o = r.Constants.Event,
        s = r.Constants.ErrorList;
    "function" == typeof window.Hls ? l() : CBC.APP.Caffeine.addDependency("//www.cbc.ca/i/caffeine/js/libs/hls.min.js", l);

    function l() {
        if (Hls.isSupported()) {
            (n = new Hls({})).startLevel = -1;
            c(!0);
            n.attachMedia(e)
        } else i.trigger(o.HLSJS_ERRORED, s.HLSJS_DEFAULT_ERROR)
    }

    function c(e) {
        var t = e ? "on" : "off";
        n[t](Hls.Events.ERROR, d);
        n[t](Hls.Events.MEDIA_ATTACHED, f);
        n[t](Hls.Events.LEVEL_SWITCH, A);
        n[t](Hls.Events.STREAM_STATE_TRANSITION, u)
    }

    function u(e, t) {
        a = t.nextState
    }

    function d(e, t) {
        switch (t.type) {
            case Hls.ErrorTypes.NETWORK_ERROR:
                t.fatal && i.trigger(o.HLSJS_ERRORED, s.HLSJS_NETWORK_ERROR);
                break;
            case Hls.ErrorTypes.MEDIA_ERROR:
                if ("bufferAppendError" === t.details && "PARSING" === a) {
                    i.trigger(o.CAFFEINE_ERROR, s.HLSJS_DEFAULT_ERROR);
                    n.recoverMediaError()
                }
                break;
            default:
                t.fatal && i.trigger(o.HLSJS_ERRORED, s.HLSJS_DEFAULT_ERROR)
        }
    }

    function f() {
        n.loadSource("/assets/mp3/Northwind_2019-02-01-14-09_cbc_r1_ykn.mp3")
    }

    function A(e, t) {
        i.trigger(o.PLAYER_BITRATECHANGED, n.levels[t.level].bitrate)
    }
    return {
        destroy: function() {
            if (n) {
                c(!1);
                n.destroy()
            }
        }
    }
});
CBC.APP.Caffeine.addModule("Players.Components.MediaSessionAPI", function() {
    "use strict";
    var t, n = this,
        a = CBC.APP.Caffeine.Helpers.Event,
        i = CBC.APP.Caffeine.Constants.PlayerStatus,
        r = navigator.mediaSession,
        o = window.MediaMetadata;
    CBC.APP.Caffeine._.defer(function() {
        if (!r) return;
        e(!0)
    });

    function e(e) {
        var t;
        n[t = e ? "on" : "off"](a.PLAYER_READY, s);
        n[t](a.PLAYER_STATUSCHANGE, l);
        n[t](a.SESSION_QUEUEUPDATED, c);
        if (e) {
            r.setActionHandler("play", u);
            r.setActionHandler("pause", d);
            r.setActionHandler("seekbackward", A);
            r.setActionHandler("seekforward", f);
            c()
        }
    }

    function s() {
        t = n.session.modelsManager.getClip(n.player.currentClip);
        navigator.mediaSession.metadata = (e = t.getHostImage() || t.getThumbnail() || "//www.cbc.ca/player/images/default_video_image.jpg", new o({
            title: t.getTitle(),
            artist: t.getShowName(),
            album: t.getDescription(),
            artwork: [{
                src: e + "?resize=*:96px&crop=h:h;*,*",
                sizes: "96x96",
                type: "image/jpeg"
            }, {
                src: e + "?resize=*:128px&crop=h:h;*,*",
                sizes: "128x128",
                type: "image/jpeg"
            }, {
                src: e + "?resize=*:192px&crop=h:h;*,*",
                sizes: "192x192",
                type: "image/jpeg"
            }, {
                src: e + "?resize=*:256px&crop=h:h;*,*",
                sizes: "256x256",
                type: "image/jpeg"
            }, {
                src: e + "?resize=*:384px&crop=h:h;*,*",
                sizes: "384x384",
                type: "image/jpeg"
            }, {
                src: e,
                sizes: "512x512",
                type: "image/jpeg"
            }]
        }));
        var e
    }

    function l(e) {
        switch (e.to) {
            case i.PLAYING:
            case i.ADVERTISING:
                r.playbackState = "playing";
                break;
            case i.PAUSED:
            case i.ADVERTISING_PAUSED:
                r.playbackState = "paused";
                break;
            default:
                r.playbackState = "none"
        }
    }

    function c() {
        if (1 < n.queue.length) {
            r.setActionHandler("previoustrack", p);
            r.setActionHandler("nexttrack", g)
        } else {
            r.setActionHandler("previoustrack", null);
            r.setActionHandler("nexttrack", null)
        }
    }

    function u() {
        n.play()
    }

    function d() {
        n.pause()
    }

    function f() {
        n.player.currentTime += 30
    }

    function A() {
        n.player.currentTime -= 15
    }

    function p() {
        n.playPrevTrack()
    }

    function g() {
        n.playNextTrack()
    }
    n.mediaSession = {
        destroy: function() {
            e(!1);
            if (r) {
                r.metadata = null;
                r.playbackState = "none"
            }
            delete n.mediaSession
        }
    }
});
CBC.APP.Caffeine.addModule("Players.Components.MediaSourceManager", function(a, e) {
    var t, n, i, r = this,
        o = CBC.APP.Caffeine.Helpers.Event,
        s = CBC.APP.Caffeine.Helpers.ErrorList,
        l = 3,
        c = [],
        u = -1,
        d = null,
        f = null,
        A = CBC.APP.Caffeine._,
        p = 0,
        g = {
            "application/vnd.apple.mpegurl": "HLS",
            "application/x-mpegurl": "HLS",
            "application/x-mpegURL": "HLS"
        };
    ! function() {
        c = e.getAssetDescriptors();
        m(!0);
        E()
    }();

    function m(e) {
        var t = e ? "on" : "off";
        r[t](o.PLAYER_SRCERR, E);
        r[t](o.HLSJS_ERRORED, E);
        r[t](o.PLAYER_PLAYINTENT, v);
        r[t](o.PLAYER_ENDED, y)
    }

    function E() {
        try {
            C()
        } catch (e) {
            window.console.error(e);
            A.defer(function() {
                r.trigger(o.CAFFEINE_ERROR, s.NO_COMPATIBLE_SOURCES)
            })
        }
    }

    function C() {
        if (!L()) throw new Error(s.NO_COMPATIBLE_SOURCES.debug); - 1 < u && y();
        u++;
        a.addEventListener("loadedmetadata", _);
        a.addEventListener("error", P);
        d = c[u];
        r.on(o.HANDLER_MEDIA_URL_UPDATED, h);
        i = r.initModule("Players.Components.PlaybackMonitor", a);
        d.load()
    }

    function v() {
        if (f)
            if (f.expiry <= Date.now() / 1e3) {
                u--;
                C()
            } else a.src || h(f)
    }

    function h(e) {
        ! function(e) {
            var t = e.mimeType;
            e.codec && (t += '; codecs="' + e.codec + '"');
            var n = document.createElement(a.tagName);
            return n && n.canPlayType && !!n.canPlayType(t)
        }(f = e) ? T(f) ? function() {
            if ("HLS" === T(f)) try {
                t = r.initModule("Players.Components.HlsjsManager", a, f)
            } catch (e) {
                E()
            }
        }() : E(): a.src = "/assets/mp3/Northwind_2019-02-01-14-09_cbc_r1_ykn.mp3"
    }

    function y() {
        r.off(o.HANDLER_MEDIA_URL_UPDATED, h);
        a.removeEventListener("loadedmetadata", _);
        a.removeEventListener("error", P);
        a.removeEventListener("error", S);
        t && t.destroy();
        if (i) {
            i.destroy();
            i = null
        }
        f && f.expiry < Date.now() / 1e3 && (a.src = null);
        if (d) {
            d.unload();
            d = null
        }
    }

    function P(e) {
        a.removeEventListener("loadedmetadata", _);
        a.removeEventListener("error", P);
        E()
    }

    function _(e) {
        a.removeEventListener("loadedmetadata", _);
        a.removeEventListener("error", P);
        a.addEventListener("error", S);
        r.trigger(o.PLAYER_SRCSET, "/assets/mp3/Northwind_2019-02-01-14-09_cbc_r1_ykn.mp3")
    }

    function S() {
        a.addEventListener("playing", R);
        if (l <= p) E();
        else {
            p++;
            n = setTimeout(D, 2e3 * p)
        }
    }

    function R(e) {
        a.removeEventListener("playing", R);
        p = 0
    }

    function D() {
        a.load();
        a.play()
    }

    function T(e) {
        return g[e.mimeType]
    }

    function L() {
        return c.length - 1 > u
    }
    r.session.mediaSourceManager = {
        destroy: function() {
            y();
            n = clearTimeout(n);
            a.removeEventListener("loadedmetadata", _);
            a.removeEventListener("error", P);
            a.removeEventListener("error", S);
            a.removeAttribute("src");
            a.load();
            m(!1);
            delete r.session.mediaSourceManager
        },
        tryNextSource: C,
        get hasNextSource() {
            return L()
        },
        get currentSource() {
            return {
                descriptor: c[u],
                asset: f
            }
        }
    };
    return r.session.mediaSourceManager
});
CBC.APP.Caffeine.addModule("Players.Components.PlaybackMonitor", function() {
    var t, n, a, i, e, r, o = this,
        s = o.session.config,
        l = CBC.APP.Caffeine.Helpers.Event,
        c = CBC.APP.Caffeine.Helpers.ErrorList,
        u = CBC.APP.Caffeine.Constants.PlayerStatus,
        d = 333,
        f = 5e3,
        A = 25e3,
        p = 0,
        g = !1;
    o.on(l.PLAYER_PLAYINTENT, m);
    o.on(l.PLAYER_STATUSCHANGE, S);
    o.on(l.PLAYER_SEEKSTART, R);
    o.on(l.PLAYER_SEEKEND, D);
    o.on(l.PLAYER_ENDED, E);
    o.on(l.PLAYER_TIMEUPDATED, P);

    function m() {
        t = o.player;
        a = window.setTimeout(y, f)
    }

    function E() {
        a = window.clearTimeout(a)
    }

    function C(e) {
        v(g = !1);
        o.trigger(l.PLAYER_SRCERR)
    }

    function v(e) {
        if (e && !n) n = setInterval(h, d);
        else if (!e && n) {
            n = clearInterval(n);
            r = clearTimeout(r);
            i = clearTimeout(i)
        }
    }

    function h() {
        if (t.currentTime <= p) y();
        else {
            P();
            p = t.currentTime
        }
    }

    function y() {
        if (!g) {
            g = !0;
            o.trigger(l.PLAYER_PLAYBACK_ISSUE);
            r = setTimeout(C, A, c.TIMEOUT_ERROR);
            i = setTimeout(_, f, !0);
            e = clearTimeout(e)
        }
    }

    function P() {
        if (g) {
            g = !1;
            o.trigger(l.PLAYER_PLAYBACK_ISSUE_RESOLVED);
            r = clearTimeout(r);
            i = clearTimeout(i);
            e = setTimeout(_, f, !1)
        }
    }

    function _(e) {
        var t = s.caffeineNode.querySelector(".playbackelement");
        t && (e ? t.classList.add("playback-unreliable") : t.classList.remove("playback-unreliable"))
    }

    function S(e) {
        t || (t = o.player);
        switch (e.to) {
            case u.LOADING:
                break;
            case u.PLAYING:
                a = window.clearTimeout(a);
                p = t.currentTime;
                P();
                v(!0);
                break;
            case u.READY:
                v(!1);
                p = 0;
                break;
            default:
                a = window.clearTimeout(a);
                v(!1)
        }
    }

    function R(e) {
        v(!1)
    }

    function D(e) {
        p = t.currentTime;
        o.player.status === u.PLAYING && v(!0)
    }
    return {
        destroy: function() {
            v(!1);
            p = 0;
            a = clearTimeout(a);
            i = clearTimeout(i);
            e = clearTimeout(e);
            r = clearTimeout(r);
            _(!1);
            o.off(l.PLAYER_PLAYINTENT, m);
            o.off(l.PLAYER_STATUSCHANGE, S);
            o.off(l.PLAYER_SEEKSTART, R);
            o.off(l.PLAYER_SEEKEND, D);
            o.off(l.PLAYER_ENDED, E);
            o.off(l.PLAYER_TIMEUPDATED, P)
        }
    }
});
CBC.APP.Caffeine.addModule("Players.Components.PlaybackStats", function(e) {
    "use strict";
    var s, n, l = this,
        t = l.session.config,
        a = CBC.APP.Caffeine.Helpers.Event,
        i = l.session.modelsManager.getClip(l.player.currentClip);
    ! function() {
        var e = CBC.APP.Caffeine.Templates["PlaybackStats.html"]({
            config: t,
            dataModel: i,
            status: l.player.status
        });
        s = l.player.ui.addOverlay("playbackstats", e);
        n = l.player.ui.addContextMenuItem("Playback Details");
        r(!0)
    }();

    function r(e) {
        var t = e ? "on" : "off";
        l[t](a.PLAYER_STATUSCHANGE, c);
        l[t](a.PLAYER_SRCSET, u);
        l[t](a.PLAYER_BITRATECHANGED, o);
        l[t](a.HANDLER_MEDIA_URL_UPDATED, d);
        n[t = e ? "addEventListener" : "removeEventListener"]("click", p);
        s.querySelector(".mediaUrl")[t]("click", f);
        s.querySelector("button")[t]("click", A)
    }

    function o(e) {
        s.querySelector(".bitrate").innerText = e
    }

    function c() {
        s.querySelector(".status").innerText = l.player.status
    }

    function u(e) {
        s.querySelector(".mediaUrl").value = e;
        l.session.conductrics && (s.querySelector(".testgroup").innerText = l.session.conductrics.selection || "None")
    }

    function d(e) {
        l.session.mediaSourceManager && (s.querySelector(".assetLoader").innerText = l.session.mediaSourceManager.currentSource.descriptor + " via " + l.session.mediaSourceManager.currentSource.descriptor.key)
    }

    function f(e) {
        this.select()
    }

    function A(e) {
        s.querySelector(".eventsLog").value = "";
        for (var t, n, a, i = l.eventLogger.getEventsFromLast(), r = 0; r < i.length; r++) {
            var o = CBC.APP.Caffeine.Templates["EventLoggerOutput.html"]({
                name: i[r].name,
                data: (a = i[r].args, JSON.stringify(a, function(e, t) {
                    if (t instanceof Function || "function" == typeof t) return t.toString();
                    if (Array.isArray(t)) {
                        var n;
                        try {
                            n = JSON.stringify(t)
                        } catch (e) {
                            n = "could not retrieve args"
                        }
                        return n.replace(/[\[\]"]+/g, "")
                    }
                    return t
                })),
                seconds: (t = i[0].timestamp, n = i[r].timestamp, (n - t) / 1e3),
                first: i[0].name
            });
            s.querySelector(".eventsLog").value += o
        }
    }

    function p() {
        l.player.ui.openOverlay("playbackstats")
    }
    l.player.playbackStats = {
        show: p,
        destroy: function() {
            r(!1);
            delete l.player.playbackStats
        }
    }
});
CBC.APP.Caffeine.addModule("Players.Components.Recommendations", function() {
    "use strict";
    var a, r, o = this,
        e = CBC.APP.Caffeine,
        i = CBC.APP.Caffeine._,
        t = e.Helpers.Event,
        s = o.session.config,
        n = .75,
        l = 5,
        c = !1,
        u = !1,
        d = !1,
        f = null;
    if (!(!o.currentClip.isVideo || o.session.config.continuousPlay && o.hasNextTrack())) {
        o.on(t.PLAYER_RESIZED, R);
        o.on(t.PLAYER_TIMEUPDATED, A);
        o.on(t.PLAYER_STATUSCHANGE, p);
        return {
            destroy: D
        }
    }

    function A(e) {
        c ? c && !u && d && o.currentClip.duration - n <= e && function() {
            u = !0;
            o.player.ui.openOverlay("recommendationcontent")
        }() : o.currentClip.duration - l <= e && function() {
            var e = CBC.APP.Caffeine.Templates["Recommendations.html"]();
            if (!a) {
                a = o.player.ui.addOverlay("recommendationcontent", e, !0);
                r = s.caffeineNode.querySelector(".recommendations");
                var t = {
                    width: s.caffeineNode.offsetWidth,
                    height: s.caffeineNode.offsetHeight
                };
                R(t)
            }
            var n = function() {
                var e, t, n, a = o.currentClip.contentArea;
                if (void 0 === a) return;
                e = "News" === a ? "a67db5" : "bf73e4";
                var i = o.session.userManager.getRecSysUserId();
                if (void 0 === i) return;
                t = Object.keys(i);
                n = i[t];
                return "https://www.cbc.ca/bistro/recommend?recSysExpId=" + e + "&recSysIdType=" + t + "&recSysUserId=" + n + "&limit=4"
            }();
            n && CBC.APP.Caffeine.getXhr(n, g, D, !1);
            c = !0
        }()
    }

    function p(e) {
        if (e.to === CBC.APP.Caffeine.Constants.PlayerStatus.PLAYING) {
            d = c = !1;
            if (u) {
                o.player.ui.closeOverlay();
                u = !1
            }
        }
    }

    function g() {
        for (var e = {}, t = JSON.parse(this.responseText).items, n = 0; n < t.length; n++) {
            var a = new CBC.APP.Caffeine.Model(o, t[n]);
            e[a.getId()] = a
        }! function(e) {
            if (0 === Object.keys(e).length) {
                D();
                return
            }
            d = !0;
            var t = r.querySelector(".recommended-items");
            t.innerHTML = "";
            i.each(e, function(e) {
                t = e, n = CBC.APP.Caffeine.Templates["RecommendedItem.html"]({
                    dataModel: t
                }), r.querySelector(".recommended-items").insertAdjacentHTML("beforeend", n);
                var t, n
            });
            m(!0);
            t.classList.add("loaded");
            A(o.player.currentTime)
        }(e)
    }

    function m(e) {
        if (r)
            for (var t = e ? "addEventListener" : "removeEventListener", n = r.querySelectorAll(".recommended-item"), a = 0; a < n.length; a++) {
                var i = n[a];
                i[t]("click", v);
                i[t]("mouseenter", h);
                i[t]("mouseleave", y);
                i[t]("touchstart", C);
                i.querySelector("a")[t]("focus", P);
                i.querySelector("a")[t]("blur", _)
            }
    }

    function E(e) {
        if (f) {
            f.classList.remove("focused");
            f = null
        }
        if (e) {
            e.classList.add("focused");
            f = e
        }
    }

    function C(e) {
        e.preventDefault();
        e.currentTarget.classList.contains("focused") ? S(e.currentTarget) : E(e.currentTarget)
    }

    function v(e) {
        e.preventDefault();
        S(e.currentTarget)
    }

    function h(e) {
        E(this)
    }

    function y(e) {
        E()
    }

    function P(e) {
        E(e.currentTarget.parentNode)
    }

    function _(e) {
        E()
    }

    function S(e) {
        var t = e.querySelector("a").getAttribute("data-id");
        o.playById({
            mediaId: t
        })
    }

    function R(e) {
        r && (e.width < 720 ? r.classList.remove("four-up") : r.classList.add("four-up"))
    }

    function D() {
        m(!1);
        o.off(t.PLAYER_RESIZED, R);
        o.off(t.PLAYER_TIMEUPDATED, A);
        o.off(t.PLAYER_STATUSCHANGE, p);
        a && a.parentElement.removeChild(a)
    }
});
CBC.APP.Caffeine.addModule("Players.Components.Share", function(t) {
    "use strict";
    var n, a, i, r, o = this,
        s = CBC.APP.Caffeine._,
        l = o.session.config,
        c = o.session.modelsManager.getClip(o.player.currentClip),
        u = l.shareUrl || c.getPageUrl();
    ! function() {
        var e = CBC.APP.Caffeine.Templates["Share.html"]({
            config: l,
            dataModel: c,
            shareUrl: u
        });
        n = o.player.ui.addOverlay("sharecontent", e);
        a = n.querySelector(".share-icon-embed");
        i = n.querySelector(".share-icon-link");
        n.addEventListener("click", d);
        t && l.container.querySelector(t).addEventListener("click", A)
    }();

    function d(e) {
        if (e.target.classList.contains("share-icon"))
            if (e.target === a || e.target === i) ! function(e) {
                r && f(r);
                var t = (r = e).querySelector(".share-icon"),
                    n = e.querySelector("input");
                t.classList.add("active");
                n.select();
                n.focus()
            }(e.target.parentNode);
            else {
                var t = e.target.dataset.href;
                t && window.open(t, "_blank")
            }
        e.target.classList.contains("close-button") && function() {
            if (r) {
                f(r);
                return
            }
            o.player.ui.closeOverlay()
        }()
    }

    function f(e) {
        s.isElement(e) || (e = r);
        var t = e.querySelector(".share-icon");
        t.classList.remove("active");
        r = null;
        t.focus()
    }

    function A(e) {
        p()
    }

    function p() {
        o.player.ui.openOverlay("sharecontent")
    }
    o.player.share = {
        show: p,
        destroy: function() {
            t && l.container.querySelector(t).removeEventListener("click", A);
            n.removeEventListener("click", d);
            delete o.player.share
        }
    }
});
CBC.APP.Caffeine.addModule("Players.Components.UI", function(f) {
    "use strict";
    var A, r, p, n, t, e, g = this,
        m = CBC.APP.Caffeine,
        a = m._,
        i = m.Helpers.DateTimeFormat,
        E = m.Helpers.Event,
        C = m.Constants.PlayerStatus,
        v = g.session.config,
        o = {},
        s = {},
        l = v.caffeineNode.contextMenu || v.caffeineNode.querySelector("menu[type=context]"),
        c = {},
        u = [],
        d = null,
        h = null,
        y = !1,
        P = .5625;
    ! function() {
        ! function() {
            var e = v.caffeineNode.querySelector(".cbc-caffeine-error-messages");
            e && e.innerHTML && (e.innerHTML = "");
            var t = m.Templates["UI.html"]({
                    dataModel: f,
                    config: v,
                    canChangeVolume: g.session.capabilities.canChangeVolume,
                    thumbnail: v.thumbnail || f.getThumbnail() || "//www.cbc.ca/player/images/default_video_image.jpg"
                }),
                n = "barista-audio";
            (f.isVideo() || v.forceVideo) && (n = "barista-video");
            v.caffeineNode.setAttribute("data-player", n);
            var a = v.caffeineNode.querySelector(".cbc-caffeine-ads-container"),
                i = "afterend";
            if (!a) {
                a = v.caffeineNode;
                i = "afterbegin"
            }
            a.insertAdjacentHTML(i, t);
            A = v.caffeineNode.querySelector(".ui");
            r = v.caffeineNode.querySelector(".cbc-caffeine-player")
        }();
        ! function() {
            p = r.querySelector(".playbackelement");
            n = r.querySelector("img.poster");
            s.metadataTitle = r.querySelector(".title");
            s.metadataShowname = r.querySelector(".showname");
            s.currentTime = A.querySelector(".currenttime");
            s.bufferBar = A.querySelector("progress.buffer");
            s.playbackBar = A.querySelector("progress.playback");
            s.duration = A.querySelector(".duration");
            o.placeholderPlay = r.querySelector(".play");
            o.btnPlay = A.querySelector(".controls .play");
            o.btnStop = A.querySelector(".controls .stop");
            o.btnPause = A.querySelector(".controls .pause");
            o.btnNext = A.querySelector(".controls .next");
            o.btnPrev = A.querySelector(".controls .prev");
            o.btnSkipForward = A.querySelector(".skip-forward");
            o.btnSkipBack = A.querySelector(".skip-back");
            if (f.getDuration() < 1 / 0) {
                o.sliderSeek = A.querySelector(".seek");
                o.sliderSeekContainer = A.querySelector(".seekprogress")
            }
            if (g.session.capabilities.canChangeVolume) {
                o.volumeControls = A.querySelector(".volume-controls");
                o.btnMute = A.querySelector(".unmuted");
                o.sliderVolumeContainer = A.querySelector(".volume-slider-container");
                o.sliderVolume = A.querySelector(".volume-slider")
            }
            o.fullscreen = A.querySelector(".fullscreen");
            g.session.capabilities.canFullscreen && o.fullscreen.classList.remove("unavailable")
        }();
        _(!0)
    }();

    function _(e) {
        var t = e ? "addEventListener" : "removeEventListener";
        if (p) {
            p[t]("click", ee);
            p[t]("touchend", te)
        }
        n && n[t]("load", b);
        o.placeholderPlay[t]("click", ne);
        o.btnPlay[t]("click", ne);
        o.btnPause[t]("click", ae);
        o.btnStop[t]("click", ie);
        o.btnSkipForward[t]("click", se);
        o.btnSkipBack[t]("click", le);
        o.btnPrev[t]("click", re);
        o.btnNext[t]("click", oe);
        if (o.sliderSeek) {
            o.sliderSeek[t]("input", ce);
            o.sliderSeek[t]("change", ue);
            o.sliderSeekContainer[t]("touchstart", Ce);
            o.sliderSeekContainer[t]("touchend", ve)
        }
        if (g.session.capabilities.canChangeVolume) {
            o.btnMute[t]("click", de);
            o.sliderVolumeContainer[t]("touchstart", Ce);
            o.sliderVolumeContainer[t]("touchend", ve);
            o.sliderVolume[t]("input", fe);
            o.sliderVolume[t]("change", fe);
            o.volumeControls[t]("keyup", Ee);
            o.volumeControls[t]("mouseleave", pe);
            o.volumeControls[t]("mouseenter", ge);
            o.volumeControls[t]("blur", Ae)
        }
        o.fullscreen[t]("click", me);
        window[t]("resize", b);
        v.caffeineNode[t]("keyup", he);
        r[t]("contextmenu", I);
        v.caffeineNode[t]("mouseleave", N);
        v.caffeineNode[t]("mousemove", w);
        v.caffeineNode[t]("touchend", ye);
        g[t = e ? "on" : "off"](E.PLAYER_STATUSCHANGE, H);
        g[t](E.SESSION_QUEUEUPDATED, W);
        g[t](E.PLAYER_METADATALOADED, x);
        g[t](E.PLAYER_READY, F);
        g[t](E.PLAYER_ENDED, G);
        g[t](E.PLAYER_VOLUMECHANGED, z);
        g[t](E.PLAYER_TIMEUPDATED, K);
        g[t](E.PLAYER_BUFFERUPDATED, j);
        g[t](E.UI_INDIRECTUSERACTION, Z);
        g[t](E.PLAYER_DOCKED, J);
        g[t](E.PLAYER_UNDOCKED, X);
        g[t](E.CHAPTER_START, Q)
    }

    function S() {
        if (g.hasNextTrack()) {
            o.btnNext.disabled = !1;
            o.btnNext.style.removeProperty("display")
        } else {
            o.btnNext.disabled = !0;
            o.btnNext.style.setProperty("display", "none")
        }
        b()
    }

    function R(e) {
        o.sliderSeek && (o.sliderSeek.disabled = !e);
        o.btnSkipForward.disabled = !e;
        o.btnSkipBack.disabled = !e
    }

    function D() {
        var e = v.caffeineNode,
            t = e.offsetWidth,
            n = e.offsetHeight;
        if (0 === t) {
            window.requestAnimationFrame(D);
            return {
                width: 0,
                height: 0
            }
        }
        var a = function() {
            var e = v.caffeineNode,
                t = p.querySelector("video"),
                n = e.querySelector("img.poster");
            if (g.fullScreen) P = screen.height / screen.width;
            else if (t && t.videoHeight && t.videoWidth && g.player.status !== C.READY) P = t.videoHeight / t.videoWidth;
            else if (n.naturalWidth && n.naturalHeight) {
                if (g.player.status === C.PLAYING) return P;
                P = n.naturalHeight / n.naturalWidth
            }
            return P = .25 * Math.round(100 * P / .25) / 100
        }();
        e.style.removeProperty("width");
        var i = function(e) {
                if (g.fullScreen) return {
                    width: screen.width,
                    height: screen.height
                };
                if ("number" != typeof v.playerWidth || y) return {
                    width: v.caffeineNode.offsetWidth,
                    height: v.caffeineNode.offsetWidth * e
                };
                var t = v.playerWidth;
                return {
                    width: t,
                    height: t * e
                }
            }(a),
            r = i.width,
            o = i.width,
            s = i.height,
            l = i.height,
            c = g.fullScreen ? screen.height : "number" == typeof v.maxHeight ? v.maxHeight : m.Helpers.Utils.getIsInIframe() ? window.innerHeight : "number" == typeof v.playerWidth ? 1 / 0 : .75 * window.innerHeight;
        c < s && (l = c);
        o = l / a;
        var u = g.fullScreen ? screen.width : m.Helpers.Utils.getIsInIframe() ? window.innerWidth : "number" != typeof v.playerWidth || y ? v.caffeineNode.offsetWidth : v.playerWidth;
        u < r && (o = u);
        l = o * a;
        o = Math.ceil(o);
        l = Math.ceil(l);
        if (f.isVideo() || v.forceVideo) {
            e.style.setProperty("width", o + "px");
            e.style.setProperty("height", l + "px")
        }
        var d = {
            width: o,
            height: l
        };
        o === t && l === n || g.trigger(E.PLAYER_RESIZED, d);
        return d
    }

    function T() {
        if (g.session.capabilities.canChangeVolume) {
            o.sliderVolume.value = 10 * g.player.volume;
            o.sliderVolume.setAttribute("aria-valuenow", 10 * g.player.volume);
            for (var e = o.btnMute.querySelectorAll(".volume-level"), t = Math.ceil(o.sliderVolume.value / e.length), n = 0; n < e.length; n++) n < t ? e[n].setAttribute("class", "foreground volume-level volume-level-" + (n + 1)) : e[n].setAttribute("class", "background volume-level volume-level-" + (n + 1))
        }
    }

    function L() {
        var e = g.player.duration;
        (isNaN(e) || 0 === e) && (e = f.getDuration());
        s.duration.innerHTML = f.isLive() ? f.getFormattedDuration() : i.getFormattedDuration(e);
        var t = Math.round(100 * e) / 100;
        o.sliderSeek && (o.sliderSeek.max = 100 * t);
        s.playbackBar && (s.playbackBar.max = 100 * t);
        s.bufferBar && (s.bufferBar.max = Math.floor(e))
    }

    function b() {
        D();
        var e = A.offsetWidth;
        if (0 !== e) {
            e < 350 ? v.caffeineNode.classList.add("caffeine-small") : v.caffeineNode.classList.remove("caffeine-small");
            for (var t, n = A.querySelectorAll("[data-display-priority]"), a = 0; a < n.length; a++)
                if ("" === (t = n[a]).style.getPropertyValue("display")) {
                    t.setAttribute("is-temporarily-visible", "");
                    t.style.setProperty("display", "block");
                    t.style.setProperty("transition", "none")
                }
            var i, r = {};
            for (a = 0; a < n.length; a++) {
                t = n[a];
                i = parseInt(t.getAttribute("data-display-priority"));
                var o = window.getComputedStyle(t),
                    s = t.offsetWidth + parseInt(o.getPropertyValue("margin-left").replace(/[^-\d\.]/g, "")) + parseInt(o.getPropertyValue("margin-right").replace(/[^-\d\.]/g, "")) + parseInt(o.getPropertyValue("border-left-width").replace(/[^-\d\.]/g, "")) + parseInt(o.getPropertyValue("border-right-width").replace(/[^-\d\.]/g, ""));
                void 0 === r[i] && (r[i] = 0);
                r[i] += s
            }
            var l = Object.keys(r),
                c = [],
                u = 0;
            for (a = 0; a < l.length; a++) {
                var d = r[i = l[a]];
                if (e < u + d) break;
                c.push(i);
                u += d
            }
            for (a = 0; a < n.length; a++) {
                t = n[a];
                i = parseInt(t.getAttribute("data-display-priority"));
                var f = -1 < c.indexOf(String(i));
                t.setAttribute("fits-on-screen", f);
                if (t.hasAttribute("is-temporarily-visible")) {
                    t.style.removeProperty("display");
                    t.style.removeProperty("transition");
                    t.removeAttribute("is-temporarily-visible")
                }
            }
        } else window.requestAnimationFrame(b)
    }

    function I(e) {
        if (!v.caffeineNode.contextMenu) {
            l.classList.contains("active") && M(e);
            h = document.activeElement;
            l.classList.add("active");
            var t = e.pageX - window.scrollX,
                n = e.pageY - window.scrollY,
                a = t + l.clientWidth,
                i = n + l.clientHeight,
                r = a < window.innerWidth ? t : t - l.clientWidth,
                o = i < window.innerHeight ? n : n - l.clientHeight;
            l.style.left = r + "px";
            l.style.top = o + "px";
            document.addEventListener("click", M);
            l.querySelector("menuitem button").focus();
            e.preventDefault()
        }
    }

    function M(e) {
        if (l && l.classList.contains("active")) {
            l.classList.remove("active");
            l.style.removeProperty("left");
            l.style.removeProperty("right");
            l.style.removeProperty("top");
            l.style.removeProperty("bottom");
            document.removeEventListener("click", M);
            h.focus();
            h = null;
            e && e.preventDefault && e.preventDefault()
        }
    }

    function w() {
        if (!f.isAudio()) {
            A.classList.remove("hidden");
            k();
            v.container.querySelector("video").style.cursor = ""
        }
    }

    function N() {
        if (!f.isAudio()) {
            e = window.clearTimeout(e);
            if (-1 < [C.PLAYING, C.ADVERTISING].indexOf(g.player.status)) {
                A.classList.add("hidden");
                v.container.querySelector("video").style.cursor = "none"
            }
        }
    }

    function k() {
        e = window.clearTimeout(e);
        e = window.setTimeout(N, 4e3)
    }

    function U() {
        e = window.clearTimeout(e)
    }

    function q() {
        window.clearTimeout(t)
    }

    function O() {
        void 0 !== o.volumeControls && o.volumeControls.classList.remove("active");
        q()
    }

    function B(e) {
        o.btnPlay.querySelector("span").innerHTML = e;
        o.btnPlay.querySelector("label").innerHTML = e;
        o.btnPlay.setAttribute("aria-label", e);
        o.btnPause.setAttribute("aria-label", e);
        o.btnStop.setAttribute("aria-label", e)
    }

    function Y() {
        if (c.activeOverlay) {
            c.activeOverlay.classList.remove("active");
            c.activeOverlay = null;
            d && d.focus()
        }
    }

    function H(e) {
        switch (g.player.status) {
            case C.READY:
                B(f.isAudio() ? "Listen" : "Watch");
                L();
                O();
                R(!0);
                w();
                break;
            case C.LOADING:
                B("Loading");
                break;
            case C.PLAYING:
                B("Pause playback");
                R(!0);
                b();
                k();
                break;
            case C.PAUSED:
                B("Resume playback");
                w();
                break;
            case C.ADVERTISING:
                B("Pause advertisement");
                R(!1);
                N();
                break;
            case C.ADVERTISING_PAUSED:
                B("Resume advertisement")
        }
    }

    function V(e) {
        switch (e.from) {
            case C.PLAYING:
                setTimeout(function() {
                    o.btnPlay.focus()
                }, 1);
                break;
            case C.PAUSED:
                setTimeout(function() {
                    o.btnPause.focus()
                }, 1);
                break;
            case C.READY:
                setTimeout(function() {
                    o.btnStop.focus()
                }, 1)
        }
        g.off(E.PLAYER_STATUSCHANGE, V)
    }

    function F() {
        S();
        T();
        v.caffeineNode.style.removeProperty("height");
        b();
        g.initModule("Players.Components.Share", ".share");
        s.currentTime.innerHTML = i.getFormattedDuration(g.player.currentTime)
    }

    function x() {
        L()
    }

    function z(e) {
        T()
    }

    function G() {
        K(0);
        b()
    }

    function K(e) {
        if (f.getDuration() < 1 / 0) {
            var t = Math.round(100 * e) / 100;
            a.defer(function() {
                o.sliderSeek.value = 100 * t;
                s.playbackBar.value = 100 * t
            });
            o.sliderSeek.setAttribute("aria-valuenow", e + " seconds")
        }
        s.currentTime.innerHTML = i.getFormattedDuration(e)
    }

    function j(e) {
        if (s.bufferBar && isFinite(e)) {
            L();
            s.bufferBar.value = e
        }
    }

    function W() {
        S()
    }

    function Q() {
        s.metadataTitle.innerHTML = "Friday Afternoon Call-in Request Show"; //f.getTitle();
        s.metadataShowname.innerHTML = f.getShowName()
    }

    function Z(e) {
        var t = v.caffeineNode.querySelector(".indicate-" + e);
        t.addEventListener("transitionend", $, !1);
        t.classList.add("show")
    }

    function J() {
        y = !0;
        b()
    }

    function X(e) {
        y = !1;
        w();
        b()
    }

    function $(e) {
        e.target.removeEventListener("transitionend", $);
        e.target.classList.remove("show")
    }

    function ee(e) {
        switch (g.player.status) {
            case C.PLAYING:
                if (f.isLive()) {
                    g.player.stop();
                    g.trigger(E.UI_INDIRECTUSERACTION, "stop")
                } else {
                    g.player.pause();
                    g.trigger(E.UI_INDIRECTUSERACTION, "pause")
                }
                break;
            case C.PAUSED:
            case C.READY:
                g.player.play();
                g.trigger(E.UI_INDIRECTUSERACTION, "play")
        }
    }

    function te(e) {
        e.preventDefault()
    }

    function ne(e) {
        Y();
        g.on(E.PLAYER_STATUSCHANGE, V);
        g.player.play()
    }

    function ae(e) {
        g.on(E.PLAYER_STATUSCHANGE, V);
        g.player.pause()
    }

    function ie(e) {
        g.on(E.PLAYER_STATUSCHANGE, V);
        g.player.stop()
    }

    function re(e) {
        g.player.currentTime < 3 || f.isLive() ? g.playPrevTrack() : g.player.currentTime = 0
    }

    function oe(e) {
        g.playNextTrack()
    }

    function se(e) {
        g.player.currentTime = g.player.currentTime + 30
    }

    function le(e) {
        g.player.currentTime = g.player.currentTime - 15
    }

    function ce(e) {
        document.activeElement != o.sliderSeek && o.sliderSeek.focus();
        s.currentTime.innerHTML = i.getFormattedDuration(o.sliderSeek.value / 100);
        g.off(E.PLAYER_TIMEUPDATED, K)
    }

    function ue(e) {
        g.player.currentTime = o.sliderSeek.value / 100;
        s.playbackBar.value = o.sliderSeek.value;
        g.on(E.PLAYER_TIMEUPDATED, K)
    }

    function de(e) {
        o.volumeControls.classList.toggle("active")
    }

    function fe(e) {
        g.player.volume = o.sliderVolume.value / 10
    }

    function Ae(e) {
        O()
    }

    function pe(e) {
        t = window.setTimeout(O, 1500)
    }

    function ge(e) {
        q()
    }

    function me(e) {
        g.fullScreen = !g.fullScreen
    }

    function Ee(e) {
        if ("Escape" === e.key || 27 === e.which) {
            O();
            o.btnMute.focus()
        }
    }

    function Ce(e) {
        U();
        e.currentTarget.classList.add("touched")
    }

    function ve(e) {
        k();
        e.currentTarget.classList.remove("touched")
    }

    function he(e) {
        w()
    }

    function ye(e) {
        if (r.querySelector(".ui").classList.contains("hidden")) {
            e.preventDefault();
            w()
        }
    }
    return {
        destroy: function() {
            U();
            q();
            for (var e = 0; e < u.length; e++) l.removeChild(u[e]);
            _(!1);
            M();
            g.player.share && g.player.share.destroy();
            r.parentNode && r.parentNode.removeChild(r);
            A.parentNode && A.parentNode.removeChild(A)
        },
        addOverlay: function(e, t, n) {
            var a = m.Templates["Overlay.html"]({
                className: e,
                overlayContent: t,
                withControls: n
            });
            r.insertAdjacentHTML("beforeend", a);
            c[e] = r.querySelector("." + e);
            return c[e]
        },
        openOverlay: function(e) {
            var t = document.activeElement;
            c.activeOverlay ? Y() : d = t;
            c[e].classList.add("active");
            c[e].focus();
            c.activeOverlay = c[e]
        },
        closeOverlay: Y,
        addContextMenuItem: function(e) {
            var t = document.createElement("menuitem"),
                n = document.createElement("button");
            n.innerText = e;
            t.innerText = e;
            t.appendChild(n);
            l.insertBefore(t, l.querySelector("hr"));
            u.push(t);
            return t
        },
        closeContextMenu: M
    }
});
CBC.APP.Caffeine.addModule("Players.Main", function(e, t) {
    "use strict";
    var n, a, i, r, o, s, l = this,
        c = CBC.APP.Caffeine._,
        u = l.session.getIdAtIndex(e),
        d = l.session.modelsManager.getClip(u);
    a = l.initModule("Players.Components.UI", d);
    n = l.initModule("Players.Barista.PlaybackController", t, d);
    i = l.initModule("Core.Components.Keyboard", d);
    r = l.initModule("Core.Components.LocalStorage");
    c.defer(function() {
        l.initModule("Players.Components.PlaybackStats");
        s = l.initModule("Players.Components.Recommendations")
    });
    var f = CBC.APP.Caffeine.Helpers.Utils.getIsInIframe();
    d.isVideo() && !f && (o = l.initModule("Players.Components.DockManager", d));
    l.player = {
        play: n.play,
        pause: n.pause,
        stop: n.stop,
        unpause: n.unpause,
        get currentClip() {
            return u
        },
        get metadata() {
            return l.initModule("Core.Components.ClipMetadata", e)
        },
        get status() {
            return n.status
        },
        get currentTime() {
            return n.currentTime
        },
        set currentTime(e) {
            n.currentTime = e
        },
        get duration() {
            return n.duration
        },
        get muted() {
            return n.muted
        },
        set muted(e) {
            n.muted = e
        },
        get volume() {
            return n.volume
        },
        set volume(e) {
            n.volume = e
        },
        get captions() {
            return n.captions.captions
        },
        set captions(e) {
            n.captions.captions = e
        },
        get hasCaptions() {
            return n.captions.hasCaptions
        },
        get ui() {
            return a
        },
        get currentChapterIndex() {
            return n.currentChapterIndex
        },
        destroy: function() {
            l.player.playbackStats && l.player.playbackStats.destroy();
            r && r.destroy();
            a && a.destroy();
            n && n.destroy();
            i && i.destroy();
            o && o.destroy();
            s && s.destroy();
            delete l.player
        }
    }
});
this.CBC = this.CBC || {};
this.CBC.APP = this.CBC.APP || {};
this.CBC.APP.Caffeine = this.CBC.APP.Caffeine || {};
this.CBC.APP.Caffeine.Templates = this.CBC.APP.Caffeine.Templates || {};
this.CBC.APP.Caffeine.Templates["Caffeine.html"] = function(obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = CBC.APP.Caffeine._.escape;
    with(obj) __p += '<div class="cbc-caffeine-container" id="Caffeine' + (null == (__t = ciid) ? "" : __t) + '" tabindex="0" contextmenu="CaffeineContextMenu' + (null == (__t = ciid) ? "" : __t) + '">\n\n    <div class="cbc-caffeine-error-messages"></div>\n\n    <menu type="context" id="CaffeineContextMenu' + (null == (__t = ciid) ? "" : __t) + '">\n        <hr />\n        <menuitem class="version">CBC Caffeine Player 14.6.2</menuitem>\n    </menu>\n</div>';
    return __p
};
this.CBC.APP.Caffeine.Templates["Ads.html"] = function(obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = CBC.APP.Caffeine._.escape;
    with(obj) __p += '<div class="cbc-caffeine-ads-container advertisement">\n    <div class="cbc-caffeine-ads-innercontainer">\n        <div class="ads-video-player"></div>\n        <div class="ads-message">\n            Ad\n            <span class="ads-count"></span>:\n            <span class="ads-time-remaining"></span>\n            <span class="message-text">Playback will start after this advertisement</span>\n        </div>\n        <button class="ads-play-button">Your content will play after this advertisement, click here to continue.</button>\n        <button class="ads-close">\n            <svg enable-background="new 0 0 24 24" id="Layer_1" version="1.0" viewBox="0 0 24 24" focusable="false">\n                <g><path fill="#FFFFFF" d="M12,4c4.4,0,8,3.6,8,8s-3.6,8-8,8s-8-3.6-8-8S7.6,4,12,4 M12,2C6.5,2,2,6.5,2,12c0,5.5,4.5,10,10,10s10-4.5,10-10   C22,6.5,17.5,2,12,2L12,2z"/></g>\n                <line fill="none" stroke="#FFFFFF" stroke-miterlimit="10" stroke-width="2" x1="18.2" x2="5.8" y1="18.2" y2="5.8"/>\n            </svg>\n            Cancel playback\n        </button>\n    </div>\n</div>';
    return __p
};
this.CBC.APP.Caffeine.Templates["DockManager.html"] = function(obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = CBC.APP.Caffeine._.escape;
    with(obj) __p += '<div class="cbc-caffeine-dock-placeholder">\n\n    <img\n        class="poster"\n        src="' + (null == (__t = thumbnail) ? "" : __t) + '?downsize=320px:*"\n        alt=""\n    />\n\n    <div class="dock-placeholder-ui">\n\n        <button class="play" type="button">\n            <svg class="icon-play" height="100%" width="100%" viewBox="0 0 80 80" focusable="false">\n                <path class="background" d="m0 0v80h80v-80h-80zm30 20 16 10 16 10-16 10-16 10v-20-20z"/>\n                <path class="foreground" d="m30.22 20.25c-0.2231 4.435-0.09351 8.89-0.1419 13.33 0.0088 8.737-0.0362 17.48 0.07256 26.21 10.54-6.515 21.05-13.07 31.52-19.69-2.395-1.814-5.047-3.302-7.568-4.948-7.952-4.981-15.92-9.972-23.89-14.91z"/>\n            </svg>\n            <span>Watch</span>\n        </button>\n\n        <button class="stop" type="button">\n            <svg class="icon-stop" height="100%" width="100%" viewBox="0 0 80 80" focusable="false">\n                <path class="background" d="m0 0v80h80v-80h-80zm22 20h36v40h-36v-40z"/>\n                <rect class="foreground" fill-rule="evenodd" height="40" width="36" y="20" x="22"/>\n            </svg>\n            <span>Stop</span>\n        </button>\n\n        <button class="pause" type="button">\n            <svg class="icon-pause" height="100%" width="100%" viewBox="0 0 80 80" focusable="false">\n                <path class="background" d="m0 0v80h80v-80h-80zm22 20h16v40h-16v-40zm20 0h16v40h-16v-40z"/>\n                <path class="foreground" d="m22.07 20.09v19.94 19.94h15.88v-39.88h-15.88zm20 0v19.94 19.94h15.88v-39.88h-15.88z"/>\n            </svg>\n            <span>Pause</span>\n        </button>\n\n        <button class="undock" type="button" aria-label="Undock">\n            <svg class="icon-undock" height="100%" width="100%" viewBox="0 0 32 32" focusable="false">\n                <path class="background" d="M26.8,9.2H5.2v13.4h21.6V9.2z M24.7,20.7H7.3v-9.7h17.4V20.7z"/>\n                <polygon class="background" points="9,12.5 9,18.2 14.7,12.5    "/>\n                <path class="background" d="M0,0v32h32V0H0z M28,23.8H4V8h24V23.8z"/>\n                <path class="foreground" d="M4,8v15.8h24V8H4z M26.8,22.6H5.2V9.2h21.6V22.6z"/>\n                <path class="foreground" d="M7.3,11.1v9.7h17.4v-9.7H7.3z M9,18.2v-5.7h5.7L9,18.2z"/>\n            </svg>\n            <span>Undock</span>\n        </button>\n    </div>\n\n</div>';
    return __p
};
this.CBC.APP.Caffeine.Templates["ErrorManager.html"] = function(obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = CBC.APP.Caffeine._.escape,
        __j = Array.prototype.join;

    function print() {
        __p += __j.call(arguments, "")
    }
    with(obj) {
        __p += '<div class="cbc-caffeine-error">\n    <strong>Oops...</strong>\n    <p>' + (null == (__t = error.message) ? "" : __t) + "</p>\n    <small>Error " + (null == (__t = error.code) ? "" : __t);
        raygunUUID && (__p += " - ID: " + (null == (__t = raygunUUID) ? "" : __t));
        __p += "</small>\n    ";
        retry && (__p += '\n        <button class="error-retry">Retry</button>\n    ');
        __p += "\n</div>\n"
    }
    return __p
};
this.CBC.APP.Caffeine.Templates["EventLoggerOutput.html"] = function(obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = CBC.APP.Caffeine._.escape;
    with(obj) __p += (null == (__t = name) ? "" : __t) + "\n    data: " + (null == (__t = data) ? "" : __t) + "\n    seconds since " + (null == (__t = first) ? "" : __t) + ": " + (null == (__t = seconds) ? "" : __t) + "\n\n";
    return __p
};
this.CBC.APP.Caffeine.Templates["Overlay.html"] = function(obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = CBC.APP.Caffeine._.escape;
    with(obj) __p += '<div class="caffeine-overlay ' + (null == (__t = className) ? "" : __t) + '" data-withcontrols="' + (null == (__t = !0 === withControls) ? "" : __t) + '" tabindex="0">\n    <div class="caffeine-overlay-content">\n        ' + (null == (__t = overlayContent) ? "" : __t) + "\n    </div>\n</div>";
    return __p
};
this.CBC.APP.Caffeine.Templates["PlaybackStats.html"] = function(obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = CBC.APP.Caffeine._.escape;
    with(obj) __p += '<code>\n    Playback Status: <span class="status">' + (null == (__t = status) ? "" : __t) + '</span>\n    <br />\n    Assigned test group: <span class="testgroup">None</span>\n    <br />\n    Identifier: ' + (null == (__t = dataModel.getIdType()) ? "" : __t) + " " + (null == (__t = dataModel.getId()) ? "" : __t) + '\n    <br />\n    Asset: <span class="assetLoader">Undetermined</span>\n    <br />\n    Bitrate: <span class="bitrate">Undetermined</span>\n    <br />\n    Streaming URL: <input class="mediaUrl" value="Undetermined" />\n    <br />\n    Events Log: <button name="getEvents">Show/Refresh Events</button>\n    <br />\n    <textarea class="eventsLog"></textarea>\n</code>\n';
    return __p
};
this.CBC.APP.Caffeine.Templates["PlaylistItem.html"] = function(obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = CBC.APP.Caffeine._.escape,
        __j = Array.prototype.join;

    function print() {
        __p += __j.call(arguments, "")
    }
    with(obj) {
        __p += '<li class="clips">\n    <button class="item" data-id="';
        dataModel.getId();
        __p += '" data-index="' + (null == (__t = index) ? "" : __t) + '" aria-label="' + (null == (__t = dataModel.getTitle()) ? "" : __t) + '">\n        <div class="media-control">\n            <span class="play-icon" aria-label="Play"></span>\n            <span class="runtime">' + (null == (__t = dataModel.getFormattedDuration()) ? "" : __t) + '</span>\n        </div>\n        <div class="media-info">\n            ';
        thumbnail && (__p += '\n            <div class="thumbnail-image">\n                <img src="' + (null == (__t = thumbnail) ? "" : __t) + '" alt="" />\n            </div>\n            ');
        __p += '\n            <span class="title">' + (null == (__t = dataModel.getTitle()) ? "" : __t) + '</span>\n            <span class="description">' + (null == (__t = dataModel.getDescription()) ? "" : __t) + '</span>\n            <span class="showname">' + (null == (__t = dataModel.getShowName()) ? "" : __t) + '</span>\n            <span class="airdate">' + (null == (__t = dataModel.getFormattedAirDate()) ? "" : __t) + "</span>\n        </div>\n    </button>\n    ";
        editable && (__p += '\n        <button class="delete" aria-label="Delete item from playlist">x</button>\n    ');
        __p += "\n</li>\n"
    }
    return __p
};
this.CBC.APP.Caffeine.Templates["Recommendations.html"] = function(obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = CBC.APP.Caffeine._.escape;
    with(obj) __p += '<div class="recommendations four-up">\n    <h2>Recommended for you</h2>\n    <ul class="recommended-items">\n    </ul>\n</div>';
    return __p
};
this.CBC.APP.Caffeine.Templates["RecommendedItem.html"] = function(obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = CBC.APP.Caffeine._.escape,
        __j = Array.prototype.join;

    function print() {
        __p += __j.call(arguments, "")
    }
    with(obj) {
        __p += '<li class="recommended-item">\n\n    <img src="' + (null == (__t = dataModel.getThumbnail()) ? "" : __t) + '" alt="" />\n\n    <a class="recommended-item-link" href="' + (null == (__t = dataModel.getPageUrl()) ? "" : __t) + '" data-id="' + (null == (__t = dataModel.getId()) ? "" : __t) + '" aria-label="Play this video: ' + (null == (__t = dataModel.getTitle()) ? "" : __t) + '">\n\n        <div class="recommended-item-rollover">\n            <div class="recommended-item-info" >\n                <div class="recommended-item-title" >\n                    ' + (null == (__t = dataModel.getTitle()) ? "" : __t) + '\n                </div>\n                <div class="recommended-item-showname" >\n                    ' + (null == (__t = dataModel.getShowName().toUpperCase()) ? "" : __t) + '\n                </div>\n            </div>\n        </div>\n\n        <div class="recommended-item-duration" >\n            ' + (null == (__t = dataModel.getFormattedDuration()) ? "" : __t) + "\n            ";
        !1 !== dataModel.getCaptions() && (__p += '\n            <span class="recommended-item-cc" aria-label="Closed-captioned">CC</span>\n            ');
        __p += "\n        </div>\n\n    </a>\n\n</li>"
    }
    return __p
};
this.CBC.APP.Caffeine.Templates["Share.html"] = function(obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = CBC.APP.Caffeine._.escape,
        __j = Array.prototype.join;

    function print() {
        __p += __j.call(arguments, "")
    }
    with(obj) {
        __p += '<button class="close-button">Close</button>\n<strong>Share ' + (null == (__t = dataModel.isAudio() ? "Audio" : "Video") ? "" : __t) + "</strong>\n\n";
        "inline" !== dataModel.getIdType() && (__p += '\n<div class="share-category">\n    <button class="button share-icon share-icon-embed">Embed</button>\n    <label>\n        Embed Code:\n        <input\n            name="embedcode"\n            class="embedcode"\n            value=\'<iframe src="//www.cbc.ca/i/caffeine/syndicate/?' + (null == (__t = dataModel.getIdType()) ? "" : __t) + "=" + (null == (__t = dataModel.getId()) ? "" : __t) + '" width="' + (null == (__t = config.caffeineNode.offsetWidth) ? "" : __t) + '" height="' + (null == (__t = config.caffeineNode.offsetHeight) ? "" : __t) + '" frameborder="0" allowfullscreen></iframe>\'\n        />\n    </label>\n</div>\n');
        __p += '\n\n<div class="share-category">\n    <button class="button share-icon share-icon-link">Link</button>\n    <label>\n        ' + (null == (__t = dataModel.isAudio() ? "Audio" : "Video") ? "" : __t) + ' Link:\n        <input name="link" class="videolink" value="' + (null == (__t = shareUrl) ? "" : __t) + '" />\n    </label>\n</div>\n\n<div class="share-category">\n    <button\n        class="button share-icon share-icon-facebook"\n        data-href="https://www.facebook.com/sharer/sharer.php?u=' + (null == (__t = shareUrl) ? "" : __t) + '"\n    >\n        Facebook\n    </button>\n</div>\n\n<div class="share-category">\n    <button\n        class="button share-icon share-icon-twitter"\n        data-href="https://www.twitter.com/intent/tweet?url=' + (null == (__t = shareUrl.replace(/%/g, "%25")) ? "" : __t) + '"\n    >\n        Twitter\n    </button>\n</div>\n\n<div class="share-category">\n    <button\n        class="button share-icon share-icon-email"\n        data-href="mailto:?body=' + (null == (__t = shareUrl) ? "" : __t) + '"\n    >\n        Email\n    </button>\n</div>\n'
    }
    return __p
};
this.CBC.APP.Caffeine.Templates["Thumbnail.html"] = function(obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = CBC.APP.Caffeine._.escape;
    with(obj) __p += '<div id="thumbnail' + (null == (__t = ciid) ? "" : __t) + '" class="thumbnail-container">\n    <img class="thumbnail" src="' + (null == (__t = thumbnail) ? "" : __t) + '" alt="Media placeholder" />\n    <div id="icons' + (null == (__t = ciid) ? "" : __t) + '" class="icons">\n        <button class="play-button">Play Media</button>\n    </div>\n</div>';
    return __p
};
this.CBC.APP.Caffeine.Templates["UI_DockManager.html"] = function(obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = CBC.APP.Caffeine._.escape;
    with(obj) __p += '<button class="dock" type="button" data-display-priority="4" aria-label="Dock player to corner of screen">\n    <svg class="icon-dock" height="100%" width="100%" viewBox="0 0 32 32" focusable="false">\n        <path class="background" d="M26.8,9.2H5.2v13.4h21.6V9.2z M24.8,20.6H16v-4.9h8.8V20.6z"/>\n        <path class="background" d="M0,0v32h32V0H0z M28,23.8H4V8h24V23.8z"/>\n        <rect class="foreground" x="16" y="15.7" class="st0" width="8.8" height="4.9"/>\n        <path class="foreground" d="M4,8v15.8h24V8H4z M26.8,22.6H5.2V9.2h21.6V22.6z"/>\n    </svg>\n    <svg class="icon-undock" height="100%" width="100%" viewBox="0 0 32 32" focusable="false">\n        <path class="background" d="M26.8,9.2H5.2v13.4h21.6V9.2z M24.7,20.7H7.3v-9.7h17.4V20.7z"/>\n        <polygon class="background" points="9,12.5 9,18.2 14.7,12.5    "/>\n        <path class="background" d="M0,0v32h32V0H0z M28,23.8H4V8h24V23.8z"/>\n        <path class="foreground" d="M4,8v15.8h24V8H4z M26.8,22.6H5.2V9.2h21.6V22.6z"/>\n        <path class="foreground" d="M7.3,11.1v9.7h17.4v-9.7H7.3z M9,18.2v-5.7h5.7L9,18.2z"/>\n    </svg>\n    <span>Dock player to corner of screen</span>\n</button>';
    return __p
};
this.CBC.APP.Caffeine.Templates["UI.html"] = function(obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = CBC.APP.Caffeine._.escape,
        __j = Array.prototype.join;

    function print() {
        __p += __j.call(arguments, "")
    }
    with(obj) {
        __p += '<div class="cbc-caffeine-player" id="CaffeinePlayer' + (null == (__t = config.ciid) ? "" : __t) + '">\n    <div class="placeholder">\n        <button class="play" type="button" aria-label="Play">\n            <svg height="100%" width="100%" viewBox="0 0 80 80" focusable="false">\n                <path class="background" d="m0 0v80h80v-80h-80zm30 20 16 10 16 10-16 10-16 10v-20-20z"/>\n                <path class="foreground" d="m30.22 20.25c-0.2231 4.435-0.09351 8.89-0.1419 13.33 0.0088 8.737-0.0362 17.48 0.07256 26.21 10.54-6.515 21.05-13.07 31.52-19.69-2.395-1.814-5.047-3.302-7.568-4.948-7.952-4.981-15.92-9.972-23.89-14.91z"/>\n            </svg>\n        </button>\n        <img\n            class="poster"\n            src="' + (null == (__t = thumbnail) ? "" : __t) + '?downsize=1280px:*"\n            alt=""\n        />\n    </div>\n\n    <div class="playbackelement"></div>\n\n    <svg class="indicate-play" height="80" width="80" viewBox="0 0 80 80" focusable="false" aria-hidden="true">\n        <path class="background" d="m0 0v80h80v-80h-80zm30 20 16 10 16 10-16 10-16 10v-20-20z"/>\n        <path class="foreground" d="m30.22 20.25c-0.2231 4.435-0.09351 8.89-0.1419 13.33 0.0088 8.737-0.0362 17.48 0.07256 26.21 10.54-6.515 21.05-13.07 31.52-19.69-2.395-1.814-5.047-3.302-7.568-4.948-7.952-4.981-15.92-9.972-23.89-14.91z"/>\n    </svg>\n\n\n    <svg class="indicate-pause" height="80" width="80" viewBox="0 0 80 80" focusable="false" aria-hidden="true">\n        <path class="background" d="m0 0v80h80v-80h-80zm22 20h16v40h-16v-40zm20 0h16v40h-16v-40z"/>\n        <path class="foreground" d="m22.07 20.09v19.94 19.94h15.88v-39.88h-15.88zm20 0v19.94 19.94h15.88v-39.88h-15.88z"/>\n    </svg>\n\n    <svg class="indicate-stop" height="80" width="80" viewBox="0 0 80 80" focusable="false" aria-hidden="true">\n        <path class="background" d="m0 0v80h80v-80h-80zm22 20h36v40h-36v-40z"/>\n        <rect class="foreground" fill-rule="evenodd" height="40" width="36" y="20" x="22"/>\n    </svg>\n\n    <svg class="indicate-volume" height="80" width="80" viewBox="0 0 80 80" focusable="false" aria-hidden="true">\n        <path class="background" d="m0 0v80h80v-80h-80zm45.48 20v20 20l-15-10-4-2.666v0.166h-11v-15h11v0.166l4-2.666 15-10zm15.288 2.371c0.043 0.003 0.087 0.027 0.134 0.078 6.069 3.702 9.967 10.649 9.895 17.764 0.07 7.043-3.951 13.832-9.959 17.418-0.583-0.626-1.088-1.497-1.387-2.324 0.825-0.768 1.94-1.36 2.74-2.223 5.369-4.628 7.189-12.713 4.59-19.258-1.2-3.419-3.574-6.318-6.519-8.392-1.189-0.329-0.764-1.15-0.248-1.887 0.225-0.166 0.449-1.195 0.754-1.176zm-7.055 3.682c5.155 2.711 8.59 7.778 8.889 13.06 0.421 5.502-2.81 11.003-7.967 14.069-0.764 0.823-1.358 0.122-1.721-0.606-0.998-1.047 1.012-1.431 1.637-2.111 2.431-1.797 4.231-4.247 5.013-6.963 1.395-4.488-0.015-9.55-3.599-12.963-0.914-0.819-1.856-1.694-3.033-2.201-0.86-0.664 0.405-1.68 0.781-2.285zm-5.381 4.068c2.74 1.735 4.802 4.576 5.361 7.793 0.99 4.592-1.303 9.54-5.277 11.965-0.581-0.66-1.036-1.588-1.416-2.424 1.092-0.887 2.245-1.855 2.918-3.152 2.101-3.538 1.101-8.362-2.047-10.949-1.056-0.494-1.046-1.09-0.273-1.969l0.734-1.264z"/>\n        <path class="foreground" d="m36.062 26.367c-3.1042 2.0659-6.2083 4.1318-9.3125 6.1978-3.7292-0.01009-7.4583-0.02018-11.188-0.03027v14.887c3.7039-0.0047 7.4077-0.0095 11.112-0.0144 6.2337 4.1563 12.467 8.3126 18.701 12.469 0.05797-13.235 0.05782-26.471 0-39.707-3.1042 2.0659-6.2083 4.1318-9.3125 6.1978z"/>\n        <path class="foreground volume-level volume-level-1" d="m48.332 30.121-0.73438 1.2637c-0.77226 0.87946-0.7829 1.4748 0.27344 1.9688 3.1475 2.5873 4.1482 7.4116 2.0469 10.949-0.67331 1.2969-1.8258 2.2651-2.918 3.1523 0.37972 0.83613 0.83496 1.7635 1.416 2.4238 3.9738-2.4251 6.2671-7.3726 5.2773-11.965-0.5594-3.217-2.6209-6.0579-5.3613-7.793z"/>\n        <path class="foreground volume-level volume-level-2" d="m53.713 26.054c-0.37554 0.60552-1.6401 1.6211-0.78082 2.2846 1.1777 0.50671 2.1189 1.3822 3.0327 2.2011 3.5837 3.413 4.9946 8.4752 3.6002 12.963-0.78264 2.7162-2.5817 5.1657-5.013 6.9624-0.62431 0.68006-2.6367 1.0649-1.6385 2.1118 0.36273 0.72797 0.95745 1.4292 1.7214 0.6058 5.157-3.067 8.388-8.568 7.967-14.07-0.299-5.283-3.734-10.348-8.889-13.059z"/>\n        <path class="foreground volume-level volume-level-3" d="m60.768 22.37c-0.30421-0.01881-0.52869 1.0101-0.75391 1.1758-0.51546 0.73746-0.94057 1.5584 0.24805 1.8867 2.9452 2.0749 5.3195 4.9734 6.5195 8.3926 2.5984 6.5447 0.77925 14.63-4.5898 19.258-0.8001 0.86325-1.915 1.4555-2.7402 2.2227 0.29904 0.82739 0.80404 1.6987 1.3867 2.3242 6.008-3.5859 10.029-10.374 9.959-17.418 0.07196-7.1151-3.8262-14.062-9.8945-17.764-0.04701-0.05102-0.09131-0.07544-0.13476-0.07813z"/>\n    </svg>\n\n    <svg class="indicate-skip-forward" width="80" height="80" viewBox="0 0 80 80" focusable="false" aria-hidden="true">\n     <rect class="background" width="80" height="80" x="0" y="0"/>\n     <path class="foreground" d="m40 17v4c-11.6 0-21 9.4-21 21s9.4 21 21 21 21-9.4 21-21c0-5.5-2.2-10.6-5.7-14.3l-5.6 4.2c2.7 2.5 4.3 6.1 4.3 10.1 0 7.7-6.3 14-14 14s-14-6.3-14-14 6.3-14 14-14v4l10-7.5zm4.764 31.894c-29.843 20.737-14.921 10.369 0 0z"/>\n    </svg>\n\n    <svg class="indicate-skip-back" width="80" height="80" viewBox="0 0 80 80" focusable="false" aria-hidden="true">\n     <rect class="background" width="80" height="80" x="0" y="0"/>\n     <path class="foreground" d="m40 17v4c11.6 0 21 9.4 21 21s-9.4 21-21 21-21-9.4-21-21c0-5.5 2.2-10.6 5.7-14.3l5.6 4.2c-2.7 2.5-4.3 6.1-4.3 10.1 0 7.7 6.3 14 14 14s14-6.3 14-14-6.3-14-14-14v4l-10-7.5zm-4.764 31.894c29.843 20.737 14.921 10.369 0 0z"/>\n    </svg>\n\n    <div class="showinfo">\n        ';
        dataModel.isLive() && dataModel.isVideo() && (__p += '\n        <div class="live-badge">\n            <div class="live-pulse-container">\n                <div class="pulse-ring"></div>\n                <div class="pulse-ring delay"></div>\n                <div class="pulse-ring delay"></div>\n                <div class="pulse-dot"></div>\n            </div>\n            <h2 class="live-badge-text">LIVE</h2>\n        </div>\n        ');
        __p += "\n        ";
        dataModel.getHostImage() && (__p += '\n        <img class="showthumbnail" src="' + (null == (__t = dataModel.getHostImage()) ? "" : __t) + '?crop=h:w;*,*&downsize=200:200" alt="Thumbnail image for ' + (null == (__t = dataModel.getShowName()) ? "" : __t) + '">\n        ');
        __p += '\n        <div class="showname">' + (null == (__t = dataModel.getShowName()) ? "" : __t) + '</div>\n        <div class="title">' + (null == (__t = dataModel.getTitle()) ? "" : __t) + '</div>\n    </div>\n\n    <div class="loading"></div>\n    <div class="ui">\n        <div class="controls">\n\n            <button class="prev" type="button" data-display-priority="2" aria-label="Jump to beginning of the track">\n                <svg class="icon-prev" height="100%" width="100%" viewBox="266 356 80 80" focusable="false">\n                    <path class="background" d="M266,356v80h80v-80H266z M284.6,383.5h3.6v10.7l21.4-10.7v8.9l17.9-8.9v25l-17.9-8.9v8.9l-21.4-10.7v10.7h-3.6V383.5z"/>\n                    <polygon class="foreground" points="288.1,383.5 288.1,394.2 309.6,383.5 309.6,392.4 327.4,383.5 327.4,408.5 309.6,399.6 309.6,408.5 288.1,397.8 288.1,408.5 284.6,408.5 284.6,383.5 "/>\n                </svg>\n                <span>Jump to beginning of the track</span>\n            </button>\n\n            <button class="skip-back" type="button" data-display-priority="3" aria-label="Skip back 15 seconds">\n                <svg class="icon-skip-forward" height="100%" width="100%" viewBox="0 0 80 80" focusable="false">\n                    <path class="background" d="m0 0v80h80v-80h-80zm40.18 15v4c11.6 0 21 9.4 21 21s-9.4 21-21 21-21-9.4-21-21c0-5.5 2.201-10.6 5.701-14.3l5.6 4.201c-2.7 2.5-4.301 6.1-4.301 10.1 0 7.7 6.3 14 14 14s14-6.3 14-14-6.3-14-14-14v4l-10-7.5 10-7.5zm0.8105 18.21h6.855v2.406h-4.787l-0.3164 2.113c0.124-0.008 0.261-0.0098 0.418-0.0098 1.108 0 2.118 0.2533 2.92 0.7363 1.275 0.724 2.014 2.039 2.014 3.605 0 2.737-2.147 4.723-5.105 4.723-1.514 0-2.725-0.4338-3.354-0.8398l-0.3242-0.209 0.7344-2.232 0.5352 0.3164c0.479 0.283 1.392 0.6152 2.389 0.6152 1.208 0 2.432-0.7486 2.432-2.182-0.022-1.851-1.677-2.24-3.059-2.24-0.621 0-1.113 0.06023-1.547 0.1152l-0.7461 0.09375 0.9414-7.012zm-5.924 0.1035h2.053v13.37h-2.615v-10.49l-2.018 1.086-0.5762-2.277 3.156-1.688z"/>\n                    <path class="foreground" d="m40.18 15-10 7.5 10 7.5v-4c7.7 0 14 6.3 14 14s-6.3 14-14 14-14-6.3-14-14c0-4 1.601-7.6 4.301-10.1l-5.6-4.201c-3.5 3.7-5.701 8.801-5.701 14.3 0 11.6 9.4 21 21 21s21-9.4 21-21-9.4-21-21-21v-4zm0.8105 18.21-0.9414 7.012 0.7461-0.09375c0.434-0.055 0.9259-0.1152 1.547-0.1152 1.382 0 3.037 0.3892 3.059 2.24-0.000001 1.433-1.224 2.182-2.432 2.182-0.997 0-1.91-0.3322-2.389-0.6152l-0.5352-0.3164-0.7344 2.232 0.3242 0.209c0.629 0.406 1.84 0.8398 3.354 0.8398 2.958 0 5.105-1.986 5.105-4.723 0-1.566-0.7387-2.881-2.014-3.605-0.802-0.483-1.812-0.7363-2.92-0.7363-0.157 0-0.294 0.0018-0.418 0.0098l0.3164-2.113h4.787v-2.406h-6.855zm-5.924 0.1035-3.156 1.688 0.5762 2.277 2.018-1.086v10.49h2.615v-13.37h-2.053z"/>\n                </svg>\n                <span>Skip back 15 seconds</span>\n            </button>\n\n            <button class="play" type="button" data-display-priority="1">\n                <svg class="icon-play" height="100%" width="100%" viewBox="0 0 80 80" focusable="false">\n                    <path class="background" d="m0 0v80h80v-80h-80zm30 20 16 10 16 10-16 10-16 10v-20-20z"/>\n                    <path class="foreground" d="m30.22 20.25c-0.2231 4.435-0.09351 8.89-0.1419 13.33 0.0088 8.737-0.0362 17.48 0.07256 26.21 10.54-6.515 21.05-13.07 31.52-19.69-2.395-1.814-5.047-3.302-7.568-4.948-7.952-4.981-15.92-9.972-23.89-14.91z"/>\n                </svg>\n                <span>Listen</span>\n                <label>Listen</label>\n            </button>\n\n            <button class="pause" type="button">\n                <svg class="icon-pause" height="100%" width="100%" viewBox="0 0 80 80" focusable="false">\n                    <path class="background" d="m0 0v80h80v-80h-80zm22 20h16v40h-16v-40zm20 0h16v40h-16v-40z"/>\n                    <path class="foreground" d="m22.07 20.09v19.94 19.94h15.88v-39.88h-15.88zm20 0v19.94 19.94h15.88v-39.88h-15.88z"/>\n                </svg>\n                <span>Pause</span>\n            </button>\n\n            <button class="stop" type="button">\n                <svg class="icon-stop" height="100%" width="100%" viewBox="0 0 80 80" focusable="false">\n                    <path class="background" d="m0 0v80h80v-80h-80zm22 20h36v40h-36v-40z"/>\n                    <rect class="foreground" fill-rule="evenodd" height="40" width="36" y="20" x="22"/>\n                </svg>\n                <span>Stop</span>\n            </button>\n\n            <button class="skip-forward" type="button" data-display-priority="3" aria-label="Skip forward 30 seconds">\n                <svg class="icon-skip-forward" height="100%" width="100%" viewBox="0 0 80 80" focusable="false">\n                    <path class="background" d="m0 0v80h80v-80h-80zm40 17 10 7.5-10 7.5v-4c-7.7 0-14 6.3-14 14s6.3 14 14 14 14-6.3 14-14c0-4-1.601-7.6-4.301-10.1l5.602-4.201c3.5 3.7 5.7 8.8 5.7 14.3 0 11.6-9.4 21-21 21s-21-9.4-21-21 9.4-21 21-21v-4zm-5.256 18.11c2.79 0 4.039 1.732 4.039 3.449 0 1.178-0.5509 2.173-1.547 2.846 1.259 0.635 2.023 1.833 2.023 3.281 0 2.027-1.568 4.209-5.01 4.209-1.54 0-2.841-0.495-3.488-0.918l-0.33-0.21 0.8-2.26 0.5332 0.3398c0.379 0.243 1.383 0.7031 2.469 0.7031 0.799 0 1.406-0.2053 1.805-0.6113 0.456-0.465 0.4972-1.05 0.4922-1.279-0.016-1.462-1.402-1.98-2.695-1.98h-1.434v-2.256h1.434c0.908 0 2.258-0.4248 2.258-1.594 0-0.881-0.592-1.365-1.668-1.365-0.863 0-1.716 0.3906-2.184 0.7246l-0.5371 0.3848-0.7891-2.189 0.3008-0.2207c0.709-0.523 2.076-1.049 3.531-1.049zm10.23 0c2.875 0 4.59 2.527 4.59 6.76 0 4.464-1.748 7.025-4.799 7.025-2.793 0-4.554-2.625-4.594-6.85 0-4.28 1.842-6.936 4.803-6.936zm-0.0957 2.293c-1.234 0-2.031 1.828-2.031 4.656 0.000001 2.802 0.7707 4.543 2.012 4.543 1.833 0 2.031-3.256 2.031-4.656 0-1.697-0.2617-4.543-2.012-4.543z"/>\n                    <path class="foreground" d="m40 17v4c-11.6 0-21 9.4-21 21s9.4 21 21 21 21-9.4 21-21c0-5.5-2.2-10.6-5.7-14.3l-5.6 4.2c2.7 2.5 4.3 6.1 4.3 10.1 0 7.7-6.3 14-14 14s-14-6.3-14-14 6.3-14 14-14v4l10-7.5-10-7.5zm-5.256 18.11c-1.455 0-2.822 0.5254-3.531 1.048l-0.3 0.2203 0.7891 2.191 0.5375-0.3859c0.468-0.334 1.32-0.725 2.183-0.725 1.076 0 1.669 0.4846 1.669 1.366 0 1.169-1.351 1.594-2.259 1.594h-1.433v2.256h1.433c1.293 0 2.679 0.5192 2.695 1.981 0.005 0.229-0.03462 0.8131-0.4906 1.278-0.399 0.406-1.007 0.6109-1.806 0.6109-1.086 0-2.09-0.4586-2.469-0.7016l-0.53-0.35-0.8 2.26 0.33 0.22c0.647 0.423 1.949 0.9172 3.489 0.9172 3.442 0 5.009-2.181 5.009-4.208 0-1.448-0.7644-2.646-2.023-3.281 0.996-0.673 1.547-1.669 1.547-2.847 0-1.717-1.249-3.448-4.039-3.448zm10.23 0c-2.961 0-4.802 2.656-4.802 6.936 0.04 4.225 1.799 6.848 4.592 6.848 3.051 0 4.8-2.561 4.8-7.025 0-4.233-1.716-6.759-4.591-6.759zm-0.09 2.29c1.75 0 2.011 2.847 2.011 4.544 0 1.4-0.1967 4.656-2.03 4.656-1.241 0-2.012-1.742-2.012-4.544 0-2.828 0.7973-4.656 2.031-4.656z"/>\n                </svg>\n                <span>Skip forward 30 seconds</span>\n            </button>\n\n            <button class="next display-optional" type="button" data-display-priority="2" aria-label="Skip to next track">\n\n                <svg class="icon-next" height="100%" width="100%" viewBox="266 356 80 80" focusable="false">\n                    <path class="background" d="M266,356v80h80v-80H266z M327.4,408.5h-3.6v-10.7l-21.4,10.7v-8.9l-17.9,8.9v-25l17.9,8.9v-8.9l21.4,10.7v-10.7h3.6V408.5z" />\n                    <polygon class="foreground" points="323.9,383.5 323.9,394.2 302.4,383.5 302.4,392.4 284.6,383.5 284.6,408.5 302.4,399.6 302.4,408.5 323.9,397.8 323.9,408.5 327.4,408.5 327.4,383.5 "/>\n                </svg>\n                <span>Skip to next track</span>\n            </button>\n\n            ';
        canChangeVolume && (__p += '\n            <div class="volume-controls" data-display-priority="5">\n                <button class="unmuted display-optional" type="button" aria-label="Adjust volume">\n\n                <svg class="icon-speaker" height="100%" width="100%" viewBox="0 0 80 80" focusable="false">\n                    <path class="background" d="m0 0v80h80v-80h-80zm45.48 20v20 20l-15-10-4-2.666v0.166h-11v-15h11v0.166l4-2.666 15-10zm15.288 2.371c0.043 0.003 0.087 0.027 0.134 0.078 6.069 3.702 9.967 10.649 9.895 17.764 0.07 7.043-3.951 13.832-9.959 17.418-0.583-0.626-1.088-1.497-1.387-2.324 0.825-0.768 1.94-1.36 2.74-2.223 5.369-4.628 7.189-12.713 4.59-19.258-1.2-3.419-3.574-6.318-6.519-8.392-1.189-0.329-0.764-1.15-0.248-1.887 0.225-0.166 0.449-1.195 0.754-1.176zm-7.055 3.682c5.155 2.711 8.59 7.778 8.889 13.06 0.421 5.502-2.81 11.003-7.967 14.069-0.764 0.823-1.358 0.122-1.721-0.606-0.998-1.047 1.012-1.431 1.637-2.111 2.431-1.797 4.231-4.247 5.013-6.963 1.395-4.488-0.015-9.55-3.599-12.963-0.914-0.819-1.856-1.694-3.033-2.201-0.86-0.664 0.405-1.68 0.781-2.285zm-5.381 4.068c2.74 1.735 4.802 4.576 5.361 7.793 0.99 4.592-1.303 9.54-5.277 11.965-0.581-0.66-1.036-1.588-1.416-2.424 1.092-0.887 2.245-1.855 2.918-3.152 2.101-3.538 1.101-8.362-2.047-10.949-1.056-0.494-1.046-1.09-0.273-1.969l0.734-1.264z"/>\n                    <path class="foreground" d="m36.062 26.367c-3.1042 2.0659-6.2083 4.1318-9.3125 6.1978-3.7292-0.01009-7.4583-0.02018-11.188-0.03027v14.887c3.7039-0.0047 7.4077-0.0095 11.112-0.0144 6.2337 4.1563 12.467 8.3126 18.701 12.469 0.05797-13.235 0.05782-26.471 0-39.707-3.1042 2.0659-6.2083 4.1318-9.3125 6.1978z"/>\n                    <path class="foreground volume-level volume-level-1" d="m48.332 30.121-0.73438 1.2637c-0.77226 0.87946-0.7829 1.4748 0.27344 1.9688 3.1475 2.5873 4.1482 7.4116 2.0469 10.949-0.67331 1.2969-1.8258 2.2651-2.918 3.1523 0.37972 0.83613 0.83496 1.7635 1.416 2.4238 3.9738-2.4251 6.2671-7.3726 5.2773-11.965-0.5594-3.217-2.6209-6.0579-5.3613-7.793z"/>\n                    <path class="foreground volume-level volume-level-2" d="m53.713 26.054c-0.37554 0.60552-1.6401 1.6211-0.78082 2.2846 1.1777 0.50671 2.1189 1.3822 3.0327 2.2011 3.5837 3.413 4.9946 8.4752 3.6002 12.963-0.78264 2.7162-2.5817 5.1657-5.013 6.9624-0.62431 0.68006-2.6367 1.0649-1.6385 2.1118 0.36273 0.72797 0.95745 1.4292 1.7214 0.6058 5.157-3.067 8.388-8.568 7.967-14.07-0.299-5.283-3.734-10.348-8.889-13.059z"/>\n                    <path class="foreground volume-level volume-level-3" d="m60.768 22.37c-0.30421-0.01881-0.52869 1.0101-0.75391 1.1758-0.51546 0.73746-0.94057 1.5584 0.24805 1.8867 2.9452 2.0749 5.3195 4.9734 6.5195 8.3926 2.5984 6.5447 0.77925 14.63-4.5898 19.258-0.8001 0.86325-1.915 1.4555-2.7402 2.2227 0.29904 0.82739 0.80404 1.6987 1.3867 2.3242 6.008-3.5859 10.029-10.374 9.959-17.418 0.07196-7.1151-3.8262-14.062-9.8945-17.764-0.04701-0.05102-0.09131-0.07544-0.13476-0.07813z"/>\n                </svg>\n                <span>Adjust volume</span>\n\n                </button>\n\n                <div class="volume-slider-container">\n                    <input\n                        class="volume-slider"\n                        type="range"\n                        value="0"\n                        max="10"\n                        role="slider"\n                        aria-label="Volume controller"\n                        aria-valuemin="0"\n                        aria-valuemax="10"\n                        aria-orientation="horizontal"\n                    />\n                </div>\n            </div>\n            ');
        __p += '\n        </div>\n\n\n        <div class="extras">\n            <div class="timeinfo" data-display-priority="2">\n                <span class="currenttime"></span>\n                <span class="duration">' + (null == (__t = dataModel.getFormattedDuration()) ? "" : __t) + '</span>\n            </div>\n\n            <button class="captions off ';
        !1 === dataModel.getCaptions() && (__p += "unavailable");
        __p += '" type="button" data-display-priority="1" aria-label="Turn captions on" disabled>\n                <svg class="icon-captions off" height="100%" width="100%" viewBox="0 0 40 40" focusable="false">\n                    <g>\n                        <path class="foreground" d="M19.544,18.575c-0.255-3.102-1.904-4.929-4.855-4.929c-2.696,0-4.815,2.431-4.815,6.337 c0,3.923,1.931,6.371,5.123,6.371c2.548,0,4.332-1.878,4.614-4.996h-3.045c-0.121,1.157-0.604,2.045-1.623,2.045 c-1.609,0-1.904-1.593-1.904-3.286c0-2.33,0.671-3.521,1.784-3.521c0.993,0,1.623,0.721,1.717,1.978H19.544z"/>\n                        <path class="foreground" d="M30.059,18.575c-0.255-3.102-1.904-4.929-4.855-4.929c-2.696,0-4.815,2.431-4.815,6.337 c0,3.923,1.931,6.371,5.123,6.371c2.548,0,4.332-1.878,4.614-4.996h-3.044c-0.121,1.157-0.603,2.045-1.623,2.045 c-1.609,0-1.905-1.593-1.905-3.286c0-2.33,0.671-3.521,1.784-3.521c0.992,0,1.623,0.721,1.717,1.978H30.059z"/>\n                        <path class="foreground" d="M20.137,10.823c10.208,0,12.211,0.739,12.475,0.864c0.109,0.077,0.224,0.151,0.339,0.225 c0.062,0.04,0.142,0.091,0.193,0.127l0.012,0.015c0.872,1.134,1.031,3.27,1.076,7.902c-0.044,4.67-0.203,6.806-1.076,7.94 l-0.029,0.037c-0.044,0.029-0.091,0.056-0.137,0.084c-0.135,0.084-0.269,0.168-0.397,0.259c-0.419,0.206-2.512,0.826-12.442,0.9 C9.879,29.1,7.8,28.445,7.438,28.28l-0.051-0.034c-0.124-0.083-0.251-0.159-0.379-0.235c-0.063-0.037-0.127-0.071-0.185-0.111 c-0.812-1.09-0.983-3.033-1.053-7.924c0.07-4.857,0.239-6.807,1.036-7.901c0.059-0.041,0.166-0.107,0.248-0.158 c0.112-0.069,0.224-0.138,0.333-0.211l0.031-0.021C8.119,11.359,11.547,10.823,20.137,10.823 M20.137,8.823 c-6.656,0-12.667,0.323-13.859,1.217c-0.373,0.248-0.795,0.447-1.043,0.794c-1.217,1.615-1.391,3.9-1.465,9.14 c0.074,5.241,0.248,7.526,1.465,9.14c0.248,0.373,0.671,0.546,1.043,0.795c1.192,0.919,7.203,1.217,13.859,1.267 c6.656-0.05,12.394-0.348,13.611-1.267c0.348-0.248,0.77-0.422,0.993-0.795c1.242-1.614,1.441-3.899,1.49-9.14 c-0.05-5.241-0.249-7.526-1.49-9.14c-0.223-0.347-0.646-0.546-0.993-0.794C32.53,9.146,26.793,8.823,20.137,8.823L20.137,8.823z"/>\n                    </g>\n\n                    <g>\n                        <path class="background" d="M33.143,12.039c-0.051-0.036-0.131-0.087-0.193-0.127c-0.115-0.074-0.229-0.148-0.339-0.225 c-0.264-0.124-2.267-0.864-12.475-0.864c-8.59,0-12.018,0.535-12.719,0.861l-0.031,0.021c-0.109,0.072-0.221,0.142-0.333,0.211 c-0.082,0.05-0.189,0.116-0.248,0.158c-0.798,1.094-0.967,3.044-1.036,7.901c0.07,4.891,0.241,6.835,1.053,7.924 c0.059,0.04,0.123,0.074,0.185,0.111c0.127,0.076,0.255,0.153,0.379,0.235l0.051,0.034c0.362,0.166,2.441,0.82,12.714,0.897 c9.929-0.074,12.023-0.694,12.442-0.9c0.128-0.091,0.262-0.175,0.397-0.259c0.046-0.028,0.093-0.055,0.137-0.084l0.029-0.037 c0.872-1.134,1.031-3.269,1.076-7.94c-0.044-4.633-0.203-6.768-1.076-7.902L33.143,12.039z M14.997,26.354 c-3.192,0-5.123-2.448-5.123-6.371c0-3.906,2.119-6.337,4.815-6.337c2.951,0,4.6,1.827,4.855,4.929H16.54 c-0.094-1.257-0.724-1.978-1.717-1.978c-1.113,0-1.784,1.19-1.784,3.521c0,1.693,0.295,3.286,1.904,3.286 c1.019,0,1.502-0.889,1.623-2.045h3.044C19.329,24.476,17.545,26.354,14.997,26.354z M25.513,26.354 c-3.192,0-5.123-2.448-5.123-6.371c0-3.906,2.119-6.337,4.815-6.337c2.951,0,4.6,1.827,4.855,4.929h-3.004 c-0.094-1.257-0.724-1.978-1.717-1.978c-1.113,0-1.784,1.19-1.784,3.521c0,1.693,0.295,3.286,1.905,3.286 c1.019,0,1.502-0.889,1.623-2.045h3.044C29.845,24.476,28.061,26.354,25.513,26.354z"/>\n                        <path class="background" d="M0,0v40h40V0H0z M34.741,29.115c-0.223,0.373-0.646,0.546-0.993,0.795c-1.217,0.919-6.954,1.217-13.611,1.267 C13.48,31.127,7.47,30.829,6.278,29.91c-0.373-0.248-0.795-0.422-1.043-0.795c-1.217-1.614-1.391-3.899-1.465-9.14 c0.074-5.241,0.248-7.526,1.465-9.14c0.248-0.347,0.671-0.546,1.043-0.794C7.47,9.146,13.48,8.823,20.137,8.823 S32.53,9.146,33.747,10.04c0.348,0.248,0.77,0.447,0.993,0.794c1.242,1.615,1.441,3.9,1.49,9.14 C36.181,25.216,35.982,27.501,34.741,29.115z"/>\n                    </g>\n                </svg>\n\n                <svg class="icon-captions on" height="100%" width="100%" viewBox="0 0 40 40" focusable="false" aria-label="Turn captions off">\n                    <path class="foreground" d="M34.741,10.835c-0.223-0.347-0.646-0.546-0.993-0.794c-1.217-0.894-6.954-1.217-13.611-1.217 S7.47,9.146,6.278,10.04c-0.373,0.248-0.795,0.447-1.043,0.794c-1.217,1.615-1.391,3.9-1.465,9.14 c0.074,5.241,0.248,7.526,1.465,9.14c0.248,0.373,0.671,0.546,1.043,0.795c1.192,0.919,7.203,1.217,13.859,1.267 c6.656-0.05,12.394-0.348,13.611-1.267c0.348-0.248,0.77-0.422,0.993-0.795c1.242-1.614,1.441-3.899,1.49-9.14 C36.181,14.734,35.982,12.449,34.741,10.835z M14.997,26.354c-3.192,0-5.123-2.448-5.123-6.371c0-3.906,2.119-6.337,4.815-6.337 c2.951,0,4.6,1.827,4.855,4.929H16.54c-0.094-1.257-0.724-1.978-1.717-1.978c-1.113,0-1.784,1.19-1.784,3.521 c0,1.693,0.295,3.286,1.904,3.286c1.019,0,1.502-0.889,1.623-2.045h3.044C19.329,24.476,17.545,26.354,14.997,26.354z M25.513,26.354c-3.192,0-5.123-2.448-5.123-6.371c0-3.906,2.119-6.337,4.815-6.337c2.951,0,4.6,1.827,4.855,4.929h-3.004 c-0.094-1.257-0.724-1.978-1.717-1.978c-1.113,0-1.784,1.19-1.784,3.521c0,1.693,0.295,3.286,1.905,3.286 c1.019,0,1.502-0.889,1.623-2.045h3.044C29.845,24.476,28.061,26.354,25.513,26.354z"/>\n                    <g>\n                        <path class="background" d="M14.944,23.403c-1.609,0-1.904-1.593-1.904-3.286c0-2.33,0.671-3.521,1.784-3.521c0.992,0,1.623,0.721,1.717,1.978h3.004 c-0.255-3.102-1.904-4.929-4.855-4.929c-2.696,0-4.815,2.431-4.815,6.337c0,3.923,1.931,6.371,5.123,6.371 c2.548,0,4.332-1.878,4.614-4.996h-3.044C16.446,22.515,15.963,23.403,14.944,23.403z"/>\n                        <path class="background" d="M0,0v40h40V0H0z M34.741,29.115c-0.223,0.373-0.646,0.546-0.993,0.795c-1.217,0.919-6.954,1.217-13.611,1.267 C13.48,31.127,7.47,30.829,6.278,29.91c-0.373-0.248-0.795-0.422-1.043-0.795c-1.217-1.614-1.391-3.899-1.465-9.14 c0.074-5.241,0.248-7.526,1.465-9.14c0.248-0.347,0.671-0.546,1.043-0.794C7.47,9.146,13.48,8.823,20.137,8.823 S32.53,9.146,33.747,10.04c0.348,0.248,0.77,0.447,0.993,0.794c1.242,1.615,1.441,3.9,1.49,9.14 C36.181,25.216,35.982,27.501,34.741,29.115z"/>\n                        <path class="background" d="M25.459,23.403c-1.609,0-1.905-1.593-1.905-3.286c0-2.33,0.671-3.521,1.784-3.521c0.992,0,1.623,0.721,1.717,1.978h3.004 c-0.255-3.102-1.904-4.929-4.855-4.929c-2.696,0-4.815,2.431-4.815,6.337c0,3.923,1.931,6.371,5.123,6.371 c2.548,0,4.332-1.878,4.614-4.996h-3.044C26.961,22.515,26.478,23.403,25.459,23.403z"/>\n                    </g>\n                </svg>\n                <span>Toggle captions</span>\n            </button>\n\n\n            <button class="share" type="button" data-display-priority="4" aria-label="Share">\n\n                <svg class="icon-share" height="100%" width="100%" viewBox="0 0 80 80" focusable="false">\n                    <path class="background"d="m0 0v80h80v-80h-80zm41 18a7 7 0 0 1 7 7 7 7 0 0 1 -7 7 7 7 0 0 1 -3.729 -1.084l-4.352 4.352a7 7 0 0 1 1.08 3.73 7 7 0 0 1 -0.92 3.46l4.463 4.463a7 7 0 0 1 3.46 -0.92 7 7 0 0 1 7 7 7 7 0 0 1 -7 7 7 7 0 0 1 -7 -7 7 7 0 0 1 1.311 -4.066l-4.25-4.24a7 7 0 0 1 -4.06 1.31 7 7 0 0 1 -7 -7 7 7 0 0 1 7 -7 7 7 0 0 1 3.811 1.135l4.32-4.33a7 7 0 0 1 -1.13 -3.81 7 7 0 0 1 7 -7z"/>\n                    <path class="foreground" d="m40.85 18.06c-2.982 0.1058-5.882 2.235-6.546 5.206-0.4989 1.889-0.0789 3.914 0.8901 5.583-1.443 1.446-2.887 2.891-4.33 4.337-2.144-1.325-4.97-1.54-7.174-0.2321-3.411 1.675-4.647 6.403-2.626 9.579 1.535 2.569 4.799 4.024 7.707 3.131 0.8324-0.2156 1.592-0.6278 2.353-1.016 1.389 1.443 2.927 2.801 4.198 4.325-1.6 2.399-1.726 5.809 0.1328 8.115 1.558 2.188 4.403 3.252 7.018 2.673 3.729-0.7675 6.423-4.972 5.18-8.656-0.8398-2.98-3.811-5.275-6.95-4.991-1.119-0.1085-2.117 0.5084-3.155 0.7419-1.654-1.273-3.012-2.924-4.461-4.412 0.6969-1.572 1.11-3.342 0.6401-5.047-0.1771-0.7728-0.5106-1.497-0.8831-2.193 1.447-1.445 2.894-2.89 4.34-4.335 2.263 1.238 5.204 1.497 7.423 0.02124 2.559-1.561 3.99-4.853 3.044-7.754-0.801-2.987-3.707-5.167-6.801-5.077z"/>\n                </svg>\n\n                <span>Share</span>\n            </button>\n\n            <button class="fullscreen unavailable" type="button" data-display-priority="1" aria-label="Fullscreen">\n                <svg viewBox="0 0 80 80" width="100%" height="100%" class="icon-fullscreen" focusable="false">\n                    <path class="background" d="M 0 0 L 0 80 L 80 80 L 80 0 L 0 0 z M 10 20 L 13 20 L 22 20 L 22 23 L 13 23 L 13 32 L 10 32 L 10 23 L 10 20 z M 58 20 L 67 20 L 70 20 L 70 23 L 70 32 L 67 32 L 67 23 L 58 23 L 58 20 z M 20 28.75 L 60 28.75 L 60 51.25 L 20 51.25 L 20 28.75 z M 10 47.5 L 13 47.5 L 13 56.5 L 22 56.5 L 22 59.5 L 13 59.5 L 10 59.5 L 10 56.5 L 10 47.5 z M 67 47.5 L 70 47.5 L 70 56.5 L 70 59.5 L 67 59.5 L 58 59.5 L 58 56.5 L 67 56.5 L 67 47.5 z "/>\n                    <rect class="foreground" y="28.75" x="20" height="22.5" width="40"/>\n                    <path class="foreground" d="m 10,20 0,3 0,9 3,0 0,-9 9,0 0,-3 -9,0 -3,0 z"/>\n                    <path class="foreground" d="m 58,20 0,3 9,0 0,9 3,0 0,-9 0,-3 -3,0 -9,0 z"/>\n                    <path class="foreground" d="m 67,47.5 0,9 -9,0 0,3 9,0 3,0 0,-3 0,-9 -3,0 z"/>\n                    <path class="foreground" d="m 10,47.5 0,9 0,3 3,0 9,0 0,-3 -9,0 0,-9 -3,0 z"/>\n                </svg>\n                <span>Fullscreen</span>\n            </button>\n\n        </div>\n\n        ';
        dataModel.getDuration() === Infinity ? __p += '\n        <div class="seekprogress">\n            <progress class="infinite-buffer" max="Infinity" aria-label="No progress bar for live streams" aria-live="off"></progress>\n        </div>\n        ' : __p += '\n        <div class="seekprogress">\n            <progress class="buffer" value="0" max="' + (null == (__t = dataModel.getDuration()) ? "" : __t) + '" aria-label="Buffer progress" aria-live="off"></progress>\n            <progress class="playback" value="0" max="' + (null == (__t = dataModel.getDuration()) ? "" : __t) + '" aria-label="Playback progress" aria-live="off"></progress>\n            <input\n                class="seek"\n                type="range"\n                value="0"\n                max="' + (null == (__t = dataModel.getDuration()) ? "" : __t) + '"\n                role="slider"\n                aria-label="Audio time controller"\n                aria-valuemin="0 seconds"\n                aria-valuemax="' + (null == (__t = dataModel.getDuration()) ? "" : __t) + ' seconds"\n                aria-orientation="horizontal"\n             />\n        </div>\n        ';
        __p += "\n    </div>\n</div>\n"
    }
    return __p
};
//# sourceMappingURL=Caffeine.modules.js.map

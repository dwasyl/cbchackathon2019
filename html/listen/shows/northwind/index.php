<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title>CBC Listen | Northwind</title>
        <script type="text/javascript" src="/assets/js/underscore-min.js"></script>
        <script type="text/javascript" src="/assets/js/Listen.js"></script>
        <script type="text/javascript" src="/assets/js/Templates.js"></script>
        <script type="text/javascript" src="/assets/js/Caffeine.js"></script>
        <link rel="stylesheet" href="/assets/css/listen.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/fonts-cloud-typography.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

            <meta name="description" content="Listeners can catch up with the day's events from communities across the territory hosted from our Inuvik studio by Wanda McLeod.">

    <meta property="fb:app_id" content="2127360530839542">
    <meta property="og:url" content="https://www.cbc.ca/listen/shows/northwind">
    <meta property="og:title" content="Northwind">
    <meta property="og:description" content="Listeners can catch up with the day's events from communities across the territory hosted from our Inuvik studio by Wanda McLeod.">
    <meta property="og:site_name" content="CBC Listen">

        <meta property="og:image" content="https://www.cbc.ca/radio/includes/apps/images/coverimage/northwind-header.jpg">
    
    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">

    <meta name="twitter:site" content="@cbcradio" />
    <meta name="twitter:url" content="https://www.cbc.ca/listen/shows/northwind" />
    <meta name="twitter:card" content="summary_large_image" />

        <meta name="twitter:image" content="https://www.cbc.ca/radio/includes/apps/images/coverimage/northwind-header.jpg" />
    
    
        <script type="text/javascript">
            //Hierarchy value for making proper ad calls
            if (!CBC){ var CBC = {}; }
            if (!CBC.Metadata) { CBC.Metadata = {}; }
            CBC.Metadata.hierarchy = "listen";
        </script>
        <script type="text/javascript" src="/assets/js/atlas_ads.js"></script>
    
<script>!function(){function o(n,i){if(n&&i)for(var r in i)i.hasOwnProperty(r)&&(void 0===n[r]?n[r]=i[r]:n[r].constructor===Object&&i[r].constructor===Object?o(n[r],i[r]):n[r]=i[r])}try{var n=decodeURIComponent("");if(n.length>0&&window.JSON&&"function"==typeof window.JSON.parse){var i=JSON.parse(n);void 0!==window.BOOMR_config?o(window.BOOMR_config,i):window.BOOMR_config=i}}catch(r){window.console&&"function"==typeof window.console.error&&console.error("mPulse: Could not parse configuration",r)}}();</script>
<script>!function(e){function a(a){if(a&&a.data&&a.data.boomr_mq)e.BOOMR_mq=e.BOOMR_mq||[],e.BOOMR_mq.push(a.data.boomr_mq)}var t="https://s.go-mpulse.net/boomerang/";if("False"=="True")e.BOOMR_config=e.BOOMR_config||{},e.BOOMR_config.PageParams=e.BOOMR_config.PageParams||{},e.BOOMR_config.PageParams.pci=!0,t="https://s2.go-mpulse.net/boomerang/";if(function(){function a(a){e.BOOMR_onload=a&&a.timeStamp||(new Date).getTime()}if(!e.BOOMR||!e.BOOMR.version&&!e.BOOMR.snippetExecuted){e.BOOMR=e.BOOMR||{},e.BOOMR.snippetExecuted=!0;var n,i,r,o=document.createElement("iframe");if(e.addEventListener)e.addEventListener("load",a,!1);else if(e.attachEvent)e.attachEvent("onload",a);o.src="javascript:void(0)",o.title="",o.role="presentation",(o.frameElement||o).style.cssText="width:0;height:0;border:0;display:none;",r=document.getElementsByTagName("script")[0],r.parentNode.insertBefore(o,r);try{i=o.contentWindow.document}catch(O){n=document.domain,o.src="javascript:var d=document.open();d.domain='"+n+"';void(0);",i=o.contentWindow.document}i.open()._l=function(){var e=this.createElement("script");if(n)this.domain=n;e.id="boomr-if-as",e.src=t+"PV5QJ-UM4B9-ECQUJ-YKUFG-EAKA2",BOOMR_lstart=(new Date).getTime(),this.body.appendChild(e)},i.write("<bo"+'dy onload="document._l();">'),i.close()}}(),"".length>0)if(e&&"performance"in e&&e.performance&&"function"==typeof e.performance.setResourceTimingBufferSize)e.performance.setResourceTimingBufferSize();if(e.addEventListener)e.addEventListener("message",a);var n=e.navigator;if(n&&"serviceWorker"in n&&n.serviceWorker.addEventListener)n.serviceWorker.addEventListener("message",a);!function(){if(BOOMR=e.BOOMR||{},BOOMR.plugins=BOOMR.plugins||{},!BOOMR.plugins.AK){var a=""=="true"?1:0,t="",n="zxgf47nydkyh6xcvfx5q-f-502683f7f-clientnsv4-s.akamaihd.net",i={"ak.v":19,"ak.cp":"9617","ak.ai":parseInt("185178",10),"ak.ol":"0","ak.cr":1,"ak.ipv":4,"ak.proto":"h2","ak.rid":"c641b42","ak.r":14022,"ak.a2":a,"ak.m":"e12","ak.n":"essl","ak.bpcip":"205.204.94.0","ak.cport":10950,"ak.gh":"67.69.197.106","ak.quicv":"","ak.tlsv":"tls1.2","ak.0rtt":"","ak.csrc":"-","ak.acc":""};if(""!==t)i["ak.ruds"]=t;var r={i:!1,av:function(a){var t="http.initiator";if(a&&(!a[t]||"spa_hard"===a[t]))i["ak.feo"]=void 0!==e.aFeoApplied?1:0,BOOMR.addVar(i)},rv:function(){var e=["ak.bpcip","ak.cport","ak.cr","ak.csrc","ak.gh","ak.ipv","ak.m","ak.n","ak.ol","ak.proto","ak.quicv","ak.tlsv","ak.0rtt","ak.r","ak.acc"];BOOMR.removeVar(e)}};BOOMR.plugins.AK={akVars:i,akDNSPreFetchDomain:n,init:function(){if(!r.i){var e=BOOMR.subscribe;e("before_beacon",r.av,null,null),e("onbeacon",r.rv,null,null),r.i=!0}return this},is_complete:function(){return!0}}}}()}(window);</script><script>bazadebezolkohpepadr="1752762695"</script></head>
    <body>
        <script type="text/javascript" src="/assets/js/cbc-stats-top.js"></script>

        <div class="ule-banner-wrapper">
	<div class="ule-banner">
		<div class="ule-banner__text">
		TRUE NORTH. The new CBC Listen is the best place to discover and hear CBC Radio One, CBC Music & CBC Podcasts · <a target="_self" href="https://betalisten.cbc.ca">Try it!</a>
		</div>
		<button id="banner-close-button" class="ule-banner__close"></button>
	</div>
</div>
<header>
	<div class="header-inner">
    <a href="#Caffeinelisten-player" id="accessibilitySkipLink" data-linktype="skiplink" tabindex="0">Skip to My Radio Player</a>
    <span class="sclt-header">
	    <a href="/listen/" class="logo">
	        <h1>CBC Listen</h1>
	    </a>
    </span>
    <button id="menu-button" class="icon-menu" data-btntype="hamburger" aria-label="Open Navigation" aria-controls="main-nav" aria-expanded="false">Open Navigation</button>
<nav id="main-nav" role="navigation" aria-label="CBC Listen">
    <ul class="hide">
        <li class="sclt-nav"><a class="menu-link" id="menu-featured" href="/listen/">Featured</a></li>
        <li class="sclt-nav"><a class="menu-link" id="menu-live" href="/listen/live">Listen Live</a></li>
        <li class="sclt-nav"><a class="menu-link" id="menu-shows" href="/listen/shows">Shows</a></li>
        <li class="has-submenu">
            <button class="menu-link" id="menu-categories" data-btntype="toggleSubnav" aria-controls="menu-categories" aria-expanded="false" aria-haspopup="true">Categories</button>
                        <ul aria-expanded="false" aria-labelledby="menu-categories" class="subnav-menu" role="group"><li class="sclt-nav"><a class="menu-link" href="/listen/categories/news">News</a></li>
            <li class="sclt-nav"><a class="menu-link" href="/listen/categories/politics">Politics</a></li>
            <li class="sclt-nav"><a class="menu-link" href="/listen/categories/arts-entertainment">Arts & Entertainment</a></li>
            <li class="sclt-nav"><a class="menu-link" href="/listen/categories/society-culture">Society & Culture</a></li>
            <li class="sclt-nav"><a class="menu-link" href="/listen/categories/comedy">Comedy</a></li>
            <li class="sclt-nav"><a class="menu-link" href="/listen/categories/science">Science</a></li>
            <li class="sclt-nav"><a class="menu-link" href="/listen/categories/technology">Technology</a></li>
            <li class="sclt-nav"><a class="menu-link" href="/listen/categories/health">Health</a></li>
            <li class="sclt-nav"><a class="menu-link" href="/listen/categories/books">Books</a></li>
            <li class="sclt-nav"><a class="menu-link" href="/listen/categories/business">Business</a></li>
            <li class="sclt-nav"><a class="menu-link" href="/listen/categories/environment-nature">Environment & Nature</a></li>
            <li class="sclt-nav"><a class="menu-link" href="/listen/categories/lifestyle">Lifestyle</a></li>
            </ul>
                        <div class="subnav-pointer"></div>
        </li>
        <li class="desktop-only"><div class="myradio-wrapper">
    <button class="icon-myradio" data-btntype="toggleOverlay" data-overlaytype="Playlist" aria-expanded="false" aria-label="Open Playlist"></button>
    <div class="playlist-title">My Radio</div>
    <span class="screen-reader" role="tooltip">My Radio</span>
</div>
<div class="live-pulse-container"><div class="pulse-ring"></div><div class="pulse-ring delay"></div><div class="pulse-ring delay"></div><div class="pulse-dot"></div></div></li>
        <li><a href="https://cbchelp.cbc.ca/hc/en-ca/sections/115000541294-Listen" target="_blank">FAQ</a></li>
        <li class="search-li">
            <div id="SearchContainer" class="search-container">
                <div class="search-box" role="search">
                    <form class="sclt-search" id="search" action="/listen/search" method="GET">
                        <input name="query" title="Search" placeholder="Search" type="text" aria-label="Search" data-overlaytype="Search" />
                        <button class="icon-search" type="submit" data-btntype="search">Search</button>
                    </form>
                </div>
            </div>
        </li>
    </ul>
</nav>  </div>
</header>
<div id="loading-dots"><span></span><span></span></div>
        <main class="page">
            <div class="show-wrapper" style="background-color:#1f1b1c">
    <div class="page-banner" style="background-color:#1f1b1c">
        <div class="page-banner-image">
            <a href="/listen/shows/northwind">
                <img
    class="round"
    src="https://www.cbc.ca/radio/podcasts/images/hosts/northwind-circle.jpg"
    alt="Show host graphic"
    srcset="
                https://www.cbc.ca/radio/podcasts/images/hosts/northwind-circle.jpg?crop=h:h;*,*&crop=w:w;*,*&downsize=1280px:* 1280w,
        https://www.cbc.ca/radio/podcasts/images/hosts/northwind-circle.jpg?crop=h:h;*,*&crop=w:w;*,*&downsize=1100px:* 1100w,
        https://www.cbc.ca/radio/podcasts/images/hosts/northwind-circle.jpg?crop=h:h;*,*&crop=w:w;*,*&downsize=920px:* 920w,
        https://www.cbc.ca/radio/podcasts/images/hosts/northwind-circle.jpg?crop=h:h;*,*&crop=w:w;*,*&downsize=740px:* 740w,
        https://www.cbc.ca/radio/podcasts/images/hosts/northwind-circle.jpg?crop=h:h;*,*&crop=w:w;*,*&downsize=560px:* 560w,
        https://www.cbc.ca/radio/podcasts/images/hosts/northwind-circle.jpg?crop=h:h;*,*&crop=w:w;*,*&downsize=380px:* 380w,
        https://www.cbc.ca/radio/podcasts/images/hosts/northwind-circle.jpg?crop=h:h;*,*&crop=w:w;*,*&downsize=200px:* 200w
    "
    sizes="
                (min-width: 1024px) 10vw,
        15vw
    "
/></a>
        </div>
        <div class="page-banner-title">
            <h1>
                <a href="/listen/shows/northwind">Northwind<span> with Wanda McLeod</span>
                </a>
            </h1>
            <p>Listeners can catch up with the day's events from communities across the territory hosted from our Inuvik studio by Wanda McLeod.</p>
        </div>
        <div class="page-banner-links sclt-showsBanner">
            <div class="share-wrapper share-wrapper-alink">
                <span><a href="https://www.cbc.ca/north">
                    <svg width="100%" height="100%" viewBox="0 0 71.955 63.549" >
                        <g>
                            <path class="st0" d="M70.189,36.026c-0.021-0.035-0.043-0.069-0.062-0.105c-0.689-1.267-1.543-2.413-2.535-3.406L54.037,18.961
                                c-0.055-0.057-0.107-0.104-0.154-0.146c-1.726-1.675-3.826-2.903-6.109-3.581c2.631,2.754,4.373,6.194,5.049,9.979
                                c0.201,1.135,0.305,2.295,0.305,3.449c0,0.46-0.02,0.945-0.061,1.481l8.447,8.449c0.734,0.734,1.279,1.66,1.572,2.677
                                c0.17,0.584,0.254,1.191,0.254,1.807c0,0.682-0.104,1.354-0.309,1.999c-0.311,0.972-0.82,1.817-1.518,2.514l-5.508,5.508
                                c-1.18,1.18-2.777,1.83-4.498,1.83c-1.721,0-3.318-0.65-4.498-1.83l-5.25-5.25l-3.398-3.398l-4.904-4.904
                                c-0.488-0.488-0.893-1.064-1.201-1.715c-0.408-0.855-0.623-1.818-0.623-2.781c0-0.65,0.096-1.293,0.283-1.91
                                c0.018-0.055,0.037-0.109,0.059-0.164l0.033-0.09c0.08-0.227,0.154-0.41,0.232-0.578c0.045-0.098,0.094-0.193,0.145-0.289
                                c0.092-0.174,0.195-0.346,0.316-0.529l0.051-0.076c0.037-0.055,0.072-0.111,0.111-0.164c0.201-0.273,0.395-0.502,0.594-0.699
                                l2.1-2.1c-0.023-0.25-0.08-0.473-0.172-0.662c-0.088-0.186-0.195-0.344-0.322-0.471l-4.904-4.904l-0.361-0.361l-2.418,2.42
                                c-0.234,0.234-0.469,0.486-0.717,0.773c-0.037,0.041-0.072,0.086-0.107,0.129l-0.078,0.096c-0.166,0.197-0.324,0.396-0.477,0.6
                                c-0.053,0.068-0.102,0.139-0.152,0.207l-0.029,0.041c-0.15,0.211-0.295,0.424-0.434,0.641l-0.037,0.057
                                c-0.033,0.051-0.066,0.104-0.1,0.154c-0.162,0.266-0.318,0.535-0.459,0.799l-0.021,0.039l-0.019,0.037
                                c-0.492,0.936-0.887,1.932-1.172,2.965c-0.102,0.367-0.182,0.707-0.248,1.035c-0.201,1.002-0.303,2.014-0.303,3.02
                                c0,0.885,0.08,1.773,0.232,2.637c0.539,3.027,1.965,5.768,4.119,7.924l2.824,2.824l4.125,4.123l6.605,6.605
                                c5.832,5.832,15.32,5.832,21.152,0l5.508-5.508c1.885-1.886,3.23-4.269,3.891-6.894c0.312-1.248,0.473-2.487,0.473-3.683
                                c0-2.363-0.568-4.724-1.641-6.826c-0.012-0.024-0.025-0.05-0.035-0.076c-0.01-0.02-0.018-0.04-0.029-0.06
                                C70.232,36.092,70.211,36.06,70.189,36.026z"/>
                            <path class="st0" d="M44.576,17.927l-2.824-2.824l-4.123-4.123l-6.605-6.607c-5.832-5.83-15.32-5.83-21.152,0.002L4.363,9.882
                                c-1.887,1.885-3.23,4.269-3.891,6.893C0.16,18.023,0,19.261,0,20.458c0,2.363,0.568,4.723,1.641,6.826
                                c0.014,0.023,0.025,0.051,0.035,0.076c0.01,0.02,0.018,0.041,0.029,0.061c0.018,0.033,0.037,0.066,0.059,0.102
                                c0.022,0.035,0.043,0.07,0.063,0.107c0.691,1.268,1.545,2.412,2.537,3.404l13.553,13.553c0.057,0.059,0.108,0.105,0.156,0.146
                                c1.727,1.674,3.824,2.902,6.107,3.58c-2.629-2.754-4.371-6.193-5.045-9.979c-0.203-1.131-0.307-2.293-0.307-3.449
                                c0-0.457,0.019-0.941,0.06-1.48l-8.447-8.449c-0.734-0.734-1.279-1.66-1.572-2.678c-0.17-0.584-0.256-1.19-0.256-1.805
                                c0-0.682,0.106-1.355,0.311-2c0.311-0.973,0.82-1.818,1.517-2.514l5.508-5.508c2.439-2.439,6.559-2.439,8.996,0l5.25,5.25
                                l3.399,3.398l4.904,4.904c0.488,0.488,0.893,1.065,1.201,1.715c0.406,0.855,0.623,1.816,0.623,2.781c0,0.65-0.096,1.293-0.283,1.91
                                c-0.018,0.057-0.037,0.111-0.059,0.168l-0.033,0.086c-0.08,0.226-0.154,0.41-0.232,0.578c-0.045,0.098-0.096,0.193-0.146,0.289
                                c-0.092,0.176-0.197,0.354-0.314,0.529l-0.047,0.07c-0.037,0.059-0.074,0.115-0.115,0.17c-0.201,0.273-0.395,0.502-0.592,0.699
                                l-2.102,2.1c0.023,0.25,0.08,0.473,0.172,0.664c0.088,0.185,0.195,0.344,0.322,0.469l4.904,4.904l0.361,0.361l2.418-2.42
                                c0.23-0.23,0.465-0.484,0.719-0.773c0.037-0.043,0.074-0.088,0.109-0.133l0.074-0.09c0.166-0.199,0.324-0.398,0.477-0.603
                                c0.062-0.081,0.121-0.163,0.18-0.247c0.154-0.211,0.297-0.424,0.438-0.641l0.045-0.071l0.088-0.141
                                c0.164-0.264,0.318-0.532,0.461-0.799l0.02-0.036l0.021-0.04c0.492-0.933,0.887-1.93,1.174-2.964
                                c0.098-0.356,0.178-0.697,0.246-1.037c0.201-1,0.303-2.012,0.303-3.018c0-0.887-0.08-1.774-0.232-2.638
                                C48.156,22.824,46.732,20.083,44.576,17.927z"/>
                        </g>
                    </svg>Visit Website</a></span>
            </div><div class="share-wrapper">
    <button class="icon-share"
        data-btntype="toggleOverlay"
        data-overlaytype="Share"
        data-url="https://www.cbc.ca/listen/shows/northwind"
        data-audioid=""
        data-liveradioid=""
        data-sharemessage="Northwind%0A%0Ahttps://www.cbc.ca/listen/shows/northwind%0A%0AListeners%20can%20catch%20up%20with%20the%20day%27s%20events%20from%20communities%20across%20the%20territory%20hosted%20from%20our%20Inuvik%20studio%20by%20Wanda%20McLeod.%0A%0AShared%20from%20cbc.ca%2Flisten%3A%20Listen%20to%20CBC%20Radio%27s%20live%20streams%20and%20shows%20on%20demand."
        data-sharesubject= Listen%20to%20this%2C%20from%20CBC%20Radio%20        data-sharetweet="Northwind"
        aria-expanded="false"
        aria-label="Open Share">
        <svg viewBox="0 0 47.895 71.961" width="100%" height="100%">
            <g>
                <path class="share-icon-path" d="M35.9,47.974c-1.712,0-3.448,0.566-3.448,0.566c-0.749,0.245-1.819-0.012-2.376-0.568l-6.138-6.139
                    c-0.557-0.558-0.808-1.625-0.557-2.372c0,0,0.606-1.806,0.606-3.561c0-1.625-0.489-3.214-0.489-3.214
                    c-0.232-0.753,0.034-1.826,0.592-2.384l6.214-6.213c0.558-0.558,1.63-0.825,2.384-0.593c0,0,1.589,0.49,3.213,0.49
                    c6.624,0,11.994-5.369,11.994-11.993C47.895,5.369,42.524,0,35.9,0c-6.623,0-11.993,5.369-11.993,11.994
                    c0,1.7,0.557,3.419,0.557,3.419c0.242,0.751-0.015,1.82-0.572,2.378l-6.101,6.1c-0.558,0.558-1.627,0.815-2.377,0.572
                    c0,0-1.718-0.557-3.42-0.557C5.37,23.906,0,29.276,0,35.9c0,6.624,5.37,11.993,11.993,11.993c1.569,0,3.063-0.446,3.063-0.446
                    c0.755-0.226,1.83,0.046,2.387,0.604l6.351,6.351c0.558,0.557,0.825,1.63,0.594,2.384c0,0-0.48,1.569-0.48,3.182
                    c0,6.625,5.37,11.994,11.993,11.994c6.624,0,11.994-5.369,11.994-11.994C47.895,53.343,42.524,47.974,35.9,47.974z"/>
            </g>
        </svg><span>SHARE SHOW</span>    </button>
</div>        </div>
    </div>
</div>

<section class="show-details">
    <div id="ad-wrapper" class="ad-wrapper"><div id="bigbox" class="ad bigbox cbc-big-box-ad"></div></div>
    <div class="show-clips sclt-showClipsnorthwind">
        <ul class="clip-types">
            <li class="clip-type active">
                <a href="/listen/shows/northwind">All</a>
            </li>
            <li class="clip-type">
                <a href="/listen/shows/northwind/episodes">Episodes</a>
            </li>
            <li class="clip-type">
                <a href="/listen/shows/northwind/segments">Segments</a>
            </li>
        </ul>

        <ul class="medialist moreclips"
     data-moreclips-path="/listen/shows/northwind/clips/All/">


<!CBC HACKATHON START->


<li class="medialist-item">
                    <div class="medialist-item-status">
        <button id="icon-play" class="icon-play" data-btntype="play" data-audioid="15649047" data-liveradioid="" aria-label="Play">
    <svg class="play" version="1.1" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40">
        <path d="M20,0C9,0,0,9,0,20c0,11,9,20,20,20s20-9,20-20C40,9,31,0,20,0z M20,38c-9.9,0-18-8.1-18-18c0-9.9,8.1-18,18-18 s18,8.1,18,18C38,29.9,29.9,38,20,38z"/>
        <polygon points="16.2,13.8 16.2,26.2 27.5,20 "/>
    </svg>

    <svg class="pause" height="100%" width="100%" viewBox="0 0 80 80">
        <path class="background" d="m0 0v80h80v-80h-80zm22 20h16v40h-16v-40zm20 0h16v40h-16v-40z"></path>
    </svg>

    <svg class="stop" height="100%" width="100%" viewBox="0 0 80 80">
        <path d="m0 0v80h80v-80h-80zm22 20h36v40h-36v-40z" class="background"/>
         <g display="none">
          <rect fill-rule="evenodd" height="40" width="36" y="20" x="22" class="foreground" fill="#fff"/>
         </g>
    </svg>
</button>        <span class="medialist-item-duration">21:11</span>
    </div>

                    <div class="medialist-item-meta">
                        <div class="medialist-item-title">
                            <a href="/listen/shows/northwind/episode/867" data-linktype="toc">Friday Afternoon Call in Request Show</a>
                        </div>

                        <div class="medialist-item-description">
                            Community announcements, greetings, best wishes and more from community members across the Northwest Territories.
                        </div>

                        <div class="medialist-item-actions">
                            <div class="medialist-item-date">
                                <span>Feb 1, 2019</span>
                            </div>

                            <div class="share-wrapper">
    <button class="icon-share"
        data-btntype="toggleOverlay"
        data-overlaytype="Share"
        data-url="https://www.cbc.ca/listen/shows/northwind/episode/15649047"
        data-audioid="15649047"
        data-liveradioid=""
        data-sharemessage="Northern%20filmmaker%20and%20storyteller%20reflects%20on%20his%20memories%20of%20CBC%20North%0A%0Ahttps://www.cbc.ca/listen/shows/northwind/episode/15649047%0A%0ACBC%20is%20celebrating%2060%20years%20of%20radio%20in%20the%20North.%20Northern%20filmmaker%20and%20storyteller%20Raymond%20Yakeleya%20joins%20Loren%20McGinnis%20on%20Northwind%20to%20reflect%20on%20some%20of%20his%20memories%20of%20CBC%20North.%0A%0AShared%20from%20cbc.ca%2Flisten%3A%20Listen%20to%20CBC%20Radio%27s%20live%20streams%20and%20shows%20on%20demand."
        data-sharesubject= Listen%20to%20this%2C%20from%20the%20CBC%20Radio%20show%20Northwind        data-sharetweet="Northern+filmmaker+and+storyteller+reflects+on+his+memories+of+CBC+North"
        aria-expanded="false"
        aria-label="Open Share">
        <svg viewBox="0 0 47.895 71.961" width="100%" height="100%">
            <g>
                <path class="share-icon-path" d="M35.9,47.974c-1.712,0-3.448,0.566-3.448,0.566c-0.749,0.245-1.819-0.012-2.376-0.568l-6.138-6.139
                    c-0.557-0.558-0.808-1.625-0.557-2.372c0,0,0.606-1.806,0.606-3.561c0-1.625-0.489-3.214-0.489-3.214
                    c-0.232-0.753,0.034-1.826,0.592-2.384l6.214-6.213c0.558-0.558,1.63-0.825,2.384-0.593c0,0,1.589,0.49,3.213,0.49
                    c6.624,0,11.994-5.369,11.994-11.993C47.895,5.369,42.524,0,35.9,0c-6.623,0-11.993,5.369-11.993,11.994
                    c0,1.7,0.557,3.419,0.557,3.419c0.242,0.751-0.015,1.82-0.572,2.378l-6.101,6.1c-0.558,0.558-1.627,0.815-2.377,0.572
                    c0,0-1.718-0.557-3.42-0.557C5.37,23.906,0,29.276,0,35.9c0,6.624,5.37,11.993,11.993,11.993c1.569,0,3.063-0.446,3.063-0.446
                    c0.755-0.226,1.83,0.046,2.387,0.604l6.351,6.351c0.558,0.557,0.825,1.63,0.594,2.384c0,0-0.48,1.569-0.48,3.182
                    c0,6.625,5.37,11.994,11.993,11.994c6.624,0,11.994-5.369,11.994-11.994C47.895,53.343,42.524,47.974,35.9,47.974z"/>
            </g>
        </svg>    </button>
</div>
            <div class="plus-wrapper">
    <button class="icon-plus" data-btntype="toggleOverlay" data-overlaytype="Plus" data-id="15649047" aria-expanded="false" aria-label="Add to My Radio options">
        <svg class="indicator-unadded" version="1.1" x="0px" y="0px"
     viewBox="0 0 40 40" enable-background="new 0 0 40 40;">
        <g>
            <path d="M39.972,19.265c-0.012-1.969-1.475-3.596-3.37-3.907C35.473,7.153,28.447,1,20.123,1
                C11.744,1,4.797,7.113,3.655,15.324c-0.946,0.077-1.827,0.465-2.504,1.146C0.402,17.223-0.006,18.219,0,19.275l0.028,4.447
                c0.014,2.19,1.818,3.972,4.021,3.972c0.52,0,1.022-0.103,1.49-0.291C7.172,34.012,13.087,38.709,20,38.709
                c6.861,0,12.831-4.729,14.461-11.328c0.48,0.202,1.006,0.313,1.557,0.313c1.071,0,2.076-0.418,2.831-1.178
                c0.748-0.753,1.157-1.749,1.15-2.804L39.972,19.265z M20.123,2.569c7.485,0,13.807,5.494,14.899,12.843
                c-0.713,0.17-1.371,0.524-1.901,1.058c-0.032,0.032-0.054,0.071-0.085,0.104C30.426,11.866,25.459,8.89,20,8.89
                c-5.498,0-10.456,2.987-13.057,7.723c-0.466-0.509-1.059-0.89-1.731-1.108C6.262,8.087,12.546,2.569,20.123,2.569z M1.597,23.712
                l-0.028-4.447c-0.004-0.634,0.243-1.234,0.695-1.689c0.458-0.461,1.068-0.715,1.717-0.715c1.343,0,2.443,1.082,2.452,2.413
                l0.028,4.447c0.004,0.634-0.243,1.234-0.694,1.688c-0.458,0.461-1.068,0.715-1.718,0.715C2.706,26.124,1.605,25.042,1.597,23.712z
                 M20,37.139c-6.385,0-11.815-4.476-13.065-10.691c0.709-0.745,1.102-1.711,1.096-2.737l-0.028-4.447
                c-0.002-0.34-0.061-0.664-0.144-0.98c2.164-4.766,6.876-7.826,12.141-7.826c5.225,0,9.946,3.045,12.12,7.776
                c-0.09,0.337-0.152,0.682-0.15,1.039l0.028,4.447c0.006,1.054,0.435,2.007,1.115,2.719l-0.033-0.007
                C31.838,32.637,26.337,37.139,20,37.139z M37.737,25.409c-0.458,0.461-1.068,0.715-1.718,0.715c-1.344,0-2.443-1.082-2.451-2.413
                l-0.028-4.447c-0.004-0.634,0.243-1.234,0.694-1.688c0.458-0.461,1.068-0.715,1.718-0.715c1.344,0,2.443,1.082,2.451,2.413
                l0.028,4.447C38.435,24.356,38.188,24.955,37.737,25.409z"/>
            <polygon points="20.589,15.489 19.411,15.489 19.411,23.211 11.69,23.211 11.69,24.388 19.411,24.388
                19.411,32.11 20.589,32.11 20.589,24.388 28.311,24.388 28.311,23.211 20.589,23.211   "/>
        </g>
        </svg>
        <svg class="indicator-added" viewBox="0 0 40 40">
            <path d="M39.804,19.416c-0.012-1.952-1.462-3.566-3.342-3.874c-1.12-8.135-8.086-14.237-16.341-14.237
            c-8.308,0-15.198,6.061-16.329,14.204c-0.938,0.076-1.812,0.461-2.483,1.136c-0.742,0.747-1.147,1.734-1.141,2.781l0.027,4.409
            c0.013,2.172,1.802,3.939,3.987,3.939c0.52,0,1.024-0.102,1.491-0.292C7.277,33.911,13.081,38.696,20,38.696
            c6.927,0,12.736-4.797,14.331-11.238c0.478,0.202,1.003,0.316,1.553,0.316c1.062,0,2.058-0.415,2.807-1.168
            c0.742-0.747,1.147-1.734,1.141-2.781L39.804,19.416z M5.886,25.509c-0.454,0.457-1.059,0.709-1.704,0.709
            c-1.332,0-2.423-1.073-2.431-2.392l-0.027-4.409c-0.004-0.629,0.241-1.224,0.689-1.674c0.454-0.457,1.059-0.709,1.703-0.709
            c1.332,0,2.423,1.073,2.431,2.393l0.027,4.409C6.579,24.463,6.334,25.058,5.886,25.509z M18.247,31.022l-6.669-6.669l1.238-1.238
            l5.432,5.431l10.189-10.189l1.238,1.238L18.247,31.022z M32.912,16.764C30.388,12.22,25.556,9.128,20,9.128
            c-5.572,0-10.415,3.109-12.933,7.674c-0.464-0.512-1.056-0.897-1.729-1.115c1.04-7.356,7.272-12.827,14.784-12.827
            c7.423,0,13.691,5.448,14.774,12.735c-0.707,0.169-1.359,0.52-1.885,1.049C32.973,16.681,32.947,16.726,32.912,16.764z
            M37.587,25.509c-0.454,0.457-1.059,0.709-1.703,0.709c-1.332,0-2.423-1.073-2.431-2.392l-0.027-4.409
            c-0.004-0.629,0.241-1.224,0.689-1.674c0.454-0.457,1.059-0.709,1.703-0.709c1.332,0,2.423,1.073,2.431,2.393l0.027,4.409
            C38.279,24.463,38.035,25.058,37.587,25.509z"/>
        </svg>
    </button>
</div>
                        </div>
                    </div>
                </li>

<!CBC HACKATHON END->

<li class="medialist-item">
    <div class="medialist-item-status">
        <button id="icon-play" class="icon-play" data-btntype="play" data-audioid="15649047" data-liveradioid="" aria-label="Play">
    <svg class="play" version="1.1" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40">
        <path d="M20,0C9,0,0,9,0,20c0,11,9,20,20,20s20-9,20-20C40,9,31,0,20,0z M20,38c-9.9,0-18-8.1-18-18c0-9.9,8.1-18,18-18 s18,8.1,18,18C38,29.9,29.9,38,20,38z"/>
        <polygon points="16.2,13.8 16.2,26.2 27.5,20 "/>
    </svg>

    <svg class="pause" height="100%" width="100%" viewBox="0 0 80 80">
        <path class="background" d="m0 0v80h80v-80h-80zm22 20h16v40h-16v-40zm20 0h16v40h-16v-40z"></path>
    </svg>

    <svg class="stop" height="100%" width="100%" viewBox="0 0 80 80">
        <path d="m0 0v80h80v-80h-80zm22 20h36v40h-36v-40z" class="background"/>
         <g display="none">
          <rect fill-rule="evenodd" height="40" width="36" y="20" x="22" class="foreground" fill="#fff"/>
         </g>
    </svg>
</button>        <span class="medialist-item-duration">13:50</span>
    </div>

    <div class="medialist-item-meta">

        <div class="medialist-item-title">
            <a href="/listen/shows/northwind/episode/15649047">
                Northern filmmaker and storyteller reflects on his memories of CBC North            </a>
        </div>

        <div class="medialist-item-description">
            CBC is celebrating 60 years of radio in the North. Northern filmmaker and storyteller Raymond Yakeleya joins Loren McGinnis on Northwind to reflect on some of his memories of CBC North.        </div>

        
        <div class="medialist-item-actions">
            <div class="medialist-item-date">
                <span>Dec 13, 2018</span>
            </div>
            <div class="share-wrapper">
    <button class="icon-share"
        data-btntype="toggleOverlay"
        data-overlaytype="Share"
        data-url="https://www.cbc.ca/listen/shows/northwind/episode/15649047"
        data-audioid="15649047"
        data-liveradioid=""
        data-sharemessage="Northern%20filmmaker%20and%20storyteller%20reflects%20on%20his%20memories%20of%20CBC%20North%0A%0Ahttps://www.cbc.ca/listen/shows/northwind/episode/15649047%0A%0ACBC%20is%20celebrating%2060%20years%20of%20radio%20in%20the%20North.%20Northern%20filmmaker%20and%20storyteller%20Raymond%20Yakeleya%20joins%20Loren%20McGinnis%20on%20Northwind%20to%20reflect%20on%20some%20of%20his%20memories%20of%20CBC%20North.%0A%0AShared%20from%20cbc.ca%2Flisten%3A%20Listen%20to%20CBC%20Radio%27s%20live%20streams%20and%20shows%20on%20demand."
        data-sharesubject= Listen%20to%20this%2C%20from%20the%20CBC%20Radio%20show%20Northwind        data-sharetweet="Northern+filmmaker+and+storyteller+reflects+on+his+memories+of+CBC+North"
        aria-expanded="false"
        aria-label="Open Share">
        <svg viewBox="0 0 47.895 71.961" width="100%" height="100%">
            <g>
                <path class="share-icon-path" d="M35.9,47.974c-1.712,0-3.448,0.566-3.448,0.566c-0.749,0.245-1.819-0.012-2.376-0.568l-6.138-6.139
                    c-0.557-0.558-0.808-1.625-0.557-2.372c0,0,0.606-1.806,0.606-3.561c0-1.625-0.489-3.214-0.489-3.214
                    c-0.232-0.753,0.034-1.826,0.592-2.384l6.214-6.213c0.558-0.558,1.63-0.825,2.384-0.593c0,0,1.589,0.49,3.213,0.49
                    c6.624,0,11.994-5.369,11.994-11.993C47.895,5.369,42.524,0,35.9,0c-6.623,0-11.993,5.369-11.993,11.994
                    c0,1.7,0.557,3.419,0.557,3.419c0.242,0.751-0.015,1.82-0.572,2.378l-6.101,6.1c-0.558,0.558-1.627,0.815-2.377,0.572
                    c0,0-1.718-0.557-3.42-0.557C5.37,23.906,0,29.276,0,35.9c0,6.624,5.37,11.993,11.993,11.993c1.569,0,3.063-0.446,3.063-0.446
                    c0.755-0.226,1.83,0.046,2.387,0.604l6.351,6.351c0.558,0.557,0.825,1.63,0.594,2.384c0,0-0.48,1.569-0.48,3.182
                    c0,6.625,5.37,11.994,11.993,11.994c6.624,0,11.994-5.369,11.994-11.994C47.895,53.343,42.524,47.974,35.9,47.974z"/>
            </g>
        </svg>    </button>
</div>
            <div class="plus-wrapper">
    <button class="icon-plus" data-btntype="toggleOverlay" data-overlaytype="Plus" data-id="15649047" aria-expanded="false" aria-label="Add to My Radio options">
        <svg class="indicator-unadded" version="1.1" x="0px" y="0px"
     viewBox="0 0 40 40" enable-background="new 0 0 40 40;">
        <g>
            <path d="M39.972,19.265c-0.012-1.969-1.475-3.596-3.37-3.907C35.473,7.153,28.447,1,20.123,1
                C11.744,1,4.797,7.113,3.655,15.324c-0.946,0.077-1.827,0.465-2.504,1.146C0.402,17.223-0.006,18.219,0,19.275l0.028,4.447
                c0.014,2.19,1.818,3.972,4.021,3.972c0.52,0,1.022-0.103,1.49-0.291C7.172,34.012,13.087,38.709,20,38.709
                c6.861,0,12.831-4.729,14.461-11.328c0.48,0.202,1.006,0.313,1.557,0.313c1.071,0,2.076-0.418,2.831-1.178
                c0.748-0.753,1.157-1.749,1.15-2.804L39.972,19.265z M20.123,2.569c7.485,0,13.807,5.494,14.899,12.843
                c-0.713,0.17-1.371,0.524-1.901,1.058c-0.032,0.032-0.054,0.071-0.085,0.104C30.426,11.866,25.459,8.89,20,8.89
                c-5.498,0-10.456,2.987-13.057,7.723c-0.466-0.509-1.059-0.89-1.731-1.108C6.262,8.087,12.546,2.569,20.123,2.569z M1.597,23.712
                l-0.028-4.447c-0.004-0.634,0.243-1.234,0.695-1.689c0.458-0.461,1.068-0.715,1.717-0.715c1.343,0,2.443,1.082,2.452,2.413
                l0.028,4.447c0.004,0.634-0.243,1.234-0.694,1.688c-0.458,0.461-1.068,0.715-1.718,0.715C2.706,26.124,1.605,25.042,1.597,23.712z
                 M20,37.139c-6.385,0-11.815-4.476-13.065-10.691c0.709-0.745,1.102-1.711,1.096-2.737l-0.028-4.447
                c-0.002-0.34-0.061-0.664-0.144-0.98c2.164-4.766,6.876-7.826,12.141-7.826c5.225,0,9.946,3.045,12.12,7.776
                c-0.09,0.337-0.152,0.682-0.15,1.039l0.028,4.447c0.006,1.054,0.435,2.007,1.115,2.719l-0.033-0.007
                C31.838,32.637,26.337,37.139,20,37.139z M37.737,25.409c-0.458,0.461-1.068,0.715-1.718,0.715c-1.344,0-2.443-1.082-2.451-2.413
                l-0.028-4.447c-0.004-0.634,0.243-1.234,0.694-1.688c0.458-0.461,1.068-0.715,1.718-0.715c1.344,0,2.443,1.082,2.451,2.413
                l0.028,4.447C38.435,24.356,38.188,24.955,37.737,25.409z"/>
            <polygon points="20.589,15.489 19.411,15.489 19.411,23.211 11.69,23.211 11.69,24.388 19.411,24.388
                19.411,32.11 20.589,32.11 20.589,24.388 28.311,24.388 28.311,23.211 20.589,23.211   "/>
        </g>
        </svg>
        <svg class="indicator-added" viewBox="0 0 40 40">
            <path d="M39.804,19.416c-0.012-1.952-1.462-3.566-3.342-3.874c-1.12-8.135-8.086-14.237-16.341-14.237
            c-8.308,0-15.198,6.061-16.329,14.204c-0.938,0.076-1.812,0.461-2.483,1.136c-0.742,0.747-1.147,1.734-1.141,2.781l0.027,4.409
            c0.013,2.172,1.802,3.939,3.987,3.939c0.52,0,1.024-0.102,1.491-0.292C7.277,33.911,13.081,38.696,20,38.696
            c6.927,0,12.736-4.797,14.331-11.238c0.478,0.202,1.003,0.316,1.553,0.316c1.062,0,2.058-0.415,2.807-1.168
            c0.742-0.747,1.147-1.734,1.141-2.781L39.804,19.416z M5.886,25.509c-0.454,0.457-1.059,0.709-1.704,0.709
            c-1.332,0-2.423-1.073-2.431-2.392l-0.027-4.409c-0.004-0.629,0.241-1.224,0.689-1.674c0.454-0.457,1.059-0.709,1.703-0.709
            c1.332,0,2.423,1.073,2.431,2.393l0.027,4.409C6.579,24.463,6.334,25.058,5.886,25.509z M18.247,31.022l-6.669-6.669l1.238-1.238
            l5.432,5.431l10.189-10.189l1.238,1.238L18.247,31.022z M32.912,16.764C30.388,12.22,25.556,9.128,20,9.128
            c-5.572,0-10.415,3.109-12.933,7.674c-0.464-0.512-1.056-0.897-1.729-1.115c1.04-7.356,7.272-12.827,14.784-12.827
            c7.423,0,13.691,5.448,14.774,12.735c-0.707,0.169-1.359,0.52-1.885,1.049C32.973,16.681,32.947,16.726,32.912,16.764z
            M37.587,25.509c-0.454,0.457-1.059,0.709-1.703,0.709c-1.332,0-2.423-1.073-2.431-2.392l-0.027-4.409
            c-0.004-0.629,0.241-1.224,0.689-1.674c0.454-0.457,1.059-0.709,1.703-0.709c1.332,0,2.423,1.073,2.431,2.393l0.027,4.409
            C38.279,24.463,38.035,25.058,37.587,25.509z"/>
        </svg>
    </button>
</div>        </div>

    </div>

</li>


<li class="medialist-item">
    <div class="medialist-item-status">
        <button id="icon-play" class="icon-play" data-btntype="play" data-audioid="15633737" data-liveradioid="" aria-label="Play">
    <svg class="play" version="1.1" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40">
        <path d="M20,0C9,0,0,9,0,20c0,11,9,20,20,20s20-9,20-20C40,9,31,0,20,0z M20,38c-9.9,0-18-8.1-18-18c0-9.9,8.1-18,18-18 s18,8.1,18,18C38,29.9,29.9,38,20,38z"/>
        <polygon points="16.2,13.8 16.2,26.2 27.5,20 "/>
    </svg>

    <svg class="pause" height="100%" width="100%" viewBox="0 0 80 80">
        <path class="background" d="m0 0v80h80v-80h-80zm22 20h16v40h-16v-40zm20 0h16v40h-16v-40z"></path>
    </svg>

    <svg class="stop" height="100%" width="100%" viewBox="0 0 80 80">
        <path d="m0 0v80h80v-80h-80zm22 20h36v40h-36v-40z" class="background"/>
         <g display="none">
          <rect fill-rule="evenodd" height="40" width="36" y="20" x="22" class="foreground" fill="#fff"/>
         </g>
    </svg>
</button>        <span class="medialist-item-duration">05:31</span>
    </div>

    <div class="medialist-item-meta">

        <div class="medialist-item-title">
            <a href="/listen/shows/northwind/episode/15633737">
                Willy the Weasel            </a>
        </div>

        <div class="medialist-item-description">
            When winter comes, some animals look for warm places to go.
And this winter, a weasel has decided to spend some time in Myrtle Graham's house in Hay River.
The CBC's Marc Winkler reached her earlier this week.        </div>

        
        <div class="medialist-item-actions">
            <div class="medialist-item-date">
                <span>Nov 18, 2018</span>
            </div>
            <div class="share-wrapper">
    <button class="icon-share"
        data-btntype="toggleOverlay"
        data-overlaytype="Share"
        data-url="https://www.cbc.ca/listen/shows/northwind/episode/15633737"
        data-audioid="15633737"
        data-liveradioid=""
        data-sharemessage="Willy%20the%20Weasel%0A%0Ahttps://www.cbc.ca/listen/shows/northwind/episode/15633737%0A%0AWhen%20winter%20comes%2C%20some%20animals%20look%20for%20warm%20places%20to%20go.%0AAnd%20this%20winter%2C%20a%20weasel%20has%20decided%20to%20spend%20some%20time%20in%20Myrtle%20Graham%27s%20house%20in%20Hay%20River.%0AThe%20CBC%27s%20Marc%20Winkler%20reached%20her%20earlier%20this%20week.%0A%0AShared%20from%20cbc.ca%2Flisten%3A%20Listen%20to%20CBC%20Radio%27s%20live%20streams%20and%20shows%20on%20demand."
        data-sharesubject= Listen%20to%20this%2C%20from%20the%20CBC%20Radio%20show%20Northwind        data-sharetweet="Willy+the+Weasel"
        aria-expanded="false"
        aria-label="Open Share">
        <svg viewBox="0 0 47.895 71.961" width="100%" height="100%">
            <g>
                <path class="share-icon-path" d="M35.9,47.974c-1.712,0-3.448,0.566-3.448,0.566c-0.749,0.245-1.819-0.012-2.376-0.568l-6.138-6.139
                    c-0.557-0.558-0.808-1.625-0.557-2.372c0,0,0.606-1.806,0.606-3.561c0-1.625-0.489-3.214-0.489-3.214
                    c-0.232-0.753,0.034-1.826,0.592-2.384l6.214-6.213c0.558-0.558,1.63-0.825,2.384-0.593c0,0,1.589,0.49,3.213,0.49
                    c6.624,0,11.994-5.369,11.994-11.993C47.895,5.369,42.524,0,35.9,0c-6.623,0-11.993,5.369-11.993,11.994
                    c0,1.7,0.557,3.419,0.557,3.419c0.242,0.751-0.015,1.82-0.572,2.378l-6.101,6.1c-0.558,0.558-1.627,0.815-2.377,0.572
                    c0,0-1.718-0.557-3.42-0.557C5.37,23.906,0,29.276,0,35.9c0,6.624,5.37,11.993,11.993,11.993c1.569,0,3.063-0.446,3.063-0.446
                    c0.755-0.226,1.83,0.046,2.387,0.604l6.351,6.351c0.558,0.557,0.825,1.63,0.594,2.384c0,0-0.48,1.569-0.48,3.182
                    c0,6.625,5.37,11.994,11.993,11.994c6.624,0,11.994-5.369,11.994-11.994C47.895,53.343,42.524,47.974,35.9,47.974z"/>
            </g>
        </svg>    </button>
</div>
            <div class="plus-wrapper">
    <button class="icon-plus" data-btntype="toggleOverlay" data-overlaytype="Plus" data-id="15633737" aria-expanded="false" aria-label="Add to My Radio options">
        <svg class="indicator-unadded" version="1.1" x="0px" y="0px"
     viewBox="0 0 40 40" enable-background="new 0 0 40 40;">
        <g>
            <path d="M39.972,19.265c-0.012-1.969-1.475-3.596-3.37-3.907C35.473,7.153,28.447,1,20.123,1
                C11.744,1,4.797,7.113,3.655,15.324c-0.946,0.077-1.827,0.465-2.504,1.146C0.402,17.223-0.006,18.219,0,19.275l0.028,4.447
                c0.014,2.19,1.818,3.972,4.021,3.972c0.52,0,1.022-0.103,1.49-0.291C7.172,34.012,13.087,38.709,20,38.709
                c6.861,0,12.831-4.729,14.461-11.328c0.48,0.202,1.006,0.313,1.557,0.313c1.071,0,2.076-0.418,2.831-1.178
                c0.748-0.753,1.157-1.749,1.15-2.804L39.972,19.265z M20.123,2.569c7.485,0,13.807,5.494,14.899,12.843
                c-0.713,0.17-1.371,0.524-1.901,1.058c-0.032,0.032-0.054,0.071-0.085,0.104C30.426,11.866,25.459,8.89,20,8.89
                c-5.498,0-10.456,2.987-13.057,7.723c-0.466-0.509-1.059-0.89-1.731-1.108C6.262,8.087,12.546,2.569,20.123,2.569z M1.597,23.712
                l-0.028-4.447c-0.004-0.634,0.243-1.234,0.695-1.689c0.458-0.461,1.068-0.715,1.717-0.715c1.343,0,2.443,1.082,2.452,2.413
                l0.028,4.447c0.004,0.634-0.243,1.234-0.694,1.688c-0.458,0.461-1.068,0.715-1.718,0.715C2.706,26.124,1.605,25.042,1.597,23.712z
                 M20,37.139c-6.385,0-11.815-4.476-13.065-10.691c0.709-0.745,1.102-1.711,1.096-2.737l-0.028-4.447
                c-0.002-0.34-0.061-0.664-0.144-0.98c2.164-4.766,6.876-7.826,12.141-7.826c5.225,0,9.946,3.045,12.12,7.776
                c-0.09,0.337-0.152,0.682-0.15,1.039l0.028,4.447c0.006,1.054,0.435,2.007,1.115,2.719l-0.033-0.007
                C31.838,32.637,26.337,37.139,20,37.139z M37.737,25.409c-0.458,0.461-1.068,0.715-1.718,0.715c-1.344,0-2.443-1.082-2.451-2.413
                l-0.028-4.447c-0.004-0.634,0.243-1.234,0.694-1.688c0.458-0.461,1.068-0.715,1.718-0.715c1.344,0,2.443,1.082,2.451,2.413
                l0.028,4.447C38.435,24.356,38.188,24.955,37.737,25.409z"/>
            <polygon points="20.589,15.489 19.411,15.489 19.411,23.211 11.69,23.211 11.69,24.388 19.411,24.388
                19.411,32.11 20.589,32.11 20.589,24.388 28.311,24.388 28.311,23.211 20.589,23.211   "/>
        </g>
        </svg>
        <svg class="indicator-added" viewBox="0 0 40 40">
            <path d="M39.804,19.416c-0.012-1.952-1.462-3.566-3.342-3.874c-1.12-8.135-8.086-14.237-16.341-14.237
            c-8.308,0-15.198,6.061-16.329,14.204c-0.938,0.076-1.812,0.461-2.483,1.136c-0.742,0.747-1.147,1.734-1.141,2.781l0.027,4.409
            c0.013,2.172,1.802,3.939,3.987,3.939c0.52,0,1.024-0.102,1.491-0.292C7.277,33.911,13.081,38.696,20,38.696
            c6.927,0,12.736-4.797,14.331-11.238c0.478,0.202,1.003,0.316,1.553,0.316c1.062,0,2.058-0.415,2.807-1.168
            c0.742-0.747,1.147-1.734,1.141-2.781L39.804,19.416z M5.886,25.509c-0.454,0.457-1.059,0.709-1.704,0.709
            c-1.332,0-2.423-1.073-2.431-2.392l-0.027-4.409c-0.004-0.629,0.241-1.224,0.689-1.674c0.454-0.457,1.059-0.709,1.703-0.709
            c1.332,0,2.423,1.073,2.431,2.393l0.027,4.409C6.579,24.463,6.334,25.058,5.886,25.509z M18.247,31.022l-6.669-6.669l1.238-1.238
            l5.432,5.431l10.189-10.189l1.238,1.238L18.247,31.022z M32.912,16.764C30.388,12.22,25.556,9.128,20,9.128
            c-5.572,0-10.415,3.109-12.933,7.674c-0.464-0.512-1.056-0.897-1.729-1.115c1.04-7.356,7.272-12.827,14.784-12.827
            c7.423,0,13.691,5.448,14.774,12.735c-0.707,0.169-1.359,0.52-1.885,1.049C32.973,16.681,32.947,16.726,32.912,16.764z
            M37.587,25.509c-0.454,0.457-1.059,0.709-1.703,0.709c-1.332,0-2.423-1.073-2.431-2.392l-0.027-4.409
            c-0.004-0.629,0.241-1.224,0.689-1.674c0.454-0.457,1.059-0.709,1.703-0.709c1.332,0,2.423,1.073,2.431,2.393l0.027,4.409
            C38.279,24.463,38.035,25.058,37.587,25.509z"/>
        </svg>
    </button>
</div>        </div>

    </div>

</li>


<li class="medialist-item">
    <div class="medialist-item-status">
        <button id="icon-play" class="icon-play" data-btntype="play" data-audioid="15563288" data-liveradioid="" aria-label="Play">
    <svg class="play" version="1.1" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40">
        <path d="M20,0C9,0,0,9,0,20c0,11,9,20,20,20s20-9,20-20C40,9,31,0,20,0z M20,38c-9.9,0-18-8.1-18-18c0-9.9,8.1-18,18-18 s18,8.1,18,18C38,29.9,29.9,38,20,38z"/>
        <polygon points="16.2,13.8 16.2,26.2 27.5,20 "/>
    </svg>

    <svg class="pause" height="100%" width="100%" viewBox="0 0 80 80">
        <path class="background" d="m0 0v80h80v-80h-80zm22 20h16v40h-16v-40zm20 0h16v40h-16v-40z"></path>
    </svg>

    <svg class="stop" height="100%" width="100%" viewBox="0 0 80 80">
        <path d="m0 0v80h80v-80h-80zm22 20h36v40h-36v-40z" class="background"/>
         <g display="none">
          <rect fill-rule="evenodd" height="40" width="36" y="20" x="22" class="foreground" fill="#fff"/>
         </g>
    </svg>
</button>        <span class="medialist-item-duration">08:10</span>
    </div>

    <div class="medialist-item-meta">

        <div class="medialist-item-title">
            <a href="/listen/shows/northwind/episode/15563288">
                Going back to school at 67 years young            </a>
        </div>

        <div class="medialist-item-description">
            Therese Mantla, from Behchoko, recently graduated with a Community Linguist Certificate after attending courses at the Canadian Indigenous Languages and Literacy Development Institute at the University of Alberta. She says keeping the Tlicho language alive is important and wants to share her knowledge with younger generations.        </div>

        
        <div class="medialist-item-actions">
            <div class="medialist-item-date">
                <span>Aug 2, 2018</span>
            </div>
            <div class="share-wrapper">
    <button class="icon-share"
        data-btntype="toggleOverlay"
        data-overlaytype="Share"
        data-url="https://www.cbc.ca/listen/shows/northwind/episode/15563288"
        data-audioid="15563288"
        data-liveradioid=""
        data-sharemessage="Going%20back%20to%20school%20at%2067%20years%20young%0A%0Ahttps://www.cbc.ca/listen/shows/northwind/episode/15563288%0A%0ATherese%20Mantla%2C%20from%20Behchoko%2C%20recently%20graduated%20with%20a%20Community%20Linguist%20Certificate%20after%20attending%20courses%20at%20the%20Canadian%20Indigenous%20Languages%20and%20Literacy%20Development%20Institute%20at%20the%20University%20of%20Alberta.%20She%20says%20keeping%20the%20Tlicho%20language%20alive%20is%20important%20and%20wants%20to%20share%20her%20knowledge%20with%20younger%20generations.%0A%0AShared%20from%20cbc.ca%2Flisten%3A%20Listen%20to%20CBC%20Radio%27s%20live%20streams%20and%20shows%20on%20demand."
        data-sharesubject= Listen%20to%20this%2C%20from%20the%20CBC%20Radio%20show%20Northwind        data-sharetweet="Going+back+to+school+at+67+years+young"
        aria-expanded="false"
        aria-label="Open Share">
        <svg viewBox="0 0 47.895 71.961" width="100%" height="100%">
            <g>
                <path class="share-icon-path" d="M35.9,47.974c-1.712,0-3.448,0.566-3.448,0.566c-0.749,0.245-1.819-0.012-2.376-0.568l-6.138-6.139
                    c-0.557-0.558-0.808-1.625-0.557-2.372c0,0,0.606-1.806,0.606-3.561c0-1.625-0.489-3.214-0.489-3.214
                    c-0.232-0.753,0.034-1.826,0.592-2.384l6.214-6.213c0.558-0.558,1.63-0.825,2.384-0.593c0,0,1.589,0.49,3.213,0.49
                    c6.624,0,11.994-5.369,11.994-11.993C47.895,5.369,42.524,0,35.9,0c-6.623,0-11.993,5.369-11.993,11.994
                    c0,1.7,0.557,3.419,0.557,3.419c0.242,0.751-0.015,1.82-0.572,2.378l-6.101,6.1c-0.558,0.558-1.627,0.815-2.377,0.572
                    c0,0-1.718-0.557-3.42-0.557C5.37,23.906,0,29.276,0,35.9c0,6.624,5.37,11.993,11.993,11.993c1.569,0,3.063-0.446,3.063-0.446
                    c0.755-0.226,1.83,0.046,2.387,0.604l6.351,6.351c0.558,0.557,0.825,1.63,0.594,2.384c0,0-0.48,1.569-0.48,3.182
                    c0,6.625,5.37,11.994,11.993,11.994c6.624,0,11.994-5.369,11.994-11.994C47.895,53.343,42.524,47.974,35.9,47.974z"/>
            </g>
        </svg>    </button>
</div>
            <div class="plus-wrapper">
    <button class="icon-plus" data-btntype="toggleOverlay" data-overlaytype="Plus" data-id="15563288" aria-expanded="false" aria-label="Add to My Radio options">
        <svg class="indicator-unadded" version="1.1" x="0px" y="0px"
     viewBox="0 0 40 40" enable-background="new 0 0 40 40;">
        <g>
            <path d="M39.972,19.265c-0.012-1.969-1.475-3.596-3.37-3.907C35.473,7.153,28.447,1,20.123,1
                C11.744,1,4.797,7.113,3.655,15.324c-0.946,0.077-1.827,0.465-2.504,1.146C0.402,17.223-0.006,18.219,0,19.275l0.028,4.447
                c0.014,2.19,1.818,3.972,4.021,3.972c0.52,0,1.022-0.103,1.49-0.291C7.172,34.012,13.087,38.709,20,38.709
                c6.861,0,12.831-4.729,14.461-11.328c0.48,0.202,1.006,0.313,1.557,0.313c1.071,0,2.076-0.418,2.831-1.178
                c0.748-0.753,1.157-1.749,1.15-2.804L39.972,19.265z M20.123,2.569c7.485,0,13.807,5.494,14.899,12.843
                c-0.713,0.17-1.371,0.524-1.901,1.058c-0.032,0.032-0.054,0.071-0.085,0.104C30.426,11.866,25.459,8.89,20,8.89
                c-5.498,0-10.456,2.987-13.057,7.723c-0.466-0.509-1.059-0.89-1.731-1.108C6.262,8.087,12.546,2.569,20.123,2.569z M1.597,23.712
                l-0.028-4.447c-0.004-0.634,0.243-1.234,0.695-1.689c0.458-0.461,1.068-0.715,1.717-0.715c1.343,0,2.443,1.082,2.452,2.413
                l0.028,4.447c0.004,0.634-0.243,1.234-0.694,1.688c-0.458,0.461-1.068,0.715-1.718,0.715C2.706,26.124,1.605,25.042,1.597,23.712z
                 M20,37.139c-6.385,0-11.815-4.476-13.065-10.691c0.709-0.745,1.102-1.711,1.096-2.737l-0.028-4.447
                c-0.002-0.34-0.061-0.664-0.144-0.98c2.164-4.766,6.876-7.826,12.141-7.826c5.225,0,9.946,3.045,12.12,7.776
                c-0.09,0.337-0.152,0.682-0.15,1.039l0.028,4.447c0.006,1.054,0.435,2.007,1.115,2.719l-0.033-0.007
                C31.838,32.637,26.337,37.139,20,37.139z M37.737,25.409c-0.458,0.461-1.068,0.715-1.718,0.715c-1.344,0-2.443-1.082-2.451-2.413
                l-0.028-4.447c-0.004-0.634,0.243-1.234,0.694-1.688c0.458-0.461,1.068-0.715,1.718-0.715c1.344,0,2.443,1.082,2.451,2.413
                l0.028,4.447C38.435,24.356,38.188,24.955,37.737,25.409z"/>
            <polygon points="20.589,15.489 19.411,15.489 19.411,23.211 11.69,23.211 11.69,24.388 19.411,24.388
                19.411,32.11 20.589,32.11 20.589,24.388 28.311,24.388 28.311,23.211 20.589,23.211   "/>
        </g>
        </svg>
        <svg class="indicator-added" viewBox="0 0 40 40">
            <path d="M39.804,19.416c-0.012-1.952-1.462-3.566-3.342-3.874c-1.12-8.135-8.086-14.237-16.341-14.237
            c-8.308,0-15.198,6.061-16.329,14.204c-0.938,0.076-1.812,0.461-2.483,1.136c-0.742,0.747-1.147,1.734-1.141,2.781l0.027,4.409
            c0.013,2.172,1.802,3.939,3.987,3.939c0.52,0,1.024-0.102,1.491-0.292C7.277,33.911,13.081,38.696,20,38.696
            c6.927,0,12.736-4.797,14.331-11.238c0.478,0.202,1.003,0.316,1.553,0.316c1.062,0,2.058-0.415,2.807-1.168
            c0.742-0.747,1.147-1.734,1.141-2.781L39.804,19.416z M5.886,25.509c-0.454,0.457-1.059,0.709-1.704,0.709
            c-1.332,0-2.423-1.073-2.431-2.392l-0.027-4.409c-0.004-0.629,0.241-1.224,0.689-1.674c0.454-0.457,1.059-0.709,1.703-0.709
            c1.332,0,2.423,1.073,2.431,2.393l0.027,4.409C6.579,24.463,6.334,25.058,5.886,25.509z M18.247,31.022l-6.669-6.669l1.238-1.238
            l5.432,5.431l10.189-10.189l1.238,1.238L18.247,31.022z M32.912,16.764C30.388,12.22,25.556,9.128,20,9.128
            c-5.572,0-10.415,3.109-12.933,7.674c-0.464-0.512-1.056-0.897-1.729-1.115c1.04-7.356,7.272-12.827,14.784-12.827
            c7.423,0,13.691,5.448,14.774,12.735c-0.707,0.169-1.359,0.52-1.885,1.049C32.973,16.681,32.947,16.726,32.912,16.764z
            M37.587,25.509c-0.454,0.457-1.059,0.709-1.703,0.709c-1.332,0-2.423-1.073-2.431-2.392l-0.027-4.409
            c-0.004-0.629,0.241-1.224,0.689-1.674c0.454-0.457,1.059-0.709,1.703-0.709c1.332,0,2.423,1.073,2.431,2.393l0.027,4.409
            C38.279,24.463,38.035,25.058,37.587,25.509z"/>
        </svg>
    </button>
</div>        </div>

    </div>

</li>


<li class="medialist-item">
    <div class="medialist-item-status">
        <button id="icon-play" class="icon-play" data-btntype="play" data-audioid="15558734" data-liveradioid="" aria-label="Play">
    <svg class="play" version="1.1" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40">
        <path d="M20,0C9,0,0,9,0,20c0,11,9,20,20,20s20-9,20-20C40,9,31,0,20,0z M20,38c-9.9,0-18-8.1-18-18c0-9.9,8.1-18,18-18 s18,8.1,18,18C38,29.9,29.9,38,20,38z"/>
        <polygon points="16.2,13.8 16.2,26.2 27.5,20 "/>
    </svg>

    <svg class="pause" height="100%" width="100%" viewBox="0 0 80 80">
        <path class="background" d="m0 0v80h80v-80h-80zm22 20h16v40h-16v-40zm20 0h16v40h-16v-40z"></path>
    </svg>

    <svg class="stop" height="100%" width="100%" viewBox="0 0 80 80">
        <path d="m0 0v80h80v-80h-80zm22 20h36v40h-36v-40z" class="background"/>
         <g display="none">
          <rect fill-rule="evenodd" height="40" width="36" y="20" x="22" class="foreground" fill="#fff"/>
         </g>
    </svg>
</button>        <span class="medialist-item-duration">08:53</span>
    </div>

    <div class="medialist-item-meta">

        <div class="medialist-item-title">
            <a href="/listen/shows/northwind/episode/15558734">
                Inuvik Turns 60: Ann Pauls            </a>
        </div>

        <div class="medialist-item-description">
            The community of Inuvik is getting ready to celebrate its 60th anniversary and to celebrate, Northwind is sharing stories from back in the day. Ann Pauls is a retired teacher who taught at Sir Alexander Mackenzie School in the 1960s. This week, she rode back into town on a travelling tour bus from Manitoba. Northwind host Wanda McLeod joined up with her to share her memories.        </div>

        
        <div class="medialist-item-actions">
            <div class="medialist-item-date">
                <span>Jul 18, 2018</span>
            </div>
            <div class="share-wrapper">
    <button class="icon-share"
        data-btntype="toggleOverlay"
        data-overlaytype="Share"
        data-url="https://www.cbc.ca/listen/shows/northwind/episode/15558734"
        data-audioid="15558734"
        data-liveradioid=""
        data-sharemessage="Inuvik%20Turns%2060%3A%20Ann%20Pauls%0A%0Ahttps://www.cbc.ca/listen/shows/northwind/episode/15558734%0A%0AThe%20community%20of%20Inuvik%20is%20getting%20ready%20to%20celebrate%20its%2060th%20anniversary%20and%20to%20celebrate%2C%20Northwind%20is%20sharing%20stories%20from%20back%20in%20the%20day.%20Ann%20Pauls%20is%20a%20retired%20teacher%20who%20taught%20at%20Sir%20Alexander%20Mackenzie%20School%20in%20the%201960s.%20This%20week%2C%20she%20rode%20back%20into%20town%20on%20a%20travelling%20tour%20bus%20from%20Manitoba.%20Northwind%20host%20Wanda%20McLeod%20joined%20up%20with%20her%20to%20share%20her%20memories.%0A%0AShared%20from%20cbc.ca%2Flisten%3A%20Listen%20to%20CBC%20Radio%27s%20live%20streams%20and%20shows%20on%20demand."
        data-sharesubject= Listen%20to%20this%2C%20from%20the%20CBC%20Radio%20show%20Northwind        data-sharetweet="Inuvik+Turns+60%3A+Ann+Pauls"
        aria-expanded="false"
        aria-label="Open Share">
        <svg viewBox="0 0 47.895 71.961" width="100%" height="100%">
            <g>
                <path class="share-icon-path" d="M35.9,47.974c-1.712,0-3.448,0.566-3.448,0.566c-0.749,0.245-1.819-0.012-2.376-0.568l-6.138-6.139
                    c-0.557-0.558-0.808-1.625-0.557-2.372c0,0,0.606-1.806,0.606-3.561c0-1.625-0.489-3.214-0.489-3.214
                    c-0.232-0.753,0.034-1.826,0.592-2.384l6.214-6.213c0.558-0.558,1.63-0.825,2.384-0.593c0,0,1.589,0.49,3.213,0.49
                    c6.624,0,11.994-5.369,11.994-11.993C47.895,5.369,42.524,0,35.9,0c-6.623,0-11.993,5.369-11.993,11.994
                    c0,1.7,0.557,3.419,0.557,3.419c0.242,0.751-0.015,1.82-0.572,2.378l-6.101,6.1c-0.558,0.558-1.627,0.815-2.377,0.572
                    c0,0-1.718-0.557-3.42-0.557C5.37,23.906,0,29.276,0,35.9c0,6.624,5.37,11.993,11.993,11.993c1.569,0,3.063-0.446,3.063-0.446
                    c0.755-0.226,1.83,0.046,2.387,0.604l6.351,6.351c0.558,0.557,0.825,1.63,0.594,2.384c0,0-0.48,1.569-0.48,3.182
                    c0,6.625,5.37,11.994,11.993,11.994c6.624,0,11.994-5.369,11.994-11.994C47.895,53.343,42.524,47.974,35.9,47.974z"/>
            </g>
        </svg>    </button>
</div>
            <div class="plus-wrapper">
    <button class="icon-plus" data-btntype="toggleOverlay" data-overlaytype="Plus" data-id="15558734" aria-expanded="false" aria-label="Add to My Radio options">
        <svg class="indicator-unadded" version="1.1" x="0px" y="0px"
     viewBox="0 0 40 40" enable-background="new 0 0 40 40;">
        <g>
            <path d="M39.972,19.265c-0.012-1.969-1.475-3.596-3.37-3.907C35.473,7.153,28.447,1,20.123,1
                C11.744,1,4.797,7.113,3.655,15.324c-0.946,0.077-1.827,0.465-2.504,1.146C0.402,17.223-0.006,18.219,0,19.275l0.028,4.447
                c0.014,2.19,1.818,3.972,4.021,3.972c0.52,0,1.022-0.103,1.49-0.291C7.172,34.012,13.087,38.709,20,38.709
                c6.861,0,12.831-4.729,14.461-11.328c0.48,0.202,1.006,0.313,1.557,0.313c1.071,0,2.076-0.418,2.831-1.178
                c0.748-0.753,1.157-1.749,1.15-2.804L39.972,19.265z M20.123,2.569c7.485,0,13.807,5.494,14.899,12.843
                c-0.713,0.17-1.371,0.524-1.901,1.058c-0.032,0.032-0.054,0.071-0.085,0.104C30.426,11.866,25.459,8.89,20,8.89
                c-5.498,0-10.456,2.987-13.057,7.723c-0.466-0.509-1.059-0.89-1.731-1.108C6.262,8.087,12.546,2.569,20.123,2.569z M1.597,23.712
                l-0.028-4.447c-0.004-0.634,0.243-1.234,0.695-1.689c0.458-0.461,1.068-0.715,1.717-0.715c1.343,0,2.443,1.082,2.452,2.413
                l0.028,4.447c0.004,0.634-0.243,1.234-0.694,1.688c-0.458,0.461-1.068,0.715-1.718,0.715C2.706,26.124,1.605,25.042,1.597,23.712z
                 M20,37.139c-6.385,0-11.815-4.476-13.065-10.691c0.709-0.745,1.102-1.711,1.096-2.737l-0.028-4.447
                c-0.002-0.34-0.061-0.664-0.144-0.98c2.164-4.766,6.876-7.826,12.141-7.826c5.225,0,9.946,3.045,12.12,7.776
                c-0.09,0.337-0.152,0.682-0.15,1.039l0.028,4.447c0.006,1.054,0.435,2.007,1.115,2.719l-0.033-0.007
                C31.838,32.637,26.337,37.139,20,37.139z M37.737,25.409c-0.458,0.461-1.068,0.715-1.718,0.715c-1.344,0-2.443-1.082-2.451-2.413
                l-0.028-4.447c-0.004-0.634,0.243-1.234,0.694-1.688c0.458-0.461,1.068-0.715,1.718-0.715c1.344,0,2.443,1.082,2.451,2.413
                l0.028,4.447C38.435,24.356,38.188,24.955,37.737,25.409z"/>
            <polygon points="20.589,15.489 19.411,15.489 19.411,23.211 11.69,23.211 11.69,24.388 19.411,24.388
                19.411,32.11 20.589,32.11 20.589,24.388 28.311,24.388 28.311,23.211 20.589,23.211   "/>
        </g>
        </svg>
        <svg class="indicator-added" viewBox="0 0 40 40">
            <path d="M39.804,19.416c-0.012-1.952-1.462-3.566-3.342-3.874c-1.12-8.135-8.086-14.237-16.341-14.237
            c-8.308,0-15.198,6.061-16.329,14.204c-0.938,0.076-1.812,0.461-2.483,1.136c-0.742,0.747-1.147,1.734-1.141,2.781l0.027,4.409
            c0.013,2.172,1.802,3.939,3.987,3.939c0.52,0,1.024-0.102,1.491-0.292C7.277,33.911,13.081,38.696,20,38.696
            c6.927,0,12.736-4.797,14.331-11.238c0.478,0.202,1.003,0.316,1.553,0.316c1.062,0,2.058-0.415,2.807-1.168
            c0.742-0.747,1.147-1.734,1.141-2.781L39.804,19.416z M5.886,25.509c-0.454,0.457-1.059,0.709-1.704,0.709
            c-1.332,0-2.423-1.073-2.431-2.392l-0.027-4.409c-0.004-0.629,0.241-1.224,0.689-1.674c0.454-0.457,1.059-0.709,1.703-0.709
            c1.332,0,2.423,1.073,2.431,2.393l0.027,4.409C6.579,24.463,6.334,25.058,5.886,25.509z M18.247,31.022l-6.669-6.669l1.238-1.238
            l5.432,5.431l10.189-10.189l1.238,1.238L18.247,31.022z M32.912,16.764C30.388,12.22,25.556,9.128,20,9.128
            c-5.572,0-10.415,3.109-12.933,7.674c-0.464-0.512-1.056-0.897-1.729-1.115c1.04-7.356,7.272-12.827,14.784-12.827
            c7.423,0,13.691,5.448,14.774,12.735c-0.707,0.169-1.359,0.52-1.885,1.049C32.973,16.681,32.947,16.726,32.912,16.764z
            M37.587,25.509c-0.454,0.457-1.059,0.709-1.703,0.709c-1.332,0-2.423-1.073-2.431-2.392l-0.027-4.409
            c-0.004-0.629,0.241-1.224,0.689-1.674c0.454-0.457,1.059-0.709,1.703-0.709c1.332,0,2.423,1.073,2.431,2.393l0.027,4.409
            C38.279,24.463,38.035,25.058,37.587,25.509z"/>
        </svg>
    </button>
</div>        </div>

    </div>

</li>


<li class="medialist-item">
    <div class="medialist-item-status">
        <button id="icon-play" class="icon-play" data-btntype="play" data-audioid="15554417" data-liveradioid="" aria-label="Play">
    <svg class="play" version="1.1" x="0px" y="0px" viewBox="0 0 40 40" enable-background="new 0 0 40 40">
        <path d="M20,0C9,0,0,9,0,20c0,11,9,20,20,20s20-9,20-20C40,9,31,0,20,0z M20,38c-9.9,0-18-8.1-18-18c0-9.9,8.1-18,18-18 s18,8.1,18,18C38,29.9,29.9,38,20,38z"/>
        <polygon points="16.2,13.8 16.2,26.2 27.5,20 "/>
    </svg>

    <svg class="pause" height="100%" width="100%" viewBox="0 0 80 80">
        <path class="background" d="m0 0v80h80v-80h-80zm22 20h16v40h-16v-40zm20 0h16v40h-16v-40z"></path>
    </svg>

    <svg class="stop" height="100%" width="100%" viewBox="0 0 80 80">
        <path d="m0 0v80h80v-80h-80zm22 20h36v40h-36v-40z" class="background"/>
         <g display="none">
          <rect fill-rule="evenodd" height="40" width="36" y="20" x="22" class="foreground" fill="#fff"/>
         </g>
    </svg>
</button>        <span class="medialist-item-duration">14:42</span>
    </div>

    <div class="medialist-item-meta">

        <div class="medialist-item-title">
            <a href="/listen/shows/northwind/episode/15554417">
                Northern broadcaster Les Carpenter dies at 61            </a>
        </div>

        <div class="medialist-item-description">
            Les Carpenter, an iconic Northern broadcaster and CEO of CKLB Radio, passed away this week after a brief illness at the age of 61. Carpenter began his career at CBC in Inuvik, but soon left to serve as mayor of Sachs Harbour and help found the Inuvialuit Regional Corporation. In an interview with Northwind host Wanda McLeod, Louis Goose, a longtime friend of Carpenter and former CBC broadcaster, reflected on his legacy and his impact on northern broadcasting and politics.        </div>

        
        <div class="medialist-item-actions">
            <div class="medialist-item-date">
                <span>Jul 4, 2018</span>
            </div>
            <div class="share-wrapper">
    <button class="icon-share"
        data-btntype="toggleOverlay"
        data-overlaytype="Share"
        data-url="https://www.cbc.ca/listen/shows/northwind/episode/15554417"
        data-audioid="15554417"
        data-liveradioid=""
        data-sharemessage="Northern%20broadcaster%20Les%20Carpenter%20dies%20at%2061%0A%0Ahttps://www.cbc.ca/listen/shows/northwind/episode/15554417%0A%0ALes%20Carpenter%2C%20an%20iconic%20Northern%20broadcaster%20and%20CEO%20of%20CKLB%20Radio%2C%20passed%20away%20this%20week%20after%20a%20brief%20illness%20at%20the%20age%20of%2061.%20Carpenter%20began%20his%20career%20at%20CBC%20in%20Inuvik%2C%20but%20soon%20left%20to%20serve%20as%20mayor%20of%20Sachs%20Harbour%20and%20help%20found%20the%20Inuvialuit%20Regional%20Corporation.%20In%20an%20interview%20with%20Northwind%20host%20Wanda%20McLeod%2C%20Louis%20Goose%2C%20a%20longtime%20friend%20of%20Carpenter%20and%20former%20CBC%20broadcaster%2C%20reflected%20on%20his%20legacy%20and%20his%20impact%20on%20northern%20broadcasting%20and%20politics.%0A%0AShared%20from%20cbc.ca%2Flisten%3A%20Listen%20to%20CBC%20Radio%27s%20live%20streams%20and%20shows%20on%20demand."
        data-sharesubject= Listen%20to%20this%2C%20from%20the%20CBC%20Radio%20show%20Northwind        data-sharetweet="Northern+broadcaster+Les+Carpenter+dies+at+61"
        aria-expanded="false"
        aria-label="Open Share">
        <svg viewBox="0 0 47.895 71.961" width="100%" height="100%">
            <g>
                <path class="share-icon-path" d="M35.9,47.974c-1.712,0-3.448,0.566-3.448,0.566c-0.749,0.245-1.819-0.012-2.376-0.568l-6.138-6.139
                    c-0.557-0.558-0.808-1.625-0.557-2.372c0,0,0.606-1.806,0.606-3.561c0-1.625-0.489-3.214-0.489-3.214
                    c-0.232-0.753,0.034-1.826,0.592-2.384l6.214-6.213c0.558-0.558,1.63-0.825,2.384-0.593c0,0,1.589,0.49,3.213,0.49
                    c6.624,0,11.994-5.369,11.994-11.993C47.895,5.369,42.524,0,35.9,0c-6.623,0-11.993,5.369-11.993,11.994
                    c0,1.7,0.557,3.419,0.557,3.419c0.242,0.751-0.015,1.82-0.572,2.378l-6.101,6.1c-0.558,0.558-1.627,0.815-2.377,0.572
                    c0,0-1.718-0.557-3.42-0.557C5.37,23.906,0,29.276,0,35.9c0,6.624,5.37,11.993,11.993,11.993c1.569,0,3.063-0.446,3.063-0.446
                    c0.755-0.226,1.83,0.046,2.387,0.604l6.351,6.351c0.558,0.557,0.825,1.63,0.594,2.384c0,0-0.48,1.569-0.48,3.182
                    c0,6.625,5.37,11.994,11.993,11.994c6.624,0,11.994-5.369,11.994-11.994C47.895,53.343,42.524,47.974,35.9,47.974z"/>
            </g>
        </svg>    </button>
</div>
            <div class="plus-wrapper">
    <button class="icon-plus" data-btntype="toggleOverlay" data-overlaytype="Plus" data-id="15554417" aria-expanded="false" aria-label="Add to My Radio options">
        <svg class="indicator-unadded" version="1.1" x="0px" y="0px"
     viewBox="0 0 40 40" enable-background="new 0 0 40 40;">
        <g>
            <path d="M39.972,19.265c-0.012-1.969-1.475-3.596-3.37-3.907C35.473,7.153,28.447,1,20.123,1
                C11.744,1,4.797,7.113,3.655,15.324c-0.946,0.077-1.827,0.465-2.504,1.146C0.402,17.223-0.006,18.219,0,19.275l0.028,4.447
                c0.014,2.19,1.818,3.972,4.021,3.972c0.52,0,1.022-0.103,1.49-0.291C7.172,34.012,13.087,38.709,20,38.709
                c6.861,0,12.831-4.729,14.461-11.328c0.48,0.202,1.006,0.313,1.557,0.313c1.071,0,2.076-0.418,2.831-1.178
                c0.748-0.753,1.157-1.749,1.15-2.804L39.972,19.265z M20.123,2.569c7.485,0,13.807,5.494,14.899,12.843
                c-0.713,0.17-1.371,0.524-1.901,1.058c-0.032,0.032-0.054,0.071-0.085,0.104C30.426,11.866,25.459,8.89,20,8.89
                c-5.498,0-10.456,2.987-13.057,7.723c-0.466-0.509-1.059-0.89-1.731-1.108C6.262,8.087,12.546,2.569,20.123,2.569z M1.597,23.712
                l-0.028-4.447c-0.004-0.634,0.243-1.234,0.695-1.689c0.458-0.461,1.068-0.715,1.717-0.715c1.343,0,2.443,1.082,2.452,2.413
                l0.028,4.447c0.004,0.634-0.243,1.234-0.694,1.688c-0.458,0.461-1.068,0.715-1.718,0.715C2.706,26.124,1.605,25.042,1.597,23.712z
                 M20,37.139c-6.385,0-11.815-4.476-13.065-10.691c0.709-0.745,1.102-1.711,1.096-2.737l-0.028-4.447
                c-0.002-0.34-0.061-0.664-0.144-0.98c2.164-4.766,6.876-7.826,12.141-7.826c5.225,0,9.946,3.045,12.12,7.776
                c-0.09,0.337-0.152,0.682-0.15,1.039l0.028,4.447c0.006,1.054,0.435,2.007,1.115,2.719l-0.033-0.007
                C31.838,32.637,26.337,37.139,20,37.139z M37.737,25.409c-0.458,0.461-1.068,0.715-1.718,0.715c-1.344,0-2.443-1.082-2.451-2.413
                l-0.028-4.447c-0.004-0.634,0.243-1.234,0.694-1.688c0.458-0.461,1.068-0.715,1.718-0.715c1.344,0,2.443,1.082,2.451,2.413
                l0.028,4.447C38.435,24.356,38.188,24.955,37.737,25.409z"/>
            <polygon points="20.589,15.489 19.411,15.489 19.411,23.211 11.69,23.211 11.69,24.388 19.411,24.388
                19.411,32.11 20.589,32.11 20.589,24.388 28.311,24.388 28.311,23.211 20.589,23.211   "/>
        </g>
        </svg>
        <svg class="indicator-added" viewBox="0 0 40 40">
            <path d="M39.804,19.416c-0.012-1.952-1.462-3.566-3.342-3.874c-1.12-8.135-8.086-14.237-16.341-14.237
            c-8.308,0-15.198,6.061-16.329,14.204c-0.938,0.076-1.812,0.461-2.483,1.136c-0.742,0.747-1.147,1.734-1.141,2.781l0.027,4.409
            c0.013,2.172,1.802,3.939,3.987,3.939c0.52,0,1.024-0.102,1.491-0.292C7.277,33.911,13.081,38.696,20,38.696
            c6.927,0,12.736-4.797,14.331-11.238c0.478,0.202,1.003,0.316,1.553,0.316c1.062,0,2.058-0.415,2.807-1.168
            c0.742-0.747,1.147-1.734,1.141-2.781L39.804,19.416z M5.886,25.509c-0.454,0.457-1.059,0.709-1.704,0.709
            c-1.332,0-2.423-1.073-2.431-2.392l-0.027-4.409c-0.004-0.629,0.241-1.224,0.689-1.674c0.454-0.457,1.059-0.709,1.703-0.709
            c1.332,0,2.423,1.073,2.431,2.393l0.027,4.409C6.579,24.463,6.334,25.058,5.886,25.509z M18.247,31.022l-6.669-6.669l1.238-1.238
            l5.432,5.431l10.189-10.189l1.238,1.238L18.247,31.022z M32.912,16.764C30.388,12.22,25.556,9.128,20,9.128
            c-5.572,0-10.415,3.109-12.933,7.674c-0.464-0.512-1.056-0.897-1.729-1.115c1.04-7.356,7.272-12.827,14.784-12.827
            c7.423,0,13.691,5.448,14.774,12.735c-0.707,0.169-1.359,0.52-1.885,1.049C32.973,16.681,32.947,16.726,32.912,16.764z
            M37.587,25.509c-0.454,0.457-1.059,0.709-1.703,0.709c-1.332,0-2.423-1.073-2.431-2.392l-0.027-4.409
            c-0.004-0.629,0.241-1.224,0.689-1.674c0.454-0.457,1.059-0.709,1.703-0.709c1.332,0,2.423,1.073,2.431,2.393l0.027,4.409
            C38.279,24.463,38.035,25.058,37.587,25.509z"/>
        </svg>
    </button>
</div>        </div>

    </div>

</li>

</ul>

    <div class="icon-loading">
        <span></span><span></span>
    </div>
    </div>
</section>
        </main>

        <script type="text/javascript">
            /**
            * @param  {[string]} name [cookie name]
            * @return {[string]}      [cookie value decoded]
            */
            function _getCookie(name) {
                if (!name) {
                    return null;
                }
                return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(name).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
            }

            var cbclistenbannercount = parseInt(_getCookie('cbclistenbannercount'), 10);
            var uleBannerWrapper = document.getElementsByClassName('ule-banner-wrapper')[0];
            var header = document.getElementsByTagName('header')[0];
            var main = document.getElementsByTagName('main')[0];

            if(!cbclistenbannercount) {
                cbclistenbannercount = 0;
            }

            if (cbclistenbannercount < 2) {
                uleBannerWrapper.classList.add('ule-banner--enabled');
                header.classList.add('ule-banner--enabled');
                main.classList.add('ule-banner--enabled');
            }
            else {
                uleBannerWrapper.classList.remove('ule-banner--enabled');
                header.classList.remove('ule-banner--enabled');
                main.classList.remove('ule-banner--enabled');
            }
        </script>

        <div class="listen-player-outer">
	<section class="listen-player-container" role="region" aria-live="off" aria-label="My Radio Player" tabindex="0">
	    <div class="listen-player">
	        <div class="myradio-wrapper">
    <button class="icon-myradio" data-btntype="toggleOverlay" data-overlaytype="Playlist" aria-expanded="false" aria-label="Open Playlist"></button>
    <div class="playlist-title">My Radio</div>
    <span class="screen-reader" role="tooltip">My Radio</span>
</div>
<div class="live-pulse-container"><div class="pulse-ring"></div><div class="pulse-ring delay"></div><div class="pulse-ring delay"></div><div class="pulse-dot"></div></div>	    </div>
	    <div id="playlist-container">
	        <section class="modal" id="playlistModal" aria-labelledby="modal-title" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="icon-myradio blue">My Radio</span>
                <h1 id="modal-title">MY RADIO</h1>
                <button type="button" class="icon-close" data-btntype="closePlaylist" aria-label="Close playlist"></button>
            </div>
            <div class="modal-body">
                <div class="modal-list">
                </div>
            </div>
            <button class="modal-clear" data-btntype="clearPlaylist">Clear Playlist</button>
        </div>
    </div>
</section>
	    </div>
	    	    <div tabindex="0"></div>
	</section>
</div>
        <script type="text/javascript">
            CBC.APP.SC.DTM.DATA = {"spa":true,"preventDefault":true,"contentarea":"listen","url":"http:\/\/www.cbc.ca\/listen\/shows\/northwind","pillar":"listen","subsection1":"shows","subsection2":"northwind","contenttype":"index","title":"CBC Listen | Northwind"};
        </script>

        <script type="text/javascript" src="/assets/js/cbc-stats-bottom.js"></script>

        <script type="text/javascript">
            //initial stats call on page load
            CBC.APP.SC.PageTracker.init();
        </script>
    </body>
</html>

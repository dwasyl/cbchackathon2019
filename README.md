# Team True North, CBC/Radio-Canada Hackathon 2019, February 2019

## Instructions
1. Either pull audio feed from CBC Audio API/DAVE, or record from live stream (record.py)
2. Save to S3 Bucket (north-audio)
3. Lambda will recognize and push to AWS Transcribe
4. Transcription result processed by Lambda, or manually if need results customized (analys.py)
5. CBC Radio page will load .json files by <AUDIOID>.json from /assets/
6. Click links

## Dependencies
- CBC Audio API or streamed show
- AWS Credentials
- Python 2.7/3.6

## Demo Site
[Demo Page](http://ec2-18-191-218-191.us-east-2.compute.amazonaws.com/listen/shows/northwind/episode/867/)
[Demo Shared Page Jumping to Interview](http://ec2-18-191-218-191.us-east-2.compute.amazonaws.com/listen/shows/northwind/episode/867/?start=1209)
